<?php

namespace App\Http\Controllers;
use App\Model\Order;
use App\Model\Hotel;
use App\Model\Merchant;
use App\Model\RoomType;
use App\Model\MerchantDetail;
use App\User;
use Carbon\Carbon;

use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orders = Order::all();
        return view("admins.orders.index",compact(["orders"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {        
        $order_last = Order::latest('created_at')->first();
        if ($order_last) {
            $increament_invoice = (int)explode("/", $order_last->invoice)[3] + 1;
            $invoice = 'WK/OR/'.date('Y').'/'.$increament_invoice;
        }else{
            $invoice = 'WK/OR/'.date('Y').'/0';
        }
        $order_by = User::where('user_group_id', 3)->get();
        $merchant_kuliner = Merchant::where('merchant_type_id', 1)->get();
        $merchant_wisata = Merchant::where('merchant_type_id', 2)->get();
        $hotels = Hotel::all();
        $room_types = RoomType::all();
        $order = new Order;
        $status_orders = $order->status_order();
        $payment_methods = $order->payment_method();
        
        return view("admins.orders.create",compact(["invoice","payment_methods","status_orders","hotels","merchant_kuliner", "merchant_wisata","order_by", "room_types"]));
    }

    public function insert(Request $request)
    {
        $input = $request->all();
        
        $order = new Order;
        $order->invoice = $input['invoice'];
        $order->order_by = $input['user_id'];
        $order->order_date = date("Y-m-d");
        $order->required_date = date("Y-m-d", strtotime($input['required_date']));                
        $order->status_order = $input['status_order'];
        $order->payment_method = $input['payment_method'];
        $order->discount = $input['discount'];
        $order->subtotal = $input['subtotal'];
        $order->additional_note = $input['additional_note'];        
        $order->save();

        $i = 0;
            $count_looping = count($input['hotel_id']);
            for ($x = 0; $x < $count_looping; $x++) {
                $order_detail = new OrderDetail;
                $order_detail->order_id = $order->id;
                $order_detail->hotel_id = $input['hotel_id'][$i];
                $order_detail->merchant_id = $input['merchant_id'][$i];
                $order_detail->flight_id = $input['flight_id'][$i];
                $order_detail->quantity_order = $input['quantity_order'][$i];
                if ($input['hotel_id'][$i]) {
                    $hotel = Hotel::find($input['hotel_id'][$i]);
                }
                if ($input['merchant_id'][$i]) {
                    $hotel = Merchant::find($input['merchant_id'][$i]);
                }
                // if ($input['flight_id'][$i]) {
                //     $hotel = Merchant::find($input['flight_id'][$i]);
                // }

                $order_detail->unit_price = 0;
            }
            
        \Session::flash('status','Order berhasil dibuat');
        return redirect("admin/orders");
    }

    public function show($id)
    {
        $order = Order::find($id);
        $order_model = new Order;
        $status_order = $order_model->status_order()[$order->status_order];
        $payment_method = $order_model->payment_method()[$order->payment_method];
        return view('admins.orders.show', compact(["order", "status_order", "payment_method"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Request $request, $id)
    {   
        $order = Order::find($id);        
        $order_by = User::where('user_group_id', 3)->get();
        $merchant = Merchant::all();
        $hotel = Hotel::all();
        $order_data = new Order;
        $status_orders = $order_data->status_order();
        $payment_methods = $order_data->payment_method();
        
        return view("admins.orders.edit",compact(["order", "order_by", "merchant", "hotel", "status_orders", "payment_methods"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {              
        $input = $request->all();
        
        $order = Order::find($input["id"]);
        $order->invoice = $input['invoice'];
        $order->order_by = $input['user_id'];
        $order->order_date = date("Y-m-d");
        $order->required_date = date("Y-m-d", strtotime($input['required_date']));                
        $order->status_order = $input['status_order'];
        $order->payment_method = $input['payment_method'];
        $order->discount = $input['discount'];
        $order->subtotal = $input['subtotal'];
        $order->additional_note = $input['additional_note'];        
        $order->save();        
        
        \Session::flash('status','Order berhasil diubah');
        return redirect("admin/orders");
    }

    public function delete(Request $request, $id)
    { 
        $order = Order::find($id);
        foreach ($order->order_details as $order_detail) {
            $order_detail->delete();
        }
        $order->delete();

        return redirect("admin/orders");
    }

    public function process($id)
    { 
        $order = Order::find($id);        
        if($order->status_order == 3){
            $order->status_order = 4;
        }
        $order->save();

        return redirect("admin/orders");
    }

    public function completed($id)
    { 
        $order = Order::find($id);
        if($order->status_order == 4){
            $order->status_order = 5;
        }
        $order->save();

        return redirect("admin/orders");
    }

    public function reject($id)
    { 
        $order = Order::find($id);
        if($order->status_order != 5){
            $order->status_order = 6;
        }
        $order->save();

        return redirect("admin/orders");
    }

    public function get_merchant_detail(Request $request, $merchant_id)
    {
        $merchant_details = MerchantDetail::where('merchant_id', $merchant_id)->get();

        return $merchant_details;
    }
}
