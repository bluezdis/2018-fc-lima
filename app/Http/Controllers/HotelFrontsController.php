<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Hotel;
use App\Model\HotelDetails;
use App\Model\State;
use App\Model\District;
use App\Model\RoomType;
use App\Model\Order;
use App\Model\OrderDetail;
use File;
use Auth;
use DB;

class HotelFrontsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index_frontend()
    {
        $district_id = $_GET['district_id'];                
        $room_type_id = $_GET['room_type_id'];

        if ($district_id == 0 ) {
            $hotels = Hotel::all();
        }else {
            if ($room_type_id == 0 ) {
                $hotels = Hotel::where('district_id',  $district_id)->get();
            }else{
                $hotels = Hotel::where('district_id',  $district_id)->get();
            }            
        }

        $districts = District::all();
        $room_types = RoomType::all();
        return view("hotels.index",compact(["hotels", "districts", "room_types"]));
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function booking()
    {

        $id = $_GET['bookId'];
        $check_in = $_GET['hotel_check_in'];
        $check_out = $_GET['hotel_check_out'];
        
        $check_in_form = date("d-m-Y",strtotime($check_in));
        $check_out_form = date("d-m-Y",strtotime($check_out));
        $check_in_data = strtotime($check_in);
        $check_out_data = strtotime($check_out);
        $quantity_order = date("d", $check_out_data - $check_in_data) - 1;

        //$hotel = Hotel::find($id);
        $hotel_detail = HotelDetails::find($id);
        $hotel = $hotel_detail->hotel;
        $order = new Order;
        $payment_methods = $order->payment_method();

        $order_last = Order::latest('created_at')->first();
        if ($order_last) {
            $increament_invoice = (int)explode("/", $order_last->invoice)[3] + 1;
            $invoice = 'WK/OR/'.date('Y').'/'.$increament_invoice;
        }else{
            $invoice = 'WK/OR/'.date('Y').'/0';
        }

        return view("hotels.show",compact(["hotel", "invoice", "hotel_detail", "payment_methods", "check_in_form", "check_out_form", "quantity_order"]));        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();
        $hotel = Hotel::find($input['hotel_id']);
        $hotel_detail = $hotel->hotel_details->where('room_type_id', $input['room_type_id'])->first();

        $order = new Order;
        $order->invoice = $input['invoice'];
        $order->order_by = Auth::user()->id;
        $order->order_date = date("Y-m-d");
        $order->required_date = date("Y-m-d", strtotime($input['hotel_check_in']));                
        $order->status_order = 1;
        $order->payment_method = $input['payment_method'];
        $order->discount = 0;        
        $order->additional_note = $input['additional_note'];                
        $order->save();

            $order_detail = new OrderDetail;
            $order_detail->order_id = $order->id;
            $order_detail->hotel_id = $input['hotel_id'];
            $order_detail->quantity_order = $input['total'];
            $order_detail->unit_price = $hotel_detail->rate;
            $order_detail->hotel_room_type = $hotel_detail->room_type_id;
            $order_detail->total_price = $order_detail->unit_price * $order_detail->quantity_order;
            $order_detail->date_order_detail = date("Y-m-d");            
            $order_detail->hotel_check_in = date("Y-m-d", strtotime($input['hotel_check_in'])); 
            $order_detail->hotel_check_out = date("Y-m-d", strtotime($input['hotel_check_out']));
            $order_detail->hotel_remarks = $input['additional_note'];
            $order_detail->save();

        $order->subtotal = $order_detail->total_price;
        $order->save();

        \Session::flash('status','Hotel berhasil dibuat');
        return redirect("/myorders");
    }


}
