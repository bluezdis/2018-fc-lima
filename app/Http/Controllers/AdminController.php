<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Merchant;
use App\Model\Hotel;
use App\Model\Order;
use App\User;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $merchant_count = Merchant::all()->count();
        $hotel_count = Hotel::all()->count();
        $user_count = User::where('user_group_id', 3)->count();
        $order_count = Order::where('status_order', 5)->count();
        return view('admins/dashboard',compact(["merchant_count", "hotel_count", "user_count", "order_count"]));
    }
}
