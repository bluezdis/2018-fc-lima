<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Merchant;
use App\Model\MerchantDetail;
use App\Model\MerchantType;
use App\Model\State;
use App\Model\District;
use App\User;
use File;

class MerchantsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $merchants = Merchant::all();
        return view("admins.merchants.index",compact(["merchants"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $merchant_types = MerchantType::all();
        $states = State::all();
        $districts = District::all();
        $merchant_member = User::where('user_group_id', 2)->get();
        
        return view("admins.merchants.create",compact(["merchant_types", "states", "districts", "merchant_member"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();

        $merchant = new Merchant;
        $merchant->name = $input['name'];
        $merchant->merchant_type_id = $input['merchant_type_id'];
        $merchant->state_id = $input['state_id'];
        $merchant->district_id = $input['district_id'];
        $merchant->user_id = $input['user_id'];
        $merchant->rate = $input['rate'];
        $merchant->address = $input['address'];
        $merchant->facilities = $input['facilities'];
        if ($request->file('photo')) {
            $files = $input['photo'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/merchants/'; // The destination were you store the image.
                if(!(file_exists(public_path('/uploads/attachment/merchants/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $merchant->photo = $destinationPath.'/'.$filename;
            }
        }
        $merchant->save();
        
        $i = 0;
            $count_looping = count($input['name_detail']);
            for ($x = 0; $x < $count_looping; $x++) {
                $merchant_detail = new MerchantDetail;
                $merchant_detail->name = $input['name_detail'][$i];
                $merchant_detail->merchant_id = $merchant->id;
                // $merchant_detail->stock = $input['stock'][$i];
                // $merchant_detail->on_booking = $input['on_booking'][$i];
                $merchant_detail->rate = $input['rate_detail'][$i];
                $merchant_detail->description = $input['description'][$i];
                
                if ($request->file('photo_detail')) {
                    $files = $input['photo_detail'][$i];
                    if ($files) {
                        $destinationPath    = 'uploads/attachment/merchant_details/'; // The destination were you store the image.
                        if(!(file_exists(public_path('/uploads/attachment/merchant_details/'))))
                        {
                            File::makeDirectory($destinationPath, $mode = 0777, true, true);
                        }
                        $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                        $mime_type          = $files->getMimeType(); // Gets this example image/png
                        $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                        $filename           = time().'-'.$filename; // random file name to replace original
                        $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                        $merchant_detail->photo = $destinationPath.'/'.$filename;
                    }
                }
                
                $merchant_detail->save();
                $i++;
            }
        \Session::flash('status','Merchant berhasil dibuat');
        return redirect("admin/merchants");
    }
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Request $request, $id)
    {   
        $merchant = Merchant::find($id);
        $merchant_types = MerchantType::all();
        $states = State::all();
        $districts = District::all();
        $merchant_member = User::where('user_group_id', 2)->get();
        
        return view("admins.merchants.edit",compact(["merchant", "merchant_types", "states", "districts", "merchant_member"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {              
        $input = $request->all();
        
        $merchant = merchant::find($input["id"]);
        $merchant->name = $input['name'];
        $merchant->merchant_type_id = $input['merchant_type_id'];
        $merchant->state_id = $input['state_id'];
        $merchant->district_id = $input['district_id'];
        $merchant->user_id = $input['user_id'];
        $merchant->rate = $input['rate'];
        $merchant->address = $input['address'];
        $merchant->facilities = $input['facilities'];
        if ($request->file('photo')) {
            $files = $input['photo'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/merchants/'; // The destination were you store the image.
                if(!(file_exists(public_path('/uploads/attachment/merchants/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $merchant->photo = $destinationPath.'/'.$filename;
            }
        }
        $merchant->save();
        
        \Session::flash('status','merchant berhasil diubah');
        return redirect("admin/merchants");
    }
    
    public function show($id)
    {
        $merchant = merchant::find($id);
        return view('admins.merchants.show', compact(["merchant"]));
    }

    public function delete(Request $request, $id)
    { 
        $merchant = merchant::find($id);
        $merchant->delete();

        return redirect("admin/merchants");
    }
}
