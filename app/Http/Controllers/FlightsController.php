<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Flight;
use App\Model\State;
use App\Model\District;
use App\Model\Airport;
use App\Model\Plane;
use App\User;
use File;

class FlightsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $flights = Flight::all();
        return view("admins.flights.index",compact(["flights"]));
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index_frontend()
    {
        $flights = Hotel::all();
        return view("flights.index",compact(["flights"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $departures = Airport::all();
        $arrivals = Airport::all();
        $states = State::all();
        $planes = Plane::all();
        
        return view("admins.flights.create",compact(["departures", "arrivals", "states", "planes"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();        

        $flight = new Flight;
        $flight->departure_id = $input['departure_id'];
        $flight->arrival_id = $input['arrival_id'];
        $flight->code = $input['code'];
        $flight->plane_id = $input['plane_id'];
        $flight->flight_date_departure = date("Y-m-d", strtotime($input['flight_date_departure']));
        $flight->flight_date_arrival = date("Y-m-d", strtotime($input['flight_date_arrival']));
        $flight->flight_time_departure = $input['flight_time_departure'];
        $flight->flight_time_arrival = $input['flight_time_arrival'];
        $flight->duration = $input['duration'];
        $flight->rate = $input['rate'];
        $flight->facilities = $input['facilities'];
        $flight->save();
            
        \Session::flash('status','Penerbangan berhasil dibuat');
        return redirect("admin/flights");
    }
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Request $request, $id)
    {   
        $flight = Flight::find($id);
        $departures = Airport::all();
        $arrivals = Airport::all();
        $states = State::all();
        $planes = Plane::all();
        
        return view("admins.flights.edit",compact(["flight", "departures", "arrivals", "states", "planes"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {              
        $input = $request->all();

        $flight = Flight::find($input["id"]);
        $flight->departure_id = $input['departure_id'];
        $flight->arrival_id = $input['arrival_id'];
        $flight->code = $input['code'];
        $flight->plane_id = $input['plane_id'];
        $flight->flight_date_departure = date("Y-m-d", strtotime($input['flight_date_departure']));
        $flight->flight_date_arrival = date("Y-m-d", strtotime($input['flight_date_arrival']));
        $flight->flight_time_departure = $input['flight_time_departure'];
        $flight->flight_time_arrival = $input['flight_time_arrival'];
        $flight->duration = $input['duration'];
        $flight->rate = $input['rate'];
        $flight->facilities = $input['facilities'];
        $flight->save();
        
        \Session::flash('status','hotel berhasil diubah');
        return redirect("admin/flights");
    }
    
    public function show($id)
    {
        $flight = Flight::find($id);
        return view('admins.flights.show', compact(["flight"]));
    }

    public function delete(Request $request, $id)
    { 
        $hotel = Hotel::find($id);
        $hotel->delete();

        return redirect("admin/flights");
    }
}
