<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Hotel;
use App\Model\HotelDetails;
use App\Model\RoomType;
use App\Model\State;
use App\Model\District;
use App\User;
use File;

class HotelsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $hotels = Hotel::all();
        return view("admins.hotels.index",compact(["hotels"]));
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index_frontend()
    {
        $hotels = Hotel::all();
        return view("hotels.index",compact(["hotels"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $room_types = RoomType::all();
        $states = State::all();
        $districts = District::all();
        $members = User::where('user_group_id', 2)->get();
        
        return view("admins.hotels.create",compact(["room_types", "states", "districts", "members"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();
        
        $hotel = new Hotel;
        $hotel->name = $input['name'];
        $hotel->star = $input['star'];
        $hotel->contact = $input['phone'];
        $hotel->email = $input['email'];
        $hotel->state_id = $input['state_id'];
        $hotel->district_id = $input['district_id'];
        $hotel->user_id = $input['user_id'];
        $hotel->rate = 0;
        $hotel->address = $input['address'];
        $hotel->facility_hotel = $input['facility_hotel'];
        $hotel->facility_room = 'facility_room';
        if ($request->file('photo_hotel')) {
        $files = $input['photo_hotel'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/hotels/'; // The destination were you store the image.
                if(!(file_exists(public_path('/uploads/attachment/hotels/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $hotel->photo = $destinationPath.'/'.$filename;
            }
        }
        $hotel->save();
        
            $i = 0;
            $count_looping = count($input['room_type_id']);
            for ($x = 0; $x < $count_looping; $x++) {
                $hotel_detail = new HotelDetails;
                $hotel_detail->hotel_id = $hotel->id;
                $hotel_detail->room_type_id = $input['room_type_id'][$i];
                $hotel_detail->capacity = $input['capacity'][$i];
                $hotel_detail->on_booking = 0;
                $hotel_detail->room_capacity = $input['room_capacity'][$i];
                $hotel_detail->rate = $input['rate_room'][$i];   
                
                if ($request->file('photo')[$i]) {       
                    $files = $input['photo'][$i];
                    if ($files) {
                        $destinationPath    = 'uploads/attachment/hotels/'; // The destination were you store the image.
                        if(!(file_exists(public_path('/uploads/attachment/hotels/'))))
                        {
                            File::makeDirectory($destinationPath, $mode = 0777, true, true);
                        }
                        $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                        $mime_type          = $files->getMimeType(); // Gets this example image/png
                        $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                        $filename           = time().'-'.$filename; // random file name to replace original
                        $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                        $hotel_detail->photo = $destinationPath.'/'.$filename;
                    }
                }
                
                $hotel_detail->save();
                $i++;
            }
            
        \Session::flash('status','Hotel berhasil dibuat');
        return redirect("admin/hotels");
    }
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Request $request, $id)
    {   
        $hotel = Hotel::find($id);
        $states = State::all();
        $districts = District::all();
        $hotel_member = User::where('user_group_id', 2)->get();
        $room_types = RoomType::all();
        
        return view("admins.hotels.edit",compact(["hotel", "states", "districts", "hotel_member", "room_types"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {              
        $input = $request->all();
        
        $hotel = Hotel::find($input["id"]);
        $hotel->name = $input['name'];
        if ($input['star'] != 0){
            $hotel->star = $input['star'];   
        }        
        $hotel->contact = $input['contact'];
        $hotel->email = $input['email'];
        $hotel->state_id = $input['state_id'];
        $hotel->district_id = $input['district_id'];
        $hotel->user_id = $input['user_id'];
        $hotel->rate = $input['rate'];
        $hotel->address = $input['address'];
        $hotel->facility_hotel = $input['facility_hotel'];
        $hotel->facility_room = $input['facility_room'];
        if ($request->file('photo_hotel')) {
        $files = $input['photo_hotel'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/hotels/'; // The destination were you store the image.
                if(!(file_exists(public_path('/uploads/attachment/hotels/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $hotel->photo = $destinationPath.'/'.$filename;
            }
        }
        $hotel->save();
        
        \Session::flash('status','hotel berhasil diubah');
        return redirect("admin/hotels");
    }
    
    public function show($id)
    {
        $hotel = Hotel::find($id);
        return view('admins.hotels.show', compact(["hotel"]));
    }

    public function delete(Request $request, $id)
    { 
        $hotel = Hotel::find($id);
        $hotel->delete();

        return redirect("admin/hotels");
    }
}
