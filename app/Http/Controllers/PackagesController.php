<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\State;
use App\Model\District;
use App\Model\Flight;
use App\Model\Hotel;
use App\Model\Merchant;
use App\Model\Package;
use App\Model\PackageDetail;
use File;

class PackagesController extends Controller
{    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $packages = Package::all();
        return view("admins.packages.index",compact(["packages"]));
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index_frontend()
    {
        $packages = Hotel::all();
        return view("packages.index",compact(["packages"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $states = State::all();
        $districts = District::all();
        $flights = Flight::all();
        $hotels = Hotel::all();
        $merchants_wisata = Merchant::where('merchant_type_id', 2)->get();       
        $merchants_culinary = Merchant::where('merchant_type_id', 1)->get();       
        return view("admins.packages.create",compact(["states", "districts", "flights", "hotels", "merchants_wisata", "merchants_culinary"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();

        $package = new Package;
        $package->name = $input['name'];
        $package->state_id = $input['state_id'];
        $package->district_id = $input['district_id'];
        $package->duration = $input['duration'];
        $package->start_date = date("Y-m-d", strtotime($input['start_date']));
        $package->end_date = date("Y-m-d", strtotime($input['ent_date']));
        $package->description = $input['additional_note'];
        if ($request->file('photo')) {
        $files = $input['photo'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/packages/'; // The destination were you store the image.
                if(!(file_exists(public_path('/uploads/attachment/packages/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $package->photo = $destinationPath.'/'.$filename;
            }
        }
        $package->save();
        
        	//input detail package wisata
            $i = 0;
            $rate_all = 0;
            $merchant_wisata_count = count($input['merchant_wisata_id']);
            for ($x = 0; $x < $merchant_wisata_count; $x++) {
                $package_detail = new PackageDetail;
                $package_detail->package_id = $package->id;
                $package_detail->merchant_id = $input['merchant_wisata_id'][$i];                
                $merchant = Merchant::find($input['merchant_wisata_id'][$i]);
                $package_detail->rate = $merchant->rate; 
                $package_detail->save();
                $rate_all = $rate_all + $package_detail->rate;
                $i++;
            }  

            //input detail package culinary
            $j = 0;
            $merchant_culinary_id = count($input['merchant_culinary_id']);
            for ($x = 0; $x < $merchant_culinary_id; $x++) {
                $package_detail = new PackageDetail;
                $package_detail->package_id = $package->id;
                $package_detail->merchant_id = $input['merchant_culinary_id'][$j];                
                $merchant = Merchant::find($input['merchant_culinary_id'][$j]);
                $package_detail->rate = $merchant->rate; 
                $package_detail->save();
                $rate_all = $rate_all + $package_detail->rate;
                $j++;
            }

            //input detail package flight departure
            $package_detail = new PackageDetail;
            $package_detail->package_id = $package->id;
            $package_detail->departure_flight_id = $input['departure_flight_id'];
            $flight = Flight::find($input['departure_flight_id']);
            $package_detail->rate = $flight->rate;
            $rate_all = $rate_all + $package_detail->rate;
            $package_detail->save();

            //input detail package flight arrival
            $package_detail = new PackageDetail;
            $package_detail->package_id = $package->id;
            $package_detail->arrival_flight_id = $input['arrival_flight_id'];
            $flight = Flight::find($input['arrival_flight_id']);
            $package_detail->rate = $flight->rate;
            $rate_all = $rate_all + $package_detail->rate;
            $package_detail->save();

            //input detail package flight arrival
            $package_detail = new PackageDetail;
            $package_detail->package_id = $package->id;
            $package_detail->hotel_id = $input['hotel_id'];
            $hotel = Hotel::find($input['hotel_id']);
            $package_detail->rate = $hotel->rate;
            $rate_all = $rate_all + $package_detail->rate;
            $package_detail->save();

        $package->rate = $rate_all;
        $package->save();
                      
        \Session::flash('status','Hotel berhasil dibuat');
        return redirect("admin/packages");
    }
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Request $request, $id)
    {   
        $package = Package::find($id);
        $states = State::all();
        $districts = District::all();
        $flights = Flight::all();
        $hotels = Hotel::all();
        $merchants_wisata = Merchant::where('merchant_type_id', 2)->get();       
        $merchants_culinary = Merchant::where('merchant_type_id', 1)->get();

        return view("admins.packages.edit",compact(["package", "states", "districts", "flights", "hotels", "merchants_wisata", "merchants_culinary"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {              
        $input = $request->all();
        
        $package = Package::find($input["id"]);
        $package->name = $input['name'];
        $package->state_id = $input['state_id'];
        $package->district_id = $input['district_id'];
        $package->duration = $input['duration'];
        $package->start_date = date("Y-m-d", strtotime($input['start_date']));
        $package->end_date = date("Y-m-d", strtotime($input['ent_date']));
        $package->description = $input['additional_note'];
        if ($request->file('photo')) {		
        	$files = $input['photo'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/packages/'; // The destination were you store the image.
                if(!(file_exists(public_path('/uploads/attachment/packages/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $package->photo = $destinationPath.'/'.$filename;
            }
        }
        $package->save();
        
        	//input detail package wisata
            $i = 1;
            $rate_all = 0;
            $merchant_wisata_count = count($input['merchant_wisata_id']);
            for ($x = 0; $x < $merchant_wisata_count; $x++) {
                $package_detail = PackageDetail::where('package_id', $package->id)->where('merchant_id', $input['merchant_wisata_id'][$i])->first();
                $package_detail->package_id = $package->id;
                $package_detail->merchant_id = $input['merchant_wisata_id'][$i];                
                $merchant = Merchant::find($input['merchant_wisata_id'][$i]);
                $package_detail->rate = $merchant->rate; 
                $package_detail->save();
                $rate_all = $rate_all + $package_detail->rate;
                $i++;
            }  

            //input detail package culinary
            $j = 1;
            $merchant_culinary_id = count($input['merchant_culinary_id']);
            for ($x = 0; $x < $merchant_culinary_id; $x++) {
                $package_detail = PackageDetail::where('package_id', $package->id)->where('merchant_id', $input['merchant_culinary_id'][$j])->first();
                $package_detail->package_id = $package->id;
                $package_detail->merchant_id = $input['merchant_culinary_id'][$j];
                $merchant = Merchant::find($input['merchant_culinary_id'][$j]);
                $package_detail->rate = $merchant->rate; 
                $package_detail->save();
                $rate_all = $rate_all + $package_detail->rate;
                $j++;
            }

            //input detail package flight departure
            $package_detail = PackageDetail::where('package_id', $package->id)->where('departure_flight_id', $input['departure_flight_id'])->first();
            $package_detail->package_id = $package->id;
            $package_detail->departure_flight_id = $input['departure_flight_id'];
            $flight = Flight::find($input['departure_flight_id']);
            $package_detail->rate = $flight->rate;
            $rate_all = $rate_all + $package_detail->rate;
            $package_detail->save();

            //input detail package flight arrival
            $package_detail = PackageDetail::where('package_id', $package->id)->where('arrival_flight_id', $input['arrival_flight_id'])->first();
            $package_detail->package_id = $package->id;
            $package_detail->arrival_flight_id = $input['arrival_flight_id'];
            $flight = Flight::find($input['arrival_flight_id']);
            $package_detail->rate = $flight->rate;
            $rate_all = $rate_all + $package_detail->rate;
            $package_detail->save();

            //input detail package flight arrival
            $package_detail = PackageDetail::where('package_id', $package->id)->where('hotel_id', $input['hotel_id'])->first();
            $package_detail->package_id = $package->id;
            $package_detail->hotel_id = $input['hotel_id'];
            $hotel = Hotel::find($input['hotel_id']);
            $package_detail->rate = $hotel->rate;
            $rate_all = $rate_all + $package_detail->rate;
            $package_detail->save();

        $package->rate = $rate_all;
        $package->save();
        
        \Session::flash('status','Package berhasil diubah');
        return redirect("admin/packages");
    }
    
    public function show($id)
    {
        $package = Package::find($id);
        $states = State::all();
        $districts = District::all();
        $flights = Flight::all();
        $hotels = Hotel::all();
        $merchants_wisata = Merchant::where('merchant_type_id', 2)->get();       
        $merchants_culinary = Merchant::where('merchant_type_id', 1)->get();

        return view('admins.packages.show', compact(["package", "flights", "hotels", "merchants_wisata", "merchants_culinary"]));
    }

    public function delete(Request $request, $id)
    { 
        $package = Package::find($id);
        $package->delete();

        return redirect("admin/packages");
    }
}
