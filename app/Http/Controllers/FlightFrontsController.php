<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Hotel;
use App\Model\Flight;
use App\Model\HotelDetails;
use App\Model\State;
use App\Model\District;
use App\Model\Airport;
use App\Model\Order;
use App\Model\OrderDetail;
use File;
use Auth;

class FlightFrontsController extends Controller
{
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departure_id = $_GET['departure_id'];                
        $arrival_id = $_GET['arrival_id'];
        $flight_date_departure = $_GET['flight_date_departure'];        
        //$flight_date_arrival = $_GET['flight_date_arrival'];

        if($departure_id != 0 && $arrival_id != 0 && $flight_date_departure){            
            $flights = Flight::where('departure_id', $departure_id)->where('arrival_id', $arrival_id)->where('flight_date_departure', $flight_date_departure)->get();
        }else{
            $flights = Flight::all();
        }

        $districts = District::all();        
        $departures = Airport::all();
        $arrivals = Airport::all();

        return view("flights.index",compact(["districts", "flights", "departures", "arrivals"]));
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function booking($id)
    {
        $flight = Flight::find($id);
        $order = new Order;
        $payment_methods = $order->payment_method();
        $order_last = Order::latest('created_at')->first();
        if ($order_last) {
            $increament_invoice = (int)explode("/", $order_last->invoice)[3] + 1;
            $invoice = 'WK/OR/'.date('Y').'/'.$increament_invoice;
        }else{
            $invoice = 'WK/OR/'.date('Y').'/0';
        }

        return view("flights.show",compact(["flight", "payment_methods", "invoice"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();
        $flight = Flight::find($input['flight_id']);

        $order = new Order;
        $order->invoice = $input['invoice'];
        $order->order_by = Auth::user()->id;
        $order->order_date = date("Y-m-d");
        $order->required_date = date("Y-m-d", strtotime($input['flight_date_departure']));                
        $order->status_order = 1;
        $order->payment_method = $input['payment_method'];
        $order->discount = 0;        
        $order->additional_note = $input['additional_note'];                
        $order->save();

            $order_detail = new OrderDetail;
            $order_detail->order_id = $order->id;
            $order_detail->flight_id = $input['flight_id'];
            $order_detail->quantity_order = $input['total'];
            $order_detail->unit_price = $flight->rate;
            $order_detail->total_price = $order_detail->unit_price * $order_detail->quantity_order;
            $order_detail->date_order_detail = date("Y-m-d");            
            $order_detail->save();

        $order->subtotal = $order_detail->total_price;
        $order->save();

        \Session::flash('status','Pernerbangan Berhasil Di Pesan');
        return redirect("/myorders");
    }
}
