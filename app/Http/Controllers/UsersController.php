<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\UserGroup;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::all();
        return view("admins.users.index",compact(["users"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $user_groups = UserGroup::all();        
        
        return view("admins.users.create",compact(["user_groups"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();

        $user = new user;
        $user->name = $input['name'];
        $user->user_group_id = $input['user_group_id'];
        $user->nik = $input['nik'];
        $user->email = $input['email'];
        $user->birthdate = date("Y-m-d", strtotime($input['birthdate']));
        $user->password = Hash::make($input['password']);
        $user->save();
        
        \Session::flash('status','user berhasil dibuat');
        return redirect("admin/users");
    }
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Request $request, $id)
    {   
        $user = User::find($id);
        $user_groups = UserGroup::all();        
        
        return view("admins.users.edit",compact(["user", "user_groups"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {              
        $input = $request->all();
        
        $user = User::find($input["id"]);
        $user->name = $input['name'];
        $user->user_group_id = $input['user_group_id'];
        $user->nik = $input['nik'];
        $user->email = $input['email'];
        $user->birthdate = date("Y-m-d", strtotime($input['birthdate']));
        if($input['password']){
            $user->password = Hash::make($input['password']);   
        }        
        $user->save();
        
        \Session::flash('status','User berhasil diubah');
        return redirect("admin/users");
    }
    
    public function show($id)
    {
        $user = User::find($id);
        return view('admins.users.show', compact(["user"]));
    }

    public function delete(Request $request, $id)
    { 
        $user = user::find($id);
        $user->delete();

        return redirect("admin/users");
    }
}
