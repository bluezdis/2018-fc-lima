<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Hotel;
use App\Model\HotelDetails;
use App\Model\State;
use App\Model\District;
use App\Model\RoomType;
use App\Model\Merchant;
use App\Model\MerchantDetail;
use App\Model\Order;
use App\Model\OrderDetail;
use File;
use Auth;

class MerchantFrontsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index_frontend()
    {        
        $merchant_type_id = $_GET['merchant_type_id'];
        $district_id = $_GET['district_id'];        

        if($merchant_type_id == 0){
            $merchants = Merchant::all();
        }else{
            if ($district_id == 0) {
                $merchants = Merchant::where('merchant_type_id', $merchant_type_id)->get();
            }else{
                $merchants = Merchant::where('merchant_type_id', $merchant_type_id)->where('district_id', $district_id)->get();
            }            
        }

        $merchants_popular =  Merchant::inRandomOrder()->limit(4)->get();

        $districts = District::all();
        return view("merchants.index",compact(["merchants", "districts", "merchants_popular"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function booking($id)
    {        
        $merchant = Merchant::find($id);
        $order = new Order;
        $payment_methods = $order->payment_method();
        $order_last = Order::latest('created_at')->first();
        if ($order_last) {
            $increament_invoice = (int)explode("/", $order_last->invoice)[3] + 1;
            $invoice = 'WK/OR/'.date('Y').'/'.$increament_invoice;
        }else{
            $invoice = 'WK/OR/'.date('Y').'/0';
        }
        $merchant_details = MerchantDetail::where('merchant_id', $merchant->id)->get();

        return view("merchants.show",compact(["merchant", "payment_methods", "invoice", "merchant_details"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();        
        $merchant = Merchant::find($input['merchant_id']);

        $order = new Order;
        $order->invoice = $input['invoice'];
        $order->order_by = Auth::user()->id;
        $order->order_date = date("Y-m-d");
        $order->required_date = date("Y-m-d", strtotime($input['order_date']));                
        $order->status_order = 1;
        $order->payment_method = $input['payment_method'];
        $order->discount = 0;        
        $order->additional_note = $input['additional_note'];                
        if($merchant->merchant_type_id == 2){
            $order->subtotal = $merchant->rate * $input['total'];
        }
        $order->save();

            if($merchant->merchant_type_id == 2){
                $order_detail = new OrderDetail;
                $order_detail->order_id = $order->id;
                $order_detail->merchant_id = $input['merchant_id'];
                $order_detail->quantity_order = $input['total'];
                $order_detail->unit_price = $merchant->rate;
                $order_detail->total_price = $merchant->rate * $input['total'];
                $order_detail->date_order_detail = date("Y-m-d");        
                $order_detail->hotel_remarks = $input['additional_note'];
                $order_detail->save();
            }else{
                $i = 0;
                $total_detail_order = 0;
                $count_looping = count($input['merchant_culinary_id']);
                for ($x = 0; $x < $count_looping; $x++) {
                    $merchant_detail = MerchantDetail::find($input['merchant_culinary_id'][$x]);
                    $order_detail = new OrderDetail;
                    $order_detail->order_id = $order->id;
                    $order_detail->merchant_id = $input['merchant_id'];
                    $order_detail->quantity_order = $input['total'][$x];
                    $order_detail->unit_price = $merchant_detail->rate;
                    $order_detail->total_price = $merchant_detail->rate * $input['total'][$x];
                    $order_detail->merchant_detail_id = $input['merchant_culinary_id'][$x];
                    $order_detail->date_order_detail = date("Y-m-d");
                    $order_detail->save();

                    $total_detail_order = $total_detail_order + $order_detail->total_price;
                }

                $order->subtotal = $total_detail_order;
                $order->save();
            }

        \Session::flash('status','Hotel berhasil dibuat');
        return redirect("/myorders");
    }
}
