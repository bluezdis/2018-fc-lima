<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Merchant;
use App\Model\MerchantDetail;
use App\Model\MerchantType;
use App\Model\State;
use App\Model\District;
use App\User;
use File;

class MerchantDetailsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $merchant_id = $_GET['merchant_id'];
        $merchant = Merchant::find($merchant_id);
        $merchant_member = User::where('user_group_id', 2)->get();   
        return view("admins.merchant_details.create",compact(["merchant", "merchant_member"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();

        $merchant_detail = new MerchantDetail;
        $merchant_detail->name = $input['name'];
        $merchant_detail->merchant_id = $input['merchant_id'];
        // $merchant_detail->stock = $input['stock'];
        // $merchant_detail->on_booking = $input['on_booking'];
        $merchant_detail->rate = $input['rate'];
        $merchant_detail->description = $input['description'];
        //$input['photo'] = 'photo';
        $files = $input['photo'];
                if ($files != 'photo') {
                    $destinationPath    = 'uploads/attachment/merchant_detail/'; // The destination were you store the image.
                    if(!(file_exists(public_path('/uploads/attachment/merchant_detail/'))))
                    {
                        File::makeDirectory($destinationPath, $mode = 0777, true, true);
                    }
                    $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                    $mime_type          = $files->getMimeType(); // Gets this example image/png
                    $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                    $filename           = time().'-'.$filename; // random file name to replace original
                    $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                    $merchant_detail->photo = $destinationPath.'/'.$filename;
                }
        $merchant_detail->save();
                
        \Session::flash('status','Merchant Detail berhasil dibuat');                                                                             
        return redirect("admin/merchants/show/".$merchant_detail->merchant_id);
    }
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Request $request, $id)
    {   
        $merchant_detail = MerchantDetail::find($id);
        $merchant_types = MerchantType::all();
        $states = State::all();
        $districts = District::all();
        $merchant_member = User::where('user_group_id', 2)->get();
        
        return view("admins.merchant_details.edit",compact(["merchant_detail", "merchant_types", "states", "districts", "merchant_member"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {              
        $input = $request->all();
        
        $merchant_detail = MerchantDetail::find($input["id"]);
        $merchant_detail->name = $input['name_detail'];
        $merchant_detail->merchant_id = $merchant_detail->merchant_id;
        // $merchant_detail->stock = $input['stock'];
        // $merchant_detail->on_booking = $input['on_booking'];
        $merchant_detail->rate = $input['rate'];
        $merchant_detail->description = $input['description'];
        
        if ($request->file('photo')) {                
            $files = $input['photo'];
            if ($files != 'photo') {
                $destinationPath    = 'uploads/attachment/merchant_details/'; // The destination were you store the image.
                if(!(file_exists(public_path('/uploads/attachment/merchant_details/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $merchant_detail->photo = $destinationPath.'/'.$filename;
            }
        }
        $merchant_detail->save();
        
        \Session::flash('status','merchant_detail berhasil diubah');
        return redirect("admin/merchants/show/$merchant_detail->merchant_id");
    }
    
    public function show($id)
    {
        $merchant_detail = MerchantDetail::find($id);
        return view('admins.merchant_details.show', compact(["merchant_detail"]));
    }

    public function delete(Request $request, $id)
    { 
        $merchant_detail = MerchantDetail::find($id);
        $merchant_id = $merchant_detail->merchant_id;
        $merchant_detail->delete();

        return redirect("admin/merchants/show/".$merchant_id);
    }
}
