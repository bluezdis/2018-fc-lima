<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Hotel;
use App\Model\HotelDetails;
use App\Model\State;
use App\Model\District;
use App\Model\RoomType;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\Merchant;
use App\Model\MerchantDetail;
use File;
use Auth;

class MyOrderController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {        
        $my_order = Order::where('order_by', Auth::user()->id)->orderBy('created_at', 'DESC')->get();
        return view("my_orders.index",compact(["my_order"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detail($id)
    {        
        $order = Order::find($id);
        $payment_methods = $order->payment_method();
                
        return view("my_orders.detail_order",compact(["order", "payment_methods"]));
    }   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();
        $order = Order::find($input['order_id']);
        $order->payment_method = $input['payment_method']; 
        if ($order->payment_method == 1) {
            $order->status_order = 3;
        }else{
            $order->status_order = 2;
        }        
        $order->save();

        \Session::flash('status','Hotel berhasil dibuat');
        return redirect("/myorders");
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function proof_of_payment(Request $request)
    {
        $input = $request->all();
        $order = Order::find($input['bookId']);
        if ($order->payment_method == 2 && $order->status_order == 2) {
            $files = $input['proof_of_payment'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/my_orders/proof_of_payment'; // The destination were you store the image.
                if(!(file_exists(public_path('/uploads/attachment/my_orders/proof_of_payment'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $order->proof_of_payment = $destinationPath.'/'.$filename;
            }
        }
        $order->purchase_date = date("Y-m-d");
        $order->status_order = 3;
        $order->save();

        \Session::flash('status','Hotel berhasil dibuat');
        return redirect("/myorders");
    }
}
