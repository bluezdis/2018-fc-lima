<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Hotel;
use App\Model\HotelDetails;
use App\Model\RoomType;
use App\Model\State;
use App\Model\District;
use App\User;
use File;

class HotelDetailsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $hotel_id = $_GET['hotel_id'];
        $hotel = Hotel::find($hotel_id);
        $room_types = RoomType::all();
        $states = State::all();
        $districts = District::all();
        $members = User::where('id', 2)->get();
        
        return view("admins.hotel_details.create",compact(["hotel","room_types", "states", "districts", "members"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();
        
        $hotel_detail = new HotelDetails;
        $hotel_detail->hotel_id = $input['id'];
        $hotel_detail->room_type_id = $input['room_type_id'];
        $hotel_detail->capacity = $input['capacity'];
        $hotel_detail->on_booking = 0;
        $hotel_detail->room_capacity = $input['room_capacity'];
        $hotel_detail->swimming_pool = $input['swimming_pool'];
        $hotel_detail->gymnesium = $input['gymnesium'];
        $hotel_detail->wifi = $input['wifi'];
        $hotel_detail->room_service = $input['room_service'];
        $hotel_detail->air_condition = $input['air_condition'];
        $hotel_detail->restaurant = $input['restaurant'];
        $hotel_detail->rate = $input['rate'];        
        if ($request->file('photo')) {
            $files = $input['photo'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/hotel_details/'; // The destination were you store the image.
                if(!(file_exists(public_path('/uploads/attachment/hotel_details/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $hotel_detail->photo = $destinationPath.'/'.$filename;
            }
        }
        $hotel_detail->save();           
            
        \Session::flash('status','Hotel Detail berhasil dibuat');
        return redirect("admin/hotels/show/".$input['id']);
    }
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Request $request, $id)
    {   
        $hotel_detail = HotelDetails::find($id);        
        $room_types = RoomType::all();        
        return view("admins.hotel_details.edit",compact(["hotel_detail", "room_types"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request)
    {              
        $input = $request->all();
        
        $hotel_detail = HotelDetails::find($input["id"]);
        $hotel_detail->room_type_id = $input['room_type_id'];
        $hotel_detail->room_capacity = $input['room_capacity'];
        $hotel_detail->on_booking = $input['on_booking'];
        $hotel_detail->capacity = $input['capacity'];
        $hotel_detail->swimming_pool = $input['swimming_pool'];
        $hotel_detail->gymnesium = $input['gymnesium'];
        $hotel_detail->wifi = $input['wifi'];
        $hotel_detail->room_service = $input['room_service'];
        $hotel_detail->air_condition = $input['air_condition'];
        $hotel_detail->restaurant = $input['restaurant'];
        if ($request->file('photo')) {
            $files = $input['photo'];
            if ($files) {
                $destinationPath    = 'uploads/attachment/hotel_details/'; // The destination were you store the image.
                if(!(file_exists(public_path('/uploads/attachment/hotel_details/'))))
                {
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $filename           = $files->getClientOriginalName(); // Original file name that the end user used for it.
                $mime_type          = $files->getMimeType(); // Gets this example image/png
                $extension          = $files->getClientOriginalExtension(); // The original extension that the user used example .jpg or .png.
                $filename           = time().'-'.$filename; // random file name to replace original
                $upload_success     = $files->move($destinationPath, $filename); // Now we move the file to its new home.
                $hotel_detail->photo = $destinationPath.'/'.$filename;
            }
        }
        $hotel_detail->save();
        
        \Session::flash('status','hotel berhasil diubah');
        return redirect("admin/hotels/show/".$hotel_detail->hotel_id);
    }
    
    public function show($id)
    {
        $hotel_detail = HotelDetails::find($id);
        return view('admins.hotel_details.show', compact(["hotel_detail"]));
    }

    public function delete(Request $request, $id)
    { 
        $hotel_detail = HotelDetails::find($id);
        $hotel_id = $hotel_detail->hotel_id;
        $hotel_detail->delete();

        return redirect("admin/hotels/show/".$hotel_id);
    }
}
