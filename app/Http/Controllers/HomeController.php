<?php

namespace App\Http\Controllers;
use App\Model\Hotel;
use App\Model\HotelDetails;
use App\Model\State;
use App\Model\District;
use App\Model\RoomType;
use App\Model\Merchant;
use App\Model\Airport;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotels = Hotel::all();
        $districts = District::all();
        $room_types = RoomType::all();
        $departures = Airport::all();
        $arrivals = Airport::all();

        $merchant_culinary = Merchant::where('merchant_type_id', 1)->inRandomOrder()->limit(4)->get();
        $merchant_wisata = Merchant::where('merchant_type_id', 2)->inRandomOrder()->limit(3)->get();
        return view("home",compact(["hotels", "districts", "room_types", "merchant_culinary", "merchant_wisata", "departures", "arrivals"]));
    }
}
