<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\State;
use App\Model\District;
use App\Model\Flight;
use App\Model\Hotel;
use App\Model\Merchant;
use App\Model\Package;
use App\Model\PackageDetail;
use App\Model\Order;
use App\Model\OrderDetail;
use Auth;

class PackageFrontsController extends Controller
{
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$packages = Package::all();
        return view("packages.index",compact(["packages"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function booking($id)
    {        
        $package = Package::find($id);
        $order = new Order;
        $payment_methods = $order->payment_method();
        $order_last = Order::latest('created_at')->first();
        if ($order_last) {
            $increament_invoice = (int)explode("/", $order_last->invoice)[3] + 1;
            $invoice = 'WK/OR/'.date('Y').'/'.$increament_invoice;
        }else{
            $invoice = 'WK/OR/'.date('Y').'/0';
        }

        return view("packages.show",compact(["package", "payment_methods", "invoice"]));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function insert(Request $request)
    {
        $input = $request->all();
        $package = Package::find($input['package_id']);

        $order = new Order;
        $order->invoice = $input['invoice'];
        $order->order_by = Auth::user()->id;
        $order->order_date = date("Y-m-d");
        $order->required_date = date("Y-m-d", strtotime($input['start_date']));                
        $order->status_order = 1;
        $order->payment_method = $input['payment_method'];
        $order->discount = 0;        
        $order->additional_note = $input['additional_note'];                
        $order->package_id = $input['package_id'];
        $order->save();

            $order_detail = new OrderDetail;
            $order_detail->order_id = $order->id;            
            $order_detail->quantity_order = $input['total'];
            $order_detail->unit_price = $package->rate;            
            $order_detail->total_price = $order_detail->unit_price * $order_detail->quantity_order;
            $order_detail->date_order_detail = date("Y-m-d");            
            $order_detail->save();

        $order->subtotal = $order_detail->total_price;
        $order->save();

        \Session::flash('status','Order berhasil dibuat');
        return redirect("/myorders");
    }
}
