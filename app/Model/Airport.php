<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Airport extends Model
{
	protected $table = 'airports';

    public $timestamps = true;

    protected $guarded = [];

    public function state()
    {
        return $this->belongsTo('App\Model\State', 'state_id');
    }
    
    public function district()
    {
        return $this->belongsTo('App\Model\District', 'district_id');
    }
}
