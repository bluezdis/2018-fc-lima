<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MerchantType extends Model
{
    protected $table = 'merchant_types';

    public $timestamps = true;

    protected $guarded = [];
}
