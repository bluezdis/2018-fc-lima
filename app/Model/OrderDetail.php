<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    public function hotel()
    {
        return $this->belongsTo('App\Model\Hotel', 'hotel_id');
    }
    
    public function merchant()
    {
        return $this->belongsTo('App\Model\Merchant', 'merchant_id');
    }

    public function flight()
    {
        return $this->belongsTo('App\Model\Flight', 'flight_id');
    }

    public function merchant_detail()
    {
        return $this->belongsTo('App\Model\MerchantDetail', 'merchant_detail_id');
    }

    public function room_type()
    {
        return $this->belongsTo('App\Model\RoomType', 'hotel_room_type');
    }
}
