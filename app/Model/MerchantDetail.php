<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MerchantDetail extends Model
{
    public function merchant()
    {
        return $this->belongsTo('App\Model\Merchant', 'merchant_id');
    }
}
