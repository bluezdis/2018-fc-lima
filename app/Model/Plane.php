<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Plane extends Model
{
    protected $table = 'planes';

    public $timestamps = true;

    protected $guarded = [];
}
