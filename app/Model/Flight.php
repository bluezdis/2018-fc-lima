<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
	protected $table = 'flights';

    public $timestamps = true;

    protected $guarded = [];

    public function departure()
    {
        return $this->belongsTo('App\Model\Airport', 'departure_id');
    }
    
    public function arrival()
    {
        return $this->belongsTo('App\Model\Airport', 'arrival_id');
    }

    public function plane()
    {
        return $this->belongsTo('App\Model\Plane', 'plane_id');
    }
}
