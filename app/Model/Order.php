<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	public function status_order(){
		$status_order = [	
								1 => 'Checkout Pesanan',
								2 => 'Menunggu Pembayaran',
								3 => 'Menunggu Konfirmasi',
								4 => 'Pesanan di Proses',
								5 => 'Pesanan Selesai',
								6 => 'Pesanan di Tolak',								
								];
		return $status_order;
	}

	public function payment_method(){
		$payment_method = [
								1 => 'Cash',
								2 => 'Transfer Bank',
								3 => 'Ovo',
								];
		return $payment_method;
	}

    public function user()
    {
        return $this->belongsTo('App\User', 'order_by');
    }

    public function order_details()
    {
        return $this->hasMany('App\Model\OrderDetail');
    }

    public function package()
    {
        return $this->belongsTo('App\Model\Package', 'package_id');
    }

    
}
