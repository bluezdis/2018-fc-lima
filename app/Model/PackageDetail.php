<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PackageDetail extends Model
{
    public function package()
    {
        return $this->belongsTo('App\Model\Package', 'package_id');
    }

    public function departure()
    {
        return $this->belongsTo('App\Model\Flight', 'departure_flight_id');
    }

    public function arrival()
    {
        return $this->belongsTo('App\Model\Flight', 'arrival_flight_id');
    }

    public function hotel()
    {
        return $this->belongsTo('App\Model\Hotel', 'hotel_id');
    }

    public function merchant()
    {
        return $this->belongsTo('App\Model\Merchant', 'merchant_id');
    }
}
