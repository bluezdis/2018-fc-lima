<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public function package_details()
    {
        return $this->hasMany('App\Model\PackageDetail');
    }

    public function state()
    {
        return $this->belongsTo('App\Model\State', 'state_id');
    }
    
    public function district()
    {
        return $this->belongsTo('App\Model\District', 'district_id');
    }
}
