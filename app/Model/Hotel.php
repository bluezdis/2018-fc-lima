<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $table = 'hotels';

    public $timestamps = true;

    protected $guarded = [];
    
    
    public function state()
    {
        return $this->belongsTo('App\Model\State', 'state_id');
    }
    
    public function district()
    {
        return $this->belongsTo('App\Model\District', 'district_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function hotel_details()
    {
        return $this->hasMany('App\Model\HotelDetails');
    }
}
