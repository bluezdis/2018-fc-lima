<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class merchant extends Model
{   
    protected $table = 'merchants';

    public $timestamps = true;

    protected $guarded = [];
    
    
    public function state()
    {
        return $this->belongsTo('App\Model\State', 'state_id');
    }
    
    public function district()
    {
        return $this->belongsTo('App\Model\District', 'district_id');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function merchant_type()
    {
        return $this->belongsTo('App\Model\MerchantType', 'merchant_type_id');
    }
    
    public function merchant_details()
    {
        return $this->hasMany('App\Model\MerchantDetail');
    }
}
