<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HotelDetails extends Model
{
    public function room_type()
    {
        return $this->belongsTo('App\Model\RoomType', 'room_type_id');
    }

    public function hotel()
    {
        return $this->belongsTo('App\Model\Hotel', 'hotel_id');
    }
}
