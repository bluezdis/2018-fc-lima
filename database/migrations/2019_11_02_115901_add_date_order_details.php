<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDateOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::table('order_details', function (Blueprint $table) {
            $table->date('date_order_detail')->nullable();
            $table->date('hotel_check_in')->nullable();
            $table->date('hotel_check_out')->nullable();
            $table->integer('hotel_room_type')->nullable();            
            $table->text('hotel_remarks')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
