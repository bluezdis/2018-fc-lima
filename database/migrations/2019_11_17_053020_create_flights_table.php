<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('departure_id')->nullable();
            $table->integer('arrival_id')->nullable();
            $table->string('code')->nullable();
            $table->integer('plane_id')->nullable();
            $table->date('flight_date_departure')->nullable();
            $table->date('flight_date_arrival')->nullable();
            $table->string('flight_time_departure')->nullable();
            $table->string('flight_time_arrival')->nullable();            
            $table->integer('class_id')->nullable();
            $table->string('duration')->nullable();
            $table->bigInteger('rate')->nullable();
            $table->text('facilities')->nullable();
            $table->integer('refund')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flights');
    }
}
