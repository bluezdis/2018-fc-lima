<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice')->nullable();
            $table->integer('order_by')->nullable();
            $table->date('order_date')->nullable();            
            $table->date('required_date')->nullable();
            $table->date('purchase_date')->nullable();
            $table->integer('status_order')->nullable();            
            $table->integer('payment_method')->nullable();            
            $table->bigInteger('discount')->nullable();
            $table->bigInteger('subtotal')->nullable();            
            $table->text('additional_note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
