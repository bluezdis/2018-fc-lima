<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditHotelSetNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::table('merchants', function (Blueprint $table) {
            $table->integer('merchant_type_id')->nullable()->change();
            $table->integer('state_id')->nullable()->change();
            $table->integer('district_id')->nullable()->change();
            $table->text('facilities')->nullable()->change();                
            $table->bigInteger('rate')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->integer('user_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
