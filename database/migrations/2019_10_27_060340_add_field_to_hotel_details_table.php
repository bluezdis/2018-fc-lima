<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToHotelDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotel_details', function (Blueprint $table) {
            $table->integer('swimming_pool')->nullable();
            $table->integer('gymnesium')->nullable();
            $table->integer('wifi')->nullable();
            $table->integer('room_service')->nullable();
            $table->integer('air_condition')->nullable();
            $table->integer('restaurant')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotel_details', function (Blueprint $table) {
            //
        });
    }
}
