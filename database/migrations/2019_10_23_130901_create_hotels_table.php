<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('state_id');
            $table->integer('district_id');
            $table->integer('star');
            $table->string('name');
            $table->string('contact')->nullable();            
            $table->string('email')->nullable();
            $table->text('facility_hotel')->nullable();                
            $table->text('facility_room')->nullable();                
            $table->bigInteger('rate');
            $table->string('address');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
