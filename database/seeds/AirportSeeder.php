<?php

use Illuminate\Database\Seeder;

class AirportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('airports')->insert([
            'id' => '1',
            'state_id' => '32',            
            'district_id' => '3273',            
            'name' => 'Husein Sastra Negara',            
            'code_airport' => 'BDO',            
            'address' => 'Jalan angkasa pura no 34',            
        ]);

        DB::table('airports')->insert([
            'id' => '2',
            'state_id' => '36',            
            'district_id' => '3671',            
            'name' => 'Soekarno Hatta',            
            'code_airport' => 'JKT',            
            'address' => 'Jalan Soekarno Hatta',            
        ]);

        DB::table('airports')->insert([
            'id' => '3',
            'state_id' => '51',            
            'district_id' => '5171',            
            'name' => 'I Gusti Ngurangrai',            
            'code_airport' => 'DPS',            
            'address' => 'Jalan I Gusti',            
        ]);

        DB::table('airports')->insert([
            'id' => '4',
            'state_id' => '32',            
            'district_id' => '3210',            
            'name' => 'Kerta Jati',            
            'code_airport' => 'KJT',            
            'address' => 'Jalan Majalengka',            
        ]);

        DB::table('airports')->insert([
            'id' => '5',
            'state_id' => '63',            
            'district_id' => '7371',            
            'name' => 'Hasanudin',            
            'code_airport' => 'UJP',            
            'address' => 'Jalan Hasanudin',            
        ]);

        DB::table('airports')->insert([
            'id' => '6',
            'state_id' => '72',            
            'district_id' => '7471',            
            'name' => 'Haluleo',            
            'code_airport' => 'KDR',            
            'address' => 'Jalan Kendari',            
        ]);

        DB::table('airports')->insert([
            'id' => '7',
            'state_id' => '13',            
            'district_id' => '1371',            
            'name' => 'Minang Kabau',            
            'code_airport' => 'MKB',            
            'address' => 'Jalan Tabing',            
        ]);
        
    }
}
