<?php

use Illuminate\Database\Seeder;

class UserGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_groups')->insert([
            'id' => '1',
            'name' => 'admin',            
            'description' => 'admin',
        ]);
        
        DB::table('user_groups')->insert([
            'id' => '2',
            'name' => 'merchant',
            'description' => 'merchant',
        ]);
        
        DB::table('user_groups')->insert([
            'id' => '3',
            'name' => 'member',
            'description' => 'member',
        ]);
    }
}
