<?php

use Illuminate\Database\Seeder;

class MerhcantTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('merchant_types')->insert([
            'id' => '1',
            'name' => 'Kuliner',            
            'description' => 'menyediakan beberapa tempat kuliner',
        ]);
        
        DB::table('merchant_types')->insert([
            'id' => '2',
            'name' => 'Wisata Foto',
            'description' => 'Menyediakan beberapa tempat wisata foto',
        ]);
    }
}
