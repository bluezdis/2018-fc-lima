<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'email_admin@gmail.com',
            'password' => bcrypt('admin123'),
            'birthdate' => '1990-01-01',
            'nik' => '12315345',
            'user_group_id' => '1'
        ]);
    }
}
