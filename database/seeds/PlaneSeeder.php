<?php

use Illuminate\Database\Seeder;

class PlaneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('planes')->insert([
            'id' => '1',
            'name' => 'Garuda Indonesia',
            'plane_code' => 'JT-387',            
            'merk' => 'Boing',            
            'model' => 'Boing',            
            'production_year' => '2018',            
            'capacity' => '223',           
            'logo' => 'frontend/img/maskapai/garuda_indonesia.png',
        ]);

        DB::table('planes')->insert([
            'id' => '2',
            'name' => 'Citilink',
            'plane_code' => 'JT-386',            
            'merk' => 'Boing',            
            'model' => 'Boing',            
            'production_year' => '2018',            
            'capacity' => '125',
            'logo' => 'frontend/img/maskapai/citilink.png',
        ]);

        DB::table('planes')->insert([
            'id' => '3',
            'name' => 'Citilink',
            'plane_code' => 'JT-383',            
            'merk' => 'Boing',            
            'model' => 'Wings',            
            'production_year' => '2018',            
            'capacity' => '90', 
            'logo' => 'frontend/img/maskapai/citilink.png',           
        ]);

        DB::table('planes')->insert([
            'id' => '4',
            'name' => 'Garuda Indonesia',
            'plane_code' => 'JT-380',            
            'merk' => 'Boing',            
            'model' => 'Wings',            
            'production_year' => '2018',            
            'capacity' => '60',
            'logo' => 'frontend/img/maskapai/garuda_indonesia.png',     
        ]);

        DB::table('planes')->insert([
            'id' => '5',
            'name' => 'Air Asia',
            'plane_code' => 'SU-387',            
            'merk' => 'Sukhoi',            
            'model' => 'Boing',            
            'production_year' => '2018',            
            'capacity' => '240',
            'logo' => 'frontend/img/maskapai/air_asia.png',                 
        ]);

        DB::table('planes')->insert([
            'id' => '6',
            'name' => 'Lion Air',
            'plane_code' => 'SU-386',            
            'merk' => 'Sukhoi',            
            'model' => 'Boing',            
            'production_year' => '2018',            
            'capacity' => '150', 
            'logo' => 'frontend/img/maskapai/lion_air.png',
        ]);

        DB::table('planes')->insert([
            'id' => '7',
            'name' => 'Lion Air',
            'plane_code' => 'SU-384',            
            'merk' => 'Sukhoi',            
            'model' => 'Wings',            
            'production_year' => '2018',            
            'capacity' => '86',
            'logo' => 'frontend/img/maskapai/lion_air.png',
        ]);

        DB::table('planes')->insert([
            'id' => '8',
            'name' => 'Lion Air',
            'plane_code' => 'SU-380',            
            'merk' => 'Sukhoi',            
            'model' => 'Wings',            
            'production_year' => '2018',            
            'capacity' => '50',
            'logo' => 'frontend/img/maskapai/lion_air.png',
        ]);
    }
}
