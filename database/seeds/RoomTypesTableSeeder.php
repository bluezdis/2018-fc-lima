<?php

use Illuminate\Database\Seeder;

class RoomTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('room_types')->insert([
            'id' => '1',
            'name' => 'standard',            
            'description' => 'room standard',
        ]);
        
        DB::table('room_types')->insert([
            'id' => '2',
            'name' => 'superior',            
            'description' => 'room superior',
        ]);
        
        DB::table('room_types')->insert([
            'id' => '3',
            'name' => 'deluxe',            
            'description' => 'room deluxe',
        ]);
        
        DB::table('room_types')->insert([
            'id' => '4',
            'name' => 'suite',            
            'description' => 'room suite',
        ]);
        
        DB::table('room_types')->insert([
            'id' => '5',
            'name' => 'studio',            
            'description' => 'room studio',
        ]);
    }
}
