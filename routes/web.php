<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/flight', 'FlightFrontsController@index')->name('flight');
Route::get('/flight/booking/{id}', 'FlightFrontsController@booking')->name('booking')->middleware('auth');
Route::post('/flight/booking/insert', 'FlightFrontsController@insert')->middleware('auth');
Route::get('/merchant', 'MerchantFrontsController@index_frontend')->name('merchant');
Route::get('/merchant/booking/{id}', 'MerchantFrontsController@booking')->name('booking')->middleware('auth');
Route::post('/merchant/booking/insert', 'MerchantFrontsController@insert')->middleware('auth');
Route::get('/packages', 'PackageFrontsController@index')->name('packages');
Route::get('/packages/booking/{id}', 'PackageFrontsController@booking')->name('packages')->middleware('auth');
Route::post('/packages/booking/insert', 'PackageFrontsController@insert');
Route::get('/hotel', 'HotelFrontsController@index_frontend')->name('hotel');
Route::get('/hotel/booking', 'HotelFrontsController@booking')->name('booking')->middleware('auth');
Route::post('/hotel/booking/insert', 'HotelFrontsController@insert');
Route::get('/myorders', 'MyOrderController@index')->name('my_order')->middleware('auth');
Route::get('/myorder/detail/{id}', 'MyOrderController@detail')->name('detail_order')->middleware('auth');
Route::post('/myorder/insert', 'MyOrderController@insert');
Route::post('/myorder/proof_of_payment', 'MyOrderController@proof_of_payment');

Route::group(["prefix" => "admin"], function(){
    Route::get('/', 'AdminController@index');
    
    /*
        route merchants
    */
    Route::get('/merchants', 'MerchantsController@index');
    Route::get('/merchants/create', 'MerchantsController@create');
    Route::get('/merchants/show/{id}', 'MerchantsController@show');
    Route::get('/merchants/edit/{id}', 'MerchantsController@edit');
    Route::get('/merchants/delete/{id}', 'MerchantsController@delete');
    Route::post('/merchants/insert', 'MerchantsController@insert');
    Route::post('/merchants/update', 'MerchantsController@update');
        /*
        route merchant_details
        */
        Route::get('/merchant_details', 'MerchantDetailsController@index');
        Route::get('/merchant_details/create', 'MerchantDetailsController@create');
        Route::get('/merchant_details/show/{id}', 'MerchantDetailsController@show');
        Route::get('/merchant_details/edit/{id}', 'MerchantDetailsController@edit');
        Route::get('/merchant_details/delete/{id}', 'MerchantDetailsController@delete');
        Route::post('/merchant_details/insert', 'MerchantDetailsController@insert');
        Route::post('/merchant_details/update', 'MerchantDetailsController@update');
    
    /*
        route users
    */
    Route::get('/users', 'UsersController@index');
    Route::get('/users/create', 'UsersController@create');
    Route::get('/users/show/{id}', 'UsersController@show');
    Route::get('/users/edit/{id}', 'UsersController@edit');
    Route::get('/users/delete/{id}', 'UsersController@delete');
    Route::post('/users/insert', 'UsersController@insert');
    Route::post('/users/update', 'UsersController@update');
    
    /*
        route hotels
    */
    Route::get('/hotels', 'HotelsController@index');
    Route::get('/hotels/create', 'HotelsController@create');
    Route::get('/hotels/show/{id}', 'HotelsController@show');
    Route::get('/hotels/edit/{id}', 'HotelsController@edit');
    Route::get('/hotels/delete/{id}', 'HotelsController@delete');
    Route::post('/hotels/insert', 'HotelsController@insert');
    Route::post('/hotels/update', 'HotelsController@update');
        /*
        route hotel_details
        */
        Route::get('/hotel_details', 'HotelDetailsController@index');
        Route::get('/hotel_details/create', 'HotelDetailsController@create');
        Route::get('/hotel_details/show/{id}', 'HotelDetailsController@show');
        Route::get('/hotel_details/edit/{id}', 'HotelDetailsController@edit');
        Route::get('/hotel_details/delete/{id}', 'HotelDetailsController@delete');
        Route::post('/hotel_details/insert', 'HotelDetailsController@insert');
        Route::post('/hotel_details/update', 'HotelDetailsController@update');

    /*
        route orders
    */
    Route::get('/orders', 'OrdersController@index');
    Route::get('/orders/create', 'OrdersController@create');
    Route::get('/orders/show/{id}', 'OrdersController@show');
    Route::get('/orders/edit/{id}', 'OrdersController@edit');
    Route::get('/orders/delete/{id}', 'OrdersController@delete');
    Route::post('/orders/insert', 'OrdersController@insert');
    Route::post('/orders/update', 'OrdersController@update');
    Route::get('/orders/merchant_details/{merchant_id}', 'OrdersController@get_merchant_detail');
    Route::get('/orders/process/{id}', 'OrdersController@process');
    Route::get('/orders/completed/{id}', 'OrdersController@completed');
    Route::get('/orders/reject/{id}', 'OrdersController@reject');
        /*
        route order_details
        */
        Route::get('/order_details', 'OrdersController@index');
        Route::get('/order_details/create', 'OrdersController@create');
        Route::get('/order_details/show/{id}', 'OrdersController@show');
        Route::get('/order_details/edit/{id}', 'OrdersController@edit');
        Route::get('/order_details/delete/{id}', 'OrdersController@delete');
        Route::post('/order_details/insert', 'OrdersController@insert');
        Route::post('/order_details/update', 'OrdersController@update');   

    /*
        route flights
    */
    Route::get('/flights', 'FlightsController@index');
    Route::get('/flights/create', 'FlightsController@create');
    Route::get('/flights/show/{id}', 'FlightsController@show');
    Route::get('/flights/edit/{id}', 'FlightsController@edit');
    Route::get('/flights/delete/{id}', 'FlightsController@delete');
    Route::post('/flights/insert', 'FlightsController@insert');
    Route::post('/flights/update', 'FlightsController@update');
    Route::get('/flights/merchant_details/{merchant_id}', 'FlightsController@get_merchant_detail');
    Route::get('/flights/process/{id}', 'FlightsController@process');
    Route::get('/flights/completed/{id}', 'FlightsController@completed');
    Route::get('/flights/reject/{id}', 'FlightsController@reject');     

    /*
        route flights
    */
    Route::get('/packages', 'PackagesController@index');
    Route::get('/packages/create', 'PackagesController@create');
    Route::get('/packages/show/{id}', 'PackagesController@show');
    Route::get('/packages/edit/{id}', 'PackagesController@edit');
    Route::get('/packages/delete/{id}', 'PackagesController@delete');
    Route::post('/packages/insert', 'PackagesController@insert');
    Route::post('/packages/update', 'PackagesController@update');
    Route::get('/packages/merchant_details/{merchant_id}', 'PackagesController@get_merchant_detail');
    Route::get('/packages/process/{id}', 'PackagesController@process');
    Route::get('/packages/completed/{id}', 'PackagesController@completed');
    Route::get('/packages/reject/{id}', 'PackagesController@reject');     

});