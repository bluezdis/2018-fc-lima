@extends('layouts.app')

@section('content')
<!-- start banner Area -->
<section class="banner-area relative">
    <div class="overlay overlay-bg"></div>				
    <div class="container">
        <div class="row fullscreen align-items-center justify-content-between">
            <div class="col-lg-6 col-md-6 banner-left">
                <h6 class="text-white">Go your own way!</h6>
                <h1 class="text-white">Wisata Kota</h1>
                <!-- <p class="text-white">
                    Arrive and revive
                </p> -->
            </div>
            <div class="col-lg-4 col-md-6 banner-right">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li>
                    <a class="nav-link active" id="flight-tab" data-toggle="tab" href="#flight" role="tab" aria-controls="flight" aria-selected="true">Flights</a>
                  </li>
                  <li>
                    <a class="nav-link" id="hotel-tab" data-toggle="tab" href="#hotel" role="tab" aria-controls="hotel" aria-selected="false">Hotels</a>
                  </li>
                  <li>
                    <a class="nav-link" id="holiday-tab" data-toggle="tab" href="#holiday" role="tab" aria-controls="holiday" aria-selected="false">Holidays</a>
                  </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active" id="flight" role="tabpanel" aria-labelledby="flight-tab">
                    <form class="form-wrap" method="GET" action="{{ url('flight') }}">                  
                        <select class="form-control select2" id="departure_id" name="departure_id" style="width: 100%;">
                            <option selected="selected" value="">Pilih Bandara Keberangkatan <i class="fa fa-globe" aria-hidden="true"></i></option>
                            @foreach($departures as $departure)
                                <option value="{{$departure->id}}">{{$departure->name}} - {{$departure->code_airport}}</option>
                            @endforeach                                
                        </select>
                        <br>
                        <br>
                        <select class="form-control select2" id="arrival_id" name="arrival_id" style="width: 100%;">
                            <option selected="selected" value="">Pilih Bandara Kedatangan <i class="fa fa-globe" aria-hidden="true"></i></option>
                            @foreach($arrivals as $arrival)
                                <option value="{{$arrival->id}}">{{$arrival->name}} - {{$arrival->code_airport}}</option>
                            @endforeach                                
                        </select>
                        <br>
                        <br>                        
                        <input type="date" class="form-control" id="datepicker" name="flight_date_departure" placeholder="Tanggal Keberangkatan">
                        <button class="primary-btn text-uppercase" >Cari Sekarang</button>
                    </form>
                  </div>
                  <div class="tab-pane fade" id="hotel" role="tabpanel" aria-labelledby="hotel-tab">
                    <form class="form-wrap" method="GET" action="{{ url('hotel') }}">
                        <select class="form-control select2" name="district_id" style="width: 100%;">
                            <option selected="selected" >Pilih Kota Hotel <i class="fa fa-globe" aria-hidden="true"></i></option>
                            @foreach($districts as $district)
                                <option value="{{$district->id}}">{{$district->name}}</option>
                            @endforeach                                
                        </select>	
                        <br>
                        <br>						                        
                        <select class="form-control select2" id="room_type_id" name="room_type_id" style="width: 100%;">
                            <option selected="selected" >Pilih Tipe kamar <i class="fa fa-globe" aria-hidden="true"></i></option>
                            @foreach($room_types as $room_types)
                                <option value="{{$room_types->id}}">{{$room_types->name}}</option>
                            @endforeach                                
                        </select>
                        <br>
                        <br>
                        <button class="primary-btn text-uppercase" >Cari Sekarang</button>
                    </form>							  	
                  </div>
                  <div class="tab-pane fade" id="holiday" role="tabpanel" aria-labelledby="holiday-tab">
                    <form class="form-wrap" method="GET" action="/merchant?merchant_type_id=0">
                        <select class="form-control select2" name="district_id" style="width: 100%;">
                        <option selected="selected" value="0">Pilih Kota <i class="fa fa-globe" aria-hidden="true"></i></option>
                            @foreach($districts as $district)
                                <option value="{{$district->id}}">{{$district->name}}</option>
                            @endforeach                                
                        </select>

                        <br>
                        <br>

                        <select class="form-control select2" id="merchant_type_id" name="merchant_type_id" style="width: 100%;">
                            <option selected="selected" value="0">Pilih Jenis Merchane <i class="fa fa-globe" aria-hidden="true"></i></option>
                            <option value="2">Wisata Kota</option>
                            <option value="1">Wisata Kuliner</option>                                           
                        </select>                       
                        <br>
                        <br>
                        <button class="primary-btn text-uppercase" >Cari Sekarang</button>
                    </form>							  	
                  </div>
                </div>
            </div>
        </div>
    </div>					
</section>
<!-- End banner Area -->

<!-- Start popular-destination Area -->
<section class="popular-destination-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Wisata Kota Popular</h1>
                </div>
            </div>
        </div>						
        <div class="row">
            @foreach($merchant_wisata as $merchant_wisata)            
            <div class="col-lg-4">
                <div class="single-destination relative">
                    <div class="thumb relative">
                        <div class="overlay overlay-bg"></div>
                        @if($merchant_wisata->photo)
                            <img class="img-fluid" src="{{ url($merchant_wisata->photo) }}" alt="" style="width:350px;height:197px;">
                        @else
                            <img class="img-fluid" src="frontend/img/no_image.png" alt="">
                        @endif                        
                    </div>
                    <div class="desc">	
                        <a href="/merchant/booking/{{$merchant_wisata->id}}" class="price-btn">Rp.{{ number_format($merchant_wisata->rate,2,',','.') }}</a>
                        <h4>{{$merchant_wisata->name}}</h4>
                        <p>{{$merchant_wisata->state->name}}</p>			
                    </div>
                </div>
            </div>
            @endforeach           											
        </div>
    </div>	
</section>
<!-- End popular-destination Area -->

<!-- Start other-issue Area -->
<section class="other-issue-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-9">
                <div class="title text-center">
                    <h1 class="mb-10">Wisata Kuliner Populer</h1>
                </div>
            </div>
        </div>					
        <div class="row">
            @foreach($merchant_culinary as $merchant_culinary)            
            <div class="col-lg-4">
                <div class="single-destination relative">
                    <div class="thumb relative">
                        <div class="overlay overlay-bg"></div>
                        @if($merchant_culinary->photo)
                            <img class="img-fluid" src="{{ url($merchant_culinary->photo) }}" alt="" style="width:350px;height:197px;">
                        @else
                            <img class="img-fluid" src="frontend/img/no_image.png" alt="">
                        @endif                                                
                    </div>
                    <div class="desc">  
                        <a href="/merchant/booking/{{$merchant_culinary->id}}" class="price-btn">Rp.{{ number_format($merchant_culinary->rate,2,',','.') }}</a>          
                        <h4>{{$merchant_culinary->name}}</h4>
                        <p>{{$merchant_culinary->state->name}}</p>          
                    </div>
                </div>
            </div>
            @endforeach																	
        </div>
    </div>	
</section>
<!-- End other-issue Area -->
@endsection
