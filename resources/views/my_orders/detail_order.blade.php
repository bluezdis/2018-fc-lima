@extends('layouts.app')

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $("#quantity_order").val(function(){            
            var quantity_order = parseInt($('#quantity_order').val());
            var rate = parseInt($('#rate_data').val());
            var total = quantity_order * rate;
            $("#total").val("Rp."+numberWithCommas(parseInt(total))+",00");
        });

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }    
    });

</script>
@endsection

@section('content')
<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">				
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    Konfrim Order 
                </h1>	
                <p class="text-white link-nav"><a href="#">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="#"> Order</a></p>
            </div>	
        </div>
    </div>
</section>
<!-- End banner Area -->	

<!-- Start destinations Area -->
<section class="destinations-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-40 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Order Detail</h1>
                </div>
            </div>            
        </div>       
        <div class="row">                      
            <div class="col-lg-6">
                <div class="single-destinations">
                    @if($order->order_details->first()->hotel_id)
                     <div class="details">
                        <div class="thumb">
                            <img src="{{url($order->order_details->first()->hotel->hotel_details->first()->photo)}}" alt="" style="width:350px;height:205px;">
                        </div>
                        <h4 class="d-flex justify-content-between">
                            <span>Nama Hotel</span>
                            {{$order->order_details->first()->hotel->name}} 
                        </h4>
                        <ul class="package-list">
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Lokasi Hotel</span>
                                {{$order->order_details->first()->hotel->state->name}} - {{$order->order_details->first()->hotel->district->name}} - {{$order->order_details->first()->hotel->address}}   
                            </li>                            
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Kontak Hotel</span>
                                {{ $order->order_details->first()->hotel->contact }} - {{$order->order_details->first()->hotel->email}}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Swimming pool</span>
                                @if($order->order_details->first()->hotel->hotel_details->first()->swimming_pool == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif                                                                
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Gymnesium</span>
                                @if($order->order_details->first()->hotel->hotel_details->first()->gymnesium == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Wi-fi</span>
                                @if($order->order_details->first()->hotel->hotel_details->first()->wifi == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Room Service</span>
                                @if($order->order_details->first()->hotel->hotel_details->first()->room_service == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Air Condition</span>
                                @if($order->order_details->first()->hotel->hotel_details->first()->air_condition == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Restaurant</span>
                                @if($order->order_details->first()->hotel->hotel_details->first()->restaurant == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif
                            </li>                                               
                        </ul>
                    </div>
                    @elseif($order->order_details->first()->merchant_id)
                    <div class="thumb">
                        @if($order->order_details->first()->merchant->merchant_type_id == 2)
                        <img class="img-fluid" src="{{ url($order->order_details->first()->merchant->merchant_details->first()->photo) }}" alt="">
                        @elseif($order->order_details->first()->merchant->merchant_type_id == 1)
                        <img class="img-fluid" src="{{ url($order->order_details->first()->merchant->photo) }}" alt="">
                        @else
                        <img src="frontend/img/no_image.png" alt="">
                        @endif                        
                    </div>
                    <div class="details">
                        <h4 class="d-flex justify-content-between">
                            <span>nama merchant</span>
                            {{$order->order_details->first()->merchant->name}}
                        </h4>
                        <ul class="package-list">
                            @foreach($order->order_details as $order_detail)    
                                @php 
                                $merchant_details = DB::table('merchant_details')->find($order_detail->merchant_detail_id);
                                @endphp  
                                @if($order_detail->merchant->merchant_type_id == 2)
                                <li class="d-flex justify-content-between align-items-center">
                                    <span>Fasilitas</span>
                                    {{ $order_detail->merchant->facilities }}
                                </li>
                                <li class="d-flex justify-content-between align-items-center">
                                    <span>Jumlah Pesanan Tike</span>
                                    {{ $order_detail->quantity_order}}
                                </li>
                                <li class="d-flex justify-content-between align-items-center">
                                    <span>Harga Tiket Masuk</span>
                                    Rp. {{ number_format($order_detail->unit_price,2,',','.') }}
                                </li>
                                @elseif($order_detail->merchant->merchant_type_id == 1)
                                <li class="d-flex justify-content-between align-items-center">
                                    <span>Nama Menu</span>
                                    {{$merchant_details->name}}                                     
                                </li>
                                <li class="d-flex justify-content-between align-items-center">
                                <span> </span>
                                    Rp. {{number_format($merchant_details->rate,2,',','.')}} x {{$order_detail->quantity_order}} = Rp. {{number_format($order_detail->total_price,2,',','.')}}
                                </li>
                                @endif                            
                            @endforeach
                        </ul>
                    </div>
                    @elseif($order->order_details->first()->flight_id)
                    <div class="thumb">
                        <img class="img-fluid" src="{{ url($order->order_details->first()->flight->plane->logo) }}" alt="">
                    </div>
                    <div class="details">
                        <h4 class="d-flex justify-content-between">
                            <span>{{$order->order_details->first()->flight->plane->name}}</span>
                        </h4>
                        <ul class="package-list">
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Kode Penerbangan</span>
                                {{ $order->order_details->first()->flight->code }}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Maskapai</span>
                                {{ $order->order_details->first()->flight->plane->name }} - {{ $order->order_details->first()->flight->plane->plane_code }}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Tanggal & Jam Keberangkatan</span>
                                {{ date("d-M-Y", strtotime($order->order_details->first()->flight->flight_date_departure)) }} - {{$order->order_details->first()->flight->flight_time_departure}}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Tanggal & Jam Keberangkatan</span>
                                {{$order->order_details->first()->flight->duration}}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Bandara Keberangkatan</span>
                                {{ $order->order_details->first()->flight->departure->name }} - {{ $order->order_details->first()->flight->departure->code_airport }}
                            </li>                            
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Bandara Kedatangan</span>
                                {{ $order->order_details->first()->flight->arrival->name }} - {{ $order->order_details->first()->flight->arrival->code_airport }} 
                            </li>           
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Harga Tiket </span>
                                <input type="text" class="price-btn" id="rate" name="rate" value="Rp.{{ number_format($order->order_details->first()->flight->rate,2,',','.') }}" readonly="" >
                                <input type="hidden" name="rate_data" id="rate_data" value="{{$order->order_details->first()->flight->rate}}">
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Total Biaya</span>               

                                <input type="text" class="price-btn" id="total" name="total" value="Rp. {{number_format($order->subtotal,2,',','.')}}" readonly="" >
                            </li>                            
                        </ul>
                    </div>
                    @elseif($order->package_id)
                    <div class="details">
                        <div class="thumb">                        
                            <img class="img-fluid" src="{{ url($order->package->photo) }}" alt="">
                        </div>
                        <h4 class="d-flex justify-content-between">
                            <span>{{$order->package->name}}</span>
                        </h4>
                        <ul class="package-list">
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Lokasi</span>
                                {{ $order->package->state->name }} - {{ $order->package->district->name }}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Bandara Keberangkatan</span>                                
                                @foreach($order->package->package_details as $package_detail)
                                    @if($package_detail->departure_flight_id)
                                    <span>{{$package_detail->departure->code}} - {{$package_detail->departure->plane->name}}</span>
                                    @endif
                                @endforeach
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Bandara Kepulangan</span>                                
                                @foreach($order->package->package_details as $package_detail)
                                    @if($package_detail->arrival_flight_id)
                                    <span>{{$package_detail->arrival->code}} - {{$package_detail->arrival->plane->name}}</span>
                                    @endif
                                @endforeach
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Tanggal</span>
                                {{ date("d-M-Y", strtotime($order->package->start_date)) }} sampai {{ date("d-M-Y", strtotime($order->package->end_date)) }}
                            </li>                            
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Deskripsi Paket</span>
                                {{ $order->package->description }}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Harga Per Tiket</span>
                                <input type="text" class="price-btn" id="rate" name="rate" value="Rp.{{ number_format($order->package->rate,2,',','.') }}" readonly="" >
                                <input type="hidden" name="rate_data" id="rate_data" value="{{$order->package->rate}}">
                            </li>
                        </ul>
                    </div>
                    @endif
                </div>
            </div>                        

            <div class="col-lg-6">
	            <div class="section-top-border">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<h3 class="mb-30">Konfrim Order Anda</h3>
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('myorder/insert') }}">
                                @csrf
                                <input type="hidden" name="order_id" id="order_id" value="{{$order->id}}">
                                <div class="mt-10">
                                    <input type="text" class="form-control" id="invoice" name="invoice" value="{{$order->invoice}}" readonly="" placeholder="Enter Nama Order">
                                    Nomor Invoice
                                </div>                                
                                <div class="mt-10">
                                    <input type="date" class="form-control" id="datepicker" name="order_date" placeholder="Tanggal Pemesanan" value="{{$order->order_date}}" readonly="">
                                    Tanggal Pemesanan 
                                </div>
                                <div class="mt-10">
                                    <input type="text" class="form-control" name="quantity_order" placeholder="Jumlah Pesanan Tiket" value="{{ $order->order_details->first()->quantity_order }}" readonly="">
                                    Jumlah Tiket Yang di Pesan
                                </div>                                
								<div class="mt-10">
									<input type="text" class="form-control" id="total" name="total" placeholder="Jumlah Tiket yang di pesan" value="Rp. {{number_format($order->subtotal,2,',','.')}}" readonly="">
                                    Biasa yang harus di bayar
								</div>
								<div class="input-group-icon mt-10">
									<div class="icon"><i class="fa fa-money" aria-hidden="true"></i></div>
									<div class="form-select" id="default-select">
										<select id="payment_method" name="payment_method">
											<option value="1">Piih Tipe Pembayaran</option>
											@php
				                              $i = 1;
				                             @endphp
				                              @foreach($payment_methods as $payment_method)
                                                @if($order->payment_method == $i)
                                                <option value="{{$i}}" selected="">{{$payment_method}}</option>
                                                @else
                                                <option value="{{$i}}">{{$payment_method}}</option>
                                                @endif				                                
				                                @php
				                                $i = $i + 1;
				                                @endphp
				                              @endforeach											
										</select>
									</div>
								</div>
								<div class="mt-10">
								<textarea class="form-control" rows="3" id="additional_note" name="additional_note" placeholder="Masukan Keterangan Order">{{$order->additional_note}}</textarea>								
								</div>
																                                

                                <div class="mt-10">
                                    <button type="submit" class="genric-btn success radius">Submit</button>
                                </div>

							</form>
						</div>					
					</div>
				</div>
			</div>            

        </div>
    </div>	
</section>
<!-- End destinations Area -->
@endsection