@extends('layouts.app')

@section('js')
<script type="text/javascript">
    $('#my_modal').on('show.bs.modal', function(e) {
        var bookId = $(e.relatedTarget).data('book-id');
        $(e.currentTarget).find('input[name="bookId"]').val(bookId);
    });
</script>
@endsection

@section('content')
<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">				
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    List Order Anda
                </h1>	
            </div>	
        </div>
    </div>
</section>
<!-- End banner Area -->

<!-- Start about-info Area -->
<section class="about-info-area section-gap">
    <div class="container">       
        <div class="section-top-border">                                    
            <table id="example1" class="table table-bordered table-striped" width="1200">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Invoice</th>
                  <th>Tanggal Order</th>
                  <th>Merchant / Hotel / Flight</th>
                  <th>Status Order</th>
                  <th>Total Pembayaran</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @php
                  $order_data = new App\Model\Order;;
                  $status_orders = $order_data->status_order();
                  $i = 1;
                @endphp
                @foreach($my_order as $order)
                <tr>
                  <td>{{$i}}</td>
                  <td>{{ $order->invoice }}</td>
                  <td>{{ date('d-M-Y', strtotime($order->order_date)) }}</td>              
                  <td>
                    @if($order->order_details->first()['hotel_id'])
                      {{$order->order_details->first()->hotel['name']}}
                    @elseif($order->order_details->first()['merchant_id'])
                      {{$order->order_details->first()->merchant['name']}}
                    @elseif($order->order_details->first()['flight_id'])
                      {{$order->order_details->first()->flight['code']}} - {{$order->order_details->first()->flight->plane['name']}} - {{$order->order_details->first()->flight->plane['plane_code']}}
                    @elseif($order->package_id)
                      {{$order->package_id ? $order->package->name : '-'}}
                    @endif
                  </td>
                  <td>
                    {{ $status_orders[$order->status_order] }}
                    @if($order->payment_method == 2 &&  $order->status_order == 3 && $order->proof_of_payment)
                    <br>
                    <a href="{{$order->proof_of_payment}}" class="img-gal">Lihat bukti tranfer</a>
                    @endif
                  </td>
                  <td>Rp. {{ number_format($order->subtotal,2,',','.') }}</td>
                  <td>                      
                    <div class="button-group-area mt-10">
                      @if($order->status_order == 1)
                      <a href="/myorder/detail/{{$order->id}}" class="genric-btn info small">Konfrim Pesanan</a>
                      @endif                      
                      @if($order->payment_method == 2 &&  $order->status_order == 2 && !$order->proof_of_payment)
                      <a href="#my_modal" data-toggle="modal" data-book-id="{{$order->id}}" class="genric-btn primary small">
                      Upload Bukti Pembayaran</a>                      
                      @endif
                      @if($order->status_order == 4)
                      <blockquote class="generic-blockquote">
                        <center> 
                          Selamat Pesanan Anda Telah Berhasil <br> 
                          Untuk Info Hubungi Kami
                        </center>
                      </blockquote>                      
                      @endif
                      
                    </div>
                  </td>          
                </tr>
                @php
                $i = $i + 1;
                @endphp
                @endforeach             
                </tbody>
                <tfoot>
                <tr>
                  <th>NO</th>
                  <th>Invoice</th>
                  <th>Tanggal Order</th>
                  <th>Merchant / Hotel / Flight</th>
                  <th>Status Order</th>
                  <th>Total Pembayaran</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>               
        </div>
    </div>	    
</section>
<!-- End about-info Area -->

<div class="modal" id="my_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Upload Bukti Tranfer</h4>
      </div>
      <form class="form-horizontal" role="form" method="POST" action="{{ url('myorder/proof_of_payment') }}" enctype="multipart/form-data">
          <div class="modal-body">        
                @csrf            
                <input type="hidden" name="bookId" value="">
                <input type="file" class="form-control" id="proof_of_payment" name="proof_of_payment" placeholder="Upload Bukti Tranfer">
          </div>
          <div class="modal-footer">
            <a href="/hotel?district_id=0&room_type_id=0" class="btn btn-info" data-dismiss="modal">Batalkan</a>
            <button type="submit" class="btn btn-success">Upload</button>
          </div>
      </form>
    </div>
  </div>
</div>



@endsection