@extends('layouts.app')

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $("#quantity_order").val(function(){            
            var quantity_order = parseInt($('#quantity_order').val());
            var rate = parseInt($('#rate_data').val());
            var total = quantity_order * rate;
            $("#total").val("Rp."+numberWithCommas(parseInt(total))+",00");
        });

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        var x = 0;
        $("#add_merchant_culinary").click(function(){
   
            ++x;      
   
            $("#dinamic_form_merchant_culinary").append('</br><div class="row"><div class="col-lg-4 col-md-12"><div class="form-select" id="default-select"><select  id="merchant_culinary_id['+x+']" name="merchant_culinary_id['+x+']"><option selected="selected" >Pilih Menu</option>@foreach($merchant_details as $merchant_detail)<option value="{{$merchant_detail->id}}">{{$merchant_detail->name}}</option>@endforeach</select></div></div><div class="col-lg-4 col-md-12"><input type="text" class="form-control" id="total['+x+']" name="total['+x+']" placeholder="Jumlah yang di pesan" value="" ></div><div class="col-lg-4 col-md-12"></div></div></br>');
        });
    });

</script>
@endsection

@section('content')
<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">				
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    Merchane
                </h1>	
                <p class="text-white link-nav"><a href="#">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="#"> Merchane</a></p>
            </div>	
        </div>
    </div>
</section>
<!-- End banner Area -->	

<!-- Start destinations Area -->
<section class="destinations-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-40 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Boking Wisata Kota</h1>
                </div>
            </div>            
        </div>       
        <div class="row">                 
            <div class="col-lg-6">
            	<div class="single-destinations">
                    <div class="thumb">
                        @if($merchant->merchant_type_id == 1)
                            @if($merchant->merchant_details->first()->photo)
                                <img class="img-fluid" src="{{ url($merchant->merchant_details->first()->photo) }}" alt="">
                            @else
                                <img class="img-fluid" src="frontend/img/no_image.png" alt="">
                            @endif                              
                        @elseif($merchant->merchant_type_id == 2)
                            @if($merchant->photo)
                                <img class="img-fluid" src="{{ url($merchant->photo) }}" alt="">
                            @else
                                <img class="img-fluid" src="frontend/img/no_image.png" alt="">
                            @endif                              
                        @else
                            <img src="frontend/img/no_image.png" alt="">
                        @endif                        
                    </div>
                    <div class="details">
                        <h4 class="d-flex justify-content-between">
                            <span>{{$merchant->name}} - {{$merchant->merchant_type->name}}</span>                              	
                            <div class="star">
                                @for ($i = 1; $i <= $merchant->star; $i++)
                                <span class="fa fa-star checked"></span>    
                                @endfor                                
                            </div>	
                        </h4>
                        <ul class="package-list">
                        	<li class="d-flex justify-content-between align-items-center">
                                <span>Lokasi Merchane</span>
                                {{ $merchant->address }} - {{$merchant->state->name}} - {{$merchant->district->name}}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Fasilitas</span>
                                {{ $merchant->facilities }}
                            </li>
                            @if($merchant->merchant_type_id == 2)
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Harga Tiket Masuk</span>
                                Rp. {{ number_format($merchant->rate,2,',','.') }}
                            </li>                            
                            @endif								
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
	            <div class="section-top-border">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<h3 class="mb-30">Isi Form Berikut</h3>
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('merchant/booking/insert') }}">
                                @csrf
                                <input type="hidden" name="merchant_id" id="merchant_id" value="{{$merchant->id}}">
                                <div class="mt-10">
                                    <input type="text" class="form-control" id="invoice" name="invoice" value="{{$invoice}}" readonly="" placeholder="Enter Nama Order">
                                    Nomor Invoice
                                </div>                                
                                <div class="mt-10">
                                    <input type="date" class="form-control" id="datepicker" name="order_date" placeholder="Tanggal Pemesanan" >
                                    Tanggal Pemesanan 
                                </div>
                                @if($merchant->merchant_type_id == 2)
								<div class="mt-10">
									<input type="text" class="form-control" id="total" name="total" placeholder="Jumlah Tiket yang di pesan" value="" >
                                    Jumlah Tiket
								</div>
                                @endif
								<div class="input-group-icon mt-10">
									<div class="icon"><i class="fa fa-money" aria-hidden="true"></i></div>
									<div class="form-select" id="default-select">
										<select id="payment_method" name="payment_method">
											<option value="1" selected="">Piih Tipe Pembayaran</option>
											@php
				                              $i = 1;
				                             @endphp
				                              @foreach($payment_methods as $payment_method)
				                                <option value="{{$i}}">{{$payment_method}}</option>
				                                @php
				                                $i = $i + 1;
				                                @endphp
				                              @endforeach											
										</select>
									</div>
								</div>
								<div class="mt-10">
								<textarea class="form-control" rows="3" id="additional_note" name="additional_note" placeholder="Masukan Keterangan Order"></textarea>								
								</div>
																
                                @if($merchant->merchant_type_id == 1)
                                <div class="col-lg-12">
                                    <div class="section-top-border">
                                        <h3 class="mb-30">Menu Yang di Pesan</h3>
                                        <div id="dinamic_form_merchant_culinary" >
                                            <div class="row">                        
                                                <div class="col-lg-4 col-md-12">
                                                    <div class="form-select" id="default-select">
                                                        <select  id="merchant_culinary_id[0]" name="merchant_culinary_id[0]">
                                                            <option selected="selected" >Pilih Menu</option>
                                                            @foreach($merchant_details as $merchant_detail)
                                                            <option value="{{$merchant_detail->id}}">{{$merchant_detail->name}}{{$merchant_detail->rate}}</option>
                                                            @endforeach              
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-12">
                                                    <input type="text" class="form-control" id="total[0]" name="total[0]" placeholder="Jumlah yang di pesan" value="" >
                                                </div>
                                                <div class="col-lg-4 col-md-12">
                                                    <a id="add_merchant_culinary" class="btn btn-info btn-sm"> tambah<i class="fa fa-plus"></i></a>
                                                </div>                                          
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <div class="mt-10">
                                    <button type="submit" class="genric-btn success radius">Submit</button>
                                </div>

							</form>
						</div>					
					</div>
				</div>
			</div>            

        </div>
    </div>	
</section>
<!-- End destinations Area -->
@endsection