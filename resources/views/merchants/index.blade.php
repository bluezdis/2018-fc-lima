@extends('layouts.app')

@section('content')
<!-- start banner Area -->
<section class="banner-area relative blog-home-banner" id="home">	
	<div class="overlay overlay-bg"></div>
	<div class="container">				
		<div class="row d-flex align-items-center justify-content-center">
			<div class="about-content blog-header-content col-lg-12">
				<h1 class="text-white">
					Ingin keliling Indonesia dengan mudah?
				</h1>	
				<p class="text-white">
					Nikmati wisata kuliner di berbagai kota.
				</p>
				<a href="#" class="primary-btn">View More</a>
			</div>	
		</div>
	</div>
</section>
<!-- End banner Area -->				  

<!-- Start top-category-widget Area -->
<section class="top-category-widget-area pt-90 pb-90 ">
	<div class="container">
		<div class="row">		
			<div class="col-lg-6">
				<div class="single-cat-widget">
					<div class="content relative">
						<div class="overlay overlay-bg"></div>
					    <a href="/merchant?district_id=0&merchant_type_id=2">
					      <div class="thumb">
					  		 <img class="content-image img-fluid d-block mx-auto" src="frontend/img/blog/cat-widget1.jpg" alt="">
					  	  </div>
					      <div class="content-details">
					        <h4 class="content-title mx-auto text-uppercase">Wisata Kota</h4>
					        <span></span>								        
					        <p>Enjoy your social life together</p>
					      </div>
					    </a>
					</div>
				</div>
			</div>			
			<div class="col-lg-6">
				<div class="single-cat-widget">
					<div class="content relative">
						<div class="overlay overlay-bg"></div>
					    <a href="/merchant?district_id=0&merchant_type_id=1">
					      <div class="thumb">
					  		 <img class="content-image img-fluid d-block mx-auto" src="frontend/img/blog/cat-widget3.jpg" alt="">
					  	  </div>
					      <div class="content-details">
					        <h4 class="content-title mx-auto text-uppercase">Wisata Kuliner</h4>
					        <span></span>
					        <p>Let the food be finished</p>
					      </div>
					    </a>
					</div>
				</div>
			</div>												
		</div>
	</div>	
</section>
<!-- End top-category-widget Area -->

<!-- Start post-content Area -->
<section class="post-content-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 posts-list">
				@if($merchants->count() != 0)  
				@foreach($merchants as $merchant)
				<div class="single-post row">
					<div class="col-lg-4  col-md-4 meta-details">
						<ul class="tags">
							@if($merchant->merchant_type_id == 1)
								<li><b>{{$merchant->merchant_type->name}} <span class="lnr lnr-coffee-cup"></span></b></li>
							@else
								<li><b>{{$merchant->merchant_type->name}} <span class="lnr lnr-camera"></span></b></li>
							@endif											
						</ul>
						<div class="user-details row">
							<p class="user-name col-lg-12 col-md-12 col-6">Rp.{{ number_format($merchant->rate,2,',','.')}} <span class="lnr lnr-bookmark"></span></p>
							<p class="date col-lg-12 col-md-12 col-6">{{$merchant->state->name}} <span class="lnr lnr-map"></span></p>
							<p class="view col-lg-12 col-md-12 col-6">{{$merchant->district->name}} <span class="lnr lnr-map-marker"></span></p>
							<p class="comments col-lg-12 col-md-12 col-6">{{$merchant->address}} <span class="lnr lnr-bubble"></span></p>						
						</div>
					</div>
					<div class="col-lg-8 col-md-8">
						<div class="feature-img">
							@if($merchant->merchant_type_id == 1)
								@if($merchant->merchant_details->first()->photo)
		                            <img class="img-fluid" src="{{ url($merchant->merchant_details->first()->photo) }}" alt="">
		                        @else
		                            <img class="img-fluid" src="frontend/img/no_image.png" alt="">
		                        @endif								
							@elseif($merchant->merchant_type_id == 2)
								@if($merchant->photo)
		                            <img class="img-fluid" src="{{ url($merchant->photo) }}" alt="">
		                        @else
		                            <img class="img-fluid" src="frontend/img/no_image.png" alt="">
		                        @endif							
							@endif
						</div>
						<a class="posts-title" href="#"><h3>{{$merchant->name}}</h3></a>
						<p class="excert">
							{{$merchant->facilities}}
						</p>
						<a href="merchant/booking/{{$merchant->id}}" class="primary-btn">Pesan Sekarang</a>
					</div>
				</div>				
				@endforeach
				<nav class="blog-pagination justify-content-center d-flex">
                    <ul class="pagination">
                        <li class="page-item">
                            <a href="#" class="page-link" aria-label="Previous">
                                <span aria-hidden="true">
                                    <span class="lnr lnr-chevron-left"></span>
                                </span>
                            </a>
                        </li>
                        <!-- <li class="page-item"><a href="#" class="page-link">01</a></li>
                        <li class="page-item active"><a href="#" class="page-link">02</a></li>
                        <li class="page-item"><a href="#" class="page-link">03</a></li>
                        <li class="page-item"><a href="#" class="page-link">04</a></li>
                        <li class="page-item"><a href="#" class="page-link">09</a></li>
                        <li class="page-item">
                            <a href="#" class="page-link" aria-label="Next">
                                <span aria-hidden="true">
                                    <span class="lnr lnr-chevron-right"></span>
                                </span>
                            </a>
                        </li> -->
                    </ul>
                </nav>
				@else
				<br>
	            <br>
	            <div class="col-lg-12">
	                <blockquote class="generic-blockquote danger">
	                    <h3>Data tidak di temukan</h3>
	                </blockquote>
	            </div>
				@endif                
			</div>
			<div class="col-lg-4 sidebar-widgets">
				<div class="widget-wrap">
					<div class="single-sidebar-widget search-widget">
						<form class="search-form" method="GET" action="/merchant?merchant_type_id=0">            
					        <div class="row">   
					            <div class="col-lg-12 form-group">
					                <p>Kota Tujuan</p>
					                <div class="input-group-icon mt-10">                            
				                        <select class="form-control select2" id="district_id" name="district_id" style="width: 100%;">
				                            <option selected="selected" value="0">Pilih Kota <i class="fa fa-globe" aria-hidden="true"></i></option>
				                            @foreach($districts as $district)
				                                <option value="{{$district->id}}">{{$district->name}}</option>
				                            @endforeach                                
				                        </select>
				                        <br><br>
				                        <select class="form-control select2" id="merchant_type_id" name="merchant_type_id" style="width: 100%;">
				                            <option selected="selected" value="0">Pilih Jenis Merchane <i class="fa fa-globe" aria-hidden="true"></i></option>
				                            <option value="2">Wisata Kota</option>
				                            <option value="1">Wisata Kuliner</option>				                            
				                        </select>
					                </div>
					            </div>            
					            <div class="col-lg-12">
					                <button type="submit"><i class="fa fa-search"></i></button>
					            </div>
					        </div>                
					    </form>						
					</div>					
					<div class="single-sidebar-widget popular-post-widget">
						<h4 class="popular-title">Holiday Popular</h4>
						<div class="popular-post-list">
							@foreach($merchants_popular as $merchants_popular)							
							<div class="single-post-list d-flex flex-row align-items-center">
								<div class="thumb">
									@if($merchants_popular->photo)
				                        <img class="img-fluid" src="{{ url($merchants_popular->photo) }}" alt="" style="width:151px;height:91px;">
				                    @else
				                        <img class="img-fluid" src="frontend/img/no_image.png" alt="" style="width:151px;height:91px;">
				                    @endif
								</div>
								<div class="details">
									<a href="#"><h6>{{$merchants_popular->name}}</h6></a>
									<p>Rp.{{ number_format($merchants_popular->rate,2,',','.')}}</p>
								</div>
							</div>
							@endforeach																					
						</div>
					</div>
					<div class="single-sidebar-widget ads-widget">
						<a href="#"><img class="img-fluid" src="frontend/img/blog/ads-banner.jpg" alt=""></a>
					</div>										
					
				</div>
			</div>
		</div>
	</div>	
</section>
<!-- End post-content Area -->
@endsection