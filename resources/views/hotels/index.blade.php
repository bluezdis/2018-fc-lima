@extends('layouts.app')

@section('js')
<script type="text/javascript">
    $('#my_modal').on('show.bs.modal', function(e) {
        var bookId = $(e.relatedTarget).data('book-id');
        $(e.currentTarget).find('input[name="bookId"]').val(bookId);
    });
</script>
@endsection

@section('content')
<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">             
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    Hotels              
                </h1>   
                <p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="hotels.html"> Hotels</a></p>
            </div>  
        </div>
    </div>
</section>
<!-- End banner Area -->    

<!-- Start destinations Area -->
<section class="destinations-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-40 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Pencarian Hotel</h1>
                </div>
            </div>            
        </div>
        <div class="col-lg-12">
            <form class="form-area contact-form text-right" method="GET">
                <div class="row">   
                    <div class="col-lg-3 form-group">
                        <p>Kota Tujuan</p>
                        <div class="input-group-icon mt-10">                            
                                <select class="form-control select2" id="district_id" name="district_id" style="width: 100%;">
                                    <option selected="selected" value="0">Pilih Kota <i class="fa fa-globe" aria-hidden="true"></i></option>
                                    @foreach($districts as $district)
                                        <option value="{{$district->id}}">{{$district->name}}</option>
                                    @endforeach                                
                                </select>
                        </div>
                    </div>
                    <div class="col-lg-3 form-group">
                        <p>Fasilitas Kamar</p>
                        <div class="input-group-icon mt-10">                            
                            <select class="form-control select2" id="room_type_id" name="room_type_id" style="width: 100%;">
                                <option selected="selected" value="0">Pilih Tipe kamar <i class="fa fa-globe" aria-hidden="true"></i></option>
                                @foreach($room_types as $room_types)
                                    <option value="{{$room_types->id}}">{{$room_types->name}}</option>
                                @endforeach                                
                            </select>
                        </div>
                    </div>                                        
                    <div class="col-lg-12">
                        <button class="genric-btn primary" style="float: left;">Cari Sekarang</button>
                    </div>
                </div>                
            </form> 
        </div>
        <div class="row">
        @if($hotels->count() != 0)        
        @foreach($hotels as $hotel)
            @foreach($hotel->hotel_details as $hotel_detail)
            @if($_GET['room_type_id'] == 0)
                
                <div class="col-lg-4">
                    <div class="single-destinations">
                        <div class="thumb">
                            @if($hotel_detail->photo != 'photo')
                            <img src="{{$hotel_detail->photo}}" alt="" style="width:350px;height:205px;">
                            @else
                            <img src="frontend/img/no_image.png" alt="">
                            @endif                        
                        </div>
                        <div class="details">
                            <h4 class="d-flex justify-content-between">
                                <span>{{$hotel->name}} - {{$hotel_detail->room_type->name}}</span>
                                <div class="star">
                                    @for ($i = 1; $i <= $hotel->star; $i++)
                                    <span class="fa fa-star checked"></span>    
                                    @endfor                                
                                </div>                                
                            </h4>
                            <ul class="package-list">
                                <li class="d-flex justify-content-between align-items-center" >
                                    <b>{{$hotel->state->name}} - {{$hotel->district->name}}</b>
                                </li>
                                <li class="d-flex justify-content-between align-items-center">
                                    <span>Swimming pool</span>
                                    @if($hotel_detail->swimming_pool == 1)
                                        <span>Yes</span>
                                    @else
                                        <span>No</span>
                                    @endif                                                                
                                </li>
                                <li class="d-flex justify-content-between align-items-center">
                                    <span>Gymnesium</span>
                                    @if($hotel_detail->gymnesium == 1)
                                        <span>Yes</span>
                                    @else
                                        <span>No</span>
                                    @endif
                                </li>
                                <li class="d-flex justify-content-between align-items-center">
                                    <span>Wi-fi</span>
                                    @if($hotel_detail->wifi == 1)
                                        <span>Yes</span>
                                    @else
                                        <span>No</span>
                                    @endif
                                </li>
                                <li class="d-flex justify-content-between align-items-center">
                                    <span>Room Service</span>
                                    @if($hotel_detail->room_service == 1)
                                        <span>Yes</span>
                                    @else
                                        <span>No</span>
                                    @endif
                                </li>
                                <li class="d-flex justify-content-between align-items-center">
                                    <span>Air Condition</span>
                                    @if($hotel_detail->air_condition == 1)
                                        <span>Yes</span>
                                    @else
                                        <span>No</span>
                                    @endif
                                </li>
                                <li class="d-flex justify-content-between align-items-center">
                                    <span>Restaurant</span>
                                    @if($hotel_detail->restaurant == 1)
                                        <span>Yes</span>
                                    @else
                                        <span>No</span>
                                    @endif
                                </li>                                               
                                <li class="d-flex justify-content-between align-items-center">
                                    <span>Price per night</span>
                                    <a href="#" class="price-btn">Rp.{{ number_format($hotel_detail->rate,2,',','.') }}</a>
                                </li>                                                   
                                <li class="d-flex justify-content-between align-items-center">
                                    <a href="#my_modal" data-toggle="modal" data-book-id="{{$hotel_detail->id}}" class="btn btn-primary">Pesan Hotel Sekarang</a>                                    
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            @else
                @if($hotel_detail->room_type_id == $_GET['room_type_id'])
                    <div class="col-lg-4">
                        <div class="single-destinations">
                            <div class="thumb">
                                @if($hotel_detail->photo != 'photo')
                                <img src="{{$hotel_detail->photo}}" alt="" style="width:350px;height:205px;">
                                @else
                                <img src="frontend/img/no_image.png" alt="">
                                @endif                        
                            </div>
                            <div class="details">
                                <h4 class="d-flex justify-content-between">
                                    <span>{{$hotel->name}} - {{$hotel_detail->room_type->name}}</span>                                  
                                    <div class="star">
                                        @for ($i = 1; $i <= $hotel->star; $i++)
                                        <span class="fa fa-star checked"></span>    
                                        @endfor                                
                                    </div>  
                                </h4>
                                <ul class="package-list">
                                    <li class="d-flex justify-content-between align-items-center" >
                                        <b>{{$hotel->state->name}} - {{$hotel->district->name}}</b>
                                    </li>
                                    <li class="d-flex justify-content-between align-items-center">
                                        <span>Swimming pool</span>
                                        @if($hotel_detail->swimming_pool == 1)
                                            <span>Yes</span>
                                        @else
                                            <span>No</span>
                                        @endif                                                                
                                    </li>
                                    <li class="d-flex justify-content-between align-items-center">
                                        <span>Gymnesium</span>
                                        @if($hotel_detail->gymnesium == 1)
                                            <span>Yes</span>
                                        @else
                                            <span>No</span>
                                        @endif
                                    </li>
                                    <li class="d-flex justify-content-between align-items-center">
                                        <span>Wi-fi</span>
                                        @if($hotel_detail->wifi == 1)
                                            <span>Yes</span>
                                        @else
                                            <span>No</span>
                                        @endif
                                    </li>
                                    <li class="d-flex justify-content-between align-items-center">
                                        <span>Room Service</span>
                                        @if($hotel_detail->room_service == 1)
                                            <span>Yes</span>
                                        @else
                                            <span>No</span>
                                        @endif
                                    </li>
                                    <li class="d-flex justify-content-between align-items-center">
                                        <span>Air Condition</span>
                                        @if($hotel_detail->air_condition == 1)
                                            <span>Yes</span>
                                        @else
                                            <span>No</span>
                                        @endif
                                    </li>
                                    <li class="d-flex justify-content-between align-items-center">
                                        <span>Restaurant</span>
                                        @if($hotel_detail->restaurant == 1)
                                            <span>Yes</span>
                                        @else
                                            <span>No</span>
                                        @endif
                                    </li>                                               
                                    <li class="d-flex justify-content-between align-items-center">
                                        <span>Price per night</span>
                                        <a href="#" class="price-btn">Rp.{{ number_format($hotel_detail->rate,2,',','.') }}</a>
                                    </li>  
                                    <li class="d-flex justify-content-between align-items-center">
                                        <a href="#my_modal" data-toggle="modal" data-book-id="{{$hotel_detail->id}}" class="btn btn-primary">Pesan Hotel Sekarang</a>
                                    </li>                                                 
                                </ul>
                            </div>
                        </div>
                    </div>                
                @endif
            @endif            
            @endforeach
        @endforeach     
        @else
            <br>
            <br>
            <div class="col-lg-12">
                <blockquote class="generic-blockquote danger">
                    <h3>Hotel tidak di temukan</h3>
                </blockquote>
            </div>
        @endif                                                                                                                              
        </div>
    </div>  
</section>
<!-- End destinations Area -->

<div class="modal" id="my_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Pilih Tanggal Check In & Check Out</h4>
      </div>
      <form class="form-horizontal" role="form" method="GET" action="{{ url('hotel/booking/') }}">
          <div class="modal-body">        
                @csrf            
                <input type="hidden" name="bookId" value="">
                <input type="date" class="form-control" id="datepicker" name="hotel_check_in" placeholder="Tanggal Check In">
                Tanggal Check In 
                <br>
                <br>
                <input type="date" class="form-control" id="datepicker" name="hotel_check_out" placeholder="Tanggal Check Out">
                Tanggal Check Out        
          </div>
          <div class="modal-footer">
            <a href="/hotel?district_id=0&room_type_id=0" class="btn btn-info" data-dismiss="modal">Batalkan</a>
            <button type="submit" class="btn btn-success">Konfrim Pesanan</button>
          </div>
      </form>
    </div>
  </div>
</div>


<!-- Start home-about Area -->
<!-- <section class="home-about-area">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-end">
            <div class="col-lg-6 col-md-12 home-about-left">
                <h1>
                    Did not find your Package? <br>
                    Feel free to ask us. <br>
                    We‘ll make it for you
                </h1>
                <p>
                    inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is often laughed.
                </p>
                <a href="#" class="primary-btn text-uppercase">request custom price</a>
            </div>
            <div class="col-lg-6 col-md-12 home-about-right no-padding">
                <img class="img-fluid" src="frontend/img/hotels/about-img.jpg" alt="">
            </div>
        </div>
    </div>  
</section> -->
<!-- End home-about Area -->
@endsection