@extends('layouts.app')

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $("#quantity_order").val(function(){            
            var quantity_order = parseInt($('#quantity_order').val());
            var rate = parseInt($('#rate_data').val());
            var total = quantity_order * rate;
            $("#total").val("Rp."+numberWithCommas(parseInt(total))+",00");
        });

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    });

</script>
@endsection

@section('content')
<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">				
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    Hotels				
                </h1>	
                <p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="hotels.html"> Hotels</a></p>
            </div>	
        </div>
    </div>
</section>
<!-- End banner Area -->	

<!-- Start destinations Area -->
<section class="destinations-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-40 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Boking Hotel</h1>
                </div>
            </div>            
        </div>       
        <div class="row">                 
            <div class="col-lg-6">
            	<div class="single-destinations">
                    <div class="thumb">
                        <img src="{{url($hotel_detail->photo)}}" alt="" style="width:350px;height:205px;">
                    </div>
                    <div class="details">
                        <h4 class="d-flex justify-content-between">
                            <span>{{$hotel->name}} - {{$hotel_detail->room_type->name}}</span>                              	
                            <div class="star">
                                @for ($i = 1; $i <= $hotel->star; $i++)
                                <span class="fa fa-star checked"></span>    
                                @endfor                                
                            </div>	
                        </h4>
                        <ul class="package-list">
                        	<li class="d-flex justify-content-between align-items-center">
                                <span>Lokasi Hotel</span>
                                {{ $hotel->address }} - {{$hotel->state->name}} - {{$hotel->district->name}}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Kontak Hotel</span>
                                {{ $hotel->contact }} - {{$hotel->email}}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Swimming pool</span>
                                @if($hotel_detail->swimming_pool == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif                                                                
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Gymnesium</span>
                                @if($hotel_detail->gymnesium == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Wi-fi</span>
                                @if($hotel_detail->wifi == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Room Service</span>
                                @if($hotel_detail->room_service == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Air Condition</span>
                                @if($hotel_detail->air_condition == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Restaurant</span>
                                @if($hotel_detail->restaurant == 1)
                                    <span>Yes</span>
                                @else
                                    <span>No</span>
                                @endif
                            </li>												
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Harga Per Malam </span>
                                <input type="text" class="price-btn" id="rate" name="rate" value="Rp.{{ number_format($hotel_detail->rate,2,',','.') }}" readonly="" >
                                <input type="hidden" name="rate_data" id="rate_data" value="{{$hotel_detail->rate}}">
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Total Biaya</span>               

                                <input type="text" class="price-btn" id="total" name="total" value="Rp.{{ number_format($hotel_detail->rate,2,',','.') }}" readonly="" >
                            </li>													
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
	            <div class="section-top-border">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<h3 class="mb-30">Isi Form Berikut</h3>
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('hotel/booking/insert') }}">
                                @csrf
								<input type="hidden" name="status_order" value="1">
								<input type="hidden" name="hotel_id" value="{{$hotel->id}}">
								<input type="hidden" name="room_type_id" value="{{$hotel_detail->room_type_id}}">

                                <div class="mt-10">
                                    <input type="text" class="form-control" id="datepicker" name="hotel_check_in" placeholder="Tanggal Check In" value="{{$check_in_form}}" readonly="">
                                    Tanggal Checkin 
                                </div>
                                <div class="mt-10">
                                    <input type="text" class="form-control" id="datepicker" name="hotel_check_out" placeholder="Tanggal Check Out" value="{{$check_out_form}}" readonly="">
                                    Tanggal Checkin Out
                                </div>
								<div class="mt-10">
									<input type="text" class="form-control" id="invoice" name="invoice" value="{{$invoice}}" readonly="" placeholder="Enter Nama Order">
                                    Nomor Invoice
								</div>
								<div class="mt-10">
									<input type="text" id="name" name="first_name" placeholder="First Name" value="{{Auth::user()->name}}" onfocus="this.placeholder = ''" onblur="this.placeholder = 'First Name'" required class="single-input">
                                    Nama Pemesan 
								</div>							
								<div class="mt-10">
									<input type="email" name="EMAIL" placeholder="Email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email address'" value="{{Auth::user()->email}}" required class="single-input">
                                    Email Pemesan
								</div>								
								<div class="mt-10">
									<input type="hidden" class="form-control" id="quantity_order" name="quantity_order" placeholder="Jumlah Kamar yang di pesan" value="{{$quantity_order}}" readonly="" >
                                    <input type="text" class="form-control" id="total" name="total" placeholder="Jumlah Kamar yang di pesan" value="{{$quantity_order}}" readonly="" >
                                    Durasi Menginap
								</div>
								<div class="input-group-icon mt-10">
									<textarea class="form-control" rows="3" id="address" name="address" placeholder="Masukan Alamat tinggal Anda"></textarea>									
								</div>							
								<div class="input-group-icon mt-10">
									<div class="icon"><i class="fa fa-money" aria-hidden="true"></i></div>
									<div class="form-select" id="default-select">
										<select id="payment_method" name="payment_method">
											<option value="1" selected="">Piih Tipe Pembayaran</option>
											@php
				                              $i = 1;
				                             @endphp
				                              @foreach($payment_methods as $payment_method)
				                                <option value="{{$i}}">{{$payment_method}}</option>
				                                @php
				                                $i = $i + 1;
				                                @endphp
				                              @endforeach											
										</select>
									</div>
								</div>
								<div class="mt-10">
								<textarea class="form-control" rows="3" id="additional_note" name="additional_note" placeholder="Masukan Keterangan Order"></textarea>								
								</div>
																
								<div class="mt-10">
                                    <button type="submit" class="genric-btn success radius">Submit</button>
								</div>
							</form>
						</div>					
					</div>
				</div>
			</div>

        </div>
    </div>	
</section>
<!-- End destinations Area -->
@endsection