	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="frontend/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="colorlib">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Wisata Kota</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="/frontend/css/linearicons.css">
			<link rel="stylesheet" href="/frontend/css/font-awesome.min.css">
			<link rel="stylesheet" href="/frontend/css/bootstrap.css">
			<link rel="stylesheet" href="/frontend/css/magnific-popup.css">
			<link rel="stylesheet" href="/frontend/css/jquery-ui.css">				
			<link rel="stylesheet" href="/frontend/css/nice-select.css">							
			<link rel="stylesheet" href="/frontend/css/animate.min.css">
			<link rel="stylesheet" href="/frontend/css/owl.carousel.css">				
			<link rel="stylesheet" href="/frontend/css/main.css">
			<!-- Select2 -->
  			<link rel="stylesheet" href="/backend/bower_components/select2/dist/css/select2.min.css">
  			<!-- DataTables -->
  			<link rel="stylesheet" href="/backend/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
		</head>
		<body>	
			<header id="header">
				<div class="header-top">
					<div class="container">
			  		<div class="row align-items-center">
			  			<div class="col-lg-6 col-sm-6 col-6 header-top-right">
							<div class="header-social">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-dribbble"></i></a>
								<a href="#"><i class="fa fa-behance"></i></a>
							</div>
			  			</div>
			  		</div>			  					
					</div>
				</div>
				<div class="container main-menu">
					<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				        <a href="/"><img src="/frontend/img/wk_logo2.png" alt="" title="" height="30" width="145" /></a>
				      </div>
				      <nav id="nav-menu-container">
				        <ul class="nav-menu">
				          <li><a href="/">Home</a></li>
				          <li><a href="/flight?departure_id=0&arrival_id=0&flight_date_departure=0&flight_date_arrival=0">Flight</a></li>
				          <li><a href="/merchant?district_id=0&merchant_type_id=0">Holiday</a></li>
				          <li><a href="/packages">Packages</a></li>
				          <li><a href="/hotel?district_id=0&room_type_id=0">Hotels</a></li>
                            @guest
                            <li class="menu-has-children"><a href="">Login/Register</a>
                                <ul>
                                  <li><a href="{{ route('login') }}">Login</a></li>
                                @if (Route::has('register'))
                                  <li><a href="{{ route('register') }}">Register</a></li>
                                @endif
                                </ul>                                
                            </li>
                            @else
                            <li class="menu-has-children"><a href="">Logout @if(Auth::user()->user_group_id == 1) / to admin @endif</a>
                                <ul>
                                  <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    </li>
                                    <li>
                                        @if(Auth::user()->user_group_id == 1)
                                        <a href="/admin">Page Admin</a>
                                        @endif
                                    </li>
                                    <li>
                                    	<a href="/myorders">my order</a>
                                    </li>
                                </ul>                                

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                               </li>
                            @endguest				          
				        </ul>
				      </nav><!-- #nav-menu-container -->					      		  
					</div>
				</div>
			</header><!-- #header -->
						
            
            @yield('content')						

			<!-- start footer Area -->		
			<footer class="footer-area section-gap">
				<div class="container">

					<div class="row">
						<div class="col-lg-3  col-md-6 col-sm-6">
							
						</div>
						<div class="col-lg-3 col-md-6 col-sm-6">
							
						</div>							
						<div class="col-lg-3  col-md-6 col-sm-6">
							
						</div>
						<div class="col-lg-3  col-md-6 col-sm-6">
							<div class="single-footer-widget mail-chimp">
								<h6 class="mb-20">InstaFeed</h6>
								<ul class="instafeed d-flex flex-wrap">
									<li><img src="/frontend/img/i1.jpg" alt=""></li>
									<li><img src="/frontend/img/i2.jpg" alt=""></li>
									<li><img src="/frontend/img/i3.jpg" alt=""></li>
									<li><img src="/frontend/img/i4.jpg" alt=""></li>
									<li><img src="/frontend/img/i5.jpg" alt=""></li>
									<li><img src="/frontend/img/i6.jpg" alt=""></li>
									<li><img src="/frontend/img/i7.jpg" alt=""></li>
									<li><img src="/frontend/img/i8.jpg" alt=""></li>
								</ul>
							</div>
						</div>						
					</div>

					<div class="row footer-bottom d-flex justify-content-between align-items-center">
						<p class="col-lg-8 col-sm-12 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;2019 All rights reserved Wisata Kota 2019
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
						<div class="col-lg-4 col-sm-12 footer-social">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-dribbble"></i></a>
							<a href="#"><i class="fa fa-behance"></i></a>
						</div>
					</div>
				</div>
			</footer>
			<!-- End footer Area -->	

			<script src="/frontend/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="/frontend/js/popper.min.js"></script>
			<script src="/frontend/js/vendor/bootstrap.min.js"></script>			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>		
 			<script src="/frontend/js/jquery-ui.js"></script>					
  			<script src="/frontend/js/easing.min.js"></script>			
			<script src="/frontend/js/hoverIntent.js"></script>
			<script src="/frontend/js/superfish.min.js"></script>	
			<script src="/frontend/js/jquery.ajaxchimp.min.js"></script>
			<script src="/frontend/js/jquery.magnific-popup.min.js"></script>						
			<script src="/frontend/js/jquery.nice-select.min.js"></script>					
			<script src="/frontend/js/owl.carousel.min.js"></script>							
			<script src="/frontend/js/mail-script.js"></script>	
			<script src="/frontend/js/main.js"></script>	
			<script src="/backend/bower_components/select2/dist/js/select2.full.min.js"></script>
			<!-- DataTables -->
			<script src="/backend/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
			<script src="/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
			@yield('js')
			<script>
			    $(function () {
			        $('.select2').select2();	

			        $('#example1').DataTable();		        
			    })    
			</script>

		</body>
	</html>