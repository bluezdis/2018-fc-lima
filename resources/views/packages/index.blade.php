@extends('layouts.app')

@section('content')

<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">				
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    Tour Packages				
                </h1>	
                <p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="packages.html"> Tour Packages</a></p>
            </div>	
        </div>
    </div>
</section>
<!-- End banner Area -->	

<!-- Start hot-deal Area -->
<section class="hot-deal-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Today’s Hot Deals</h1>
                    <p>We all live in an age that belongs to the young at heart. Life that is becoming extremely fast, day to.</p>
                </div>
            </div>
        </div>						
        <div class="row justify-content-center">
            <div class="col-lg-10 active-hot-deal-carusel">
                <div class="single-carusel">
                    <div class="thumb relative">
                        <div class="overlay overlay-bg"></div>
                        <img class="img-fluid" src="frontend/img/packages/hot-deal.jpg" alt="">
                    </div>
                    <div class="price-detials">
                        <a href="#" class="price-btn">Starting From <span>$250</span></a>
                    </div>
                    <div class="details">
                        <h4 class="text-white">Ancient Architecture</h4>
                        <p class="text-white">
                            Cairo, Egypt
                        </p>
                    </div>								
                </div>
                <div class="single-carusel">
                    <div class="thumb relative">
                        <div class="overlay overlay-bg"></div>
                        <img class="img-fluid" src="frontend/img/packages/hot-deal.jpg" alt="">
                    </div>
                    <div class="price-detials">
                        <a href="#" class="price-btn">Starting From <span>$250</span></a>
                    </div>
                    <div class="details">
                        <h4 class="text-white">Ancient Architecture</h4>
                        <p class="text-white">
                            Cairo, Egypt
                        </p>
                    </div>								
                </div>
                <div class="single-carusel">
                    <div class="thumb relative">
                        <div class="overlay overlay-bg"></div>
                        <img class="img-fluid" src="frontend/img/packages/hot-deal.jpg" alt="">
                    </div>
                    <div class="price-detials">
                        <a href="#" class="price-btn">Starting From <span>$250</span></a>
                    </div>
                    <div class="details">
                        <h4 class="text-white">Ancient Architecture</h4>
                        <p class="text-white">
                            Cairo, Egypt
                        </p>
                    </div>								
                </div>														
            </div>
        </div>
    </div>	
</section>
<!-- End hot-deal Area -->


<!-- Start destinations Area -->
<section class="destinations-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-40 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Paket Liburan Wisata Kota</h1>
                    <p>Berlibur tanpa harus mikir tujuan</p>
                </div>
            </div>
        </div>						
        <div class="row">
            @foreach($packages as $package)            
            <div class="col-lg-4">
                <div class="single-destinations">
                    <div class="thumb">
                        <img src="{{ url($package->photo) }}" alt="">
                    </div>
                    <div class="details">
                        <h4>{{$package->name}}</h4>
                        <p>
                            {{$package->description}}
                        </p>
                        <ul class="package-list">
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Lokasi</span>
                                <span>{{$package->state->name}} - {{$package->district->name}}</span>
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Duration</span>
                                <span>{{$package->duration}}</span>
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Tanggal Mulai</span>
                                <span>{{date("d-M-Y", strtotime($package->start_date))}}</span>
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Tanggal Selesai</span>
                                <span>{{date("d-M-Y", strtotime($package->end_date))}}</span>
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Penerbangan Keberangkatan</span>
                                @foreach($package->package_details as $package_detail)
                                    @if($package_detail->departure_flight_id)
                                    <span>{{$package_detail->departure->code}} - {{$package_detail->departure->plane->name}}</span>
                                    @endif
                                @endforeach                                
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Penerbangan Kepulangan</span>
                                @foreach($package->package_details as $package_detail)
                                    @if($package_detail->arrival_flight_id)
                                    <span>{{$package_detail->arrival->code}} - {{$package_detail->arrival->plane->name}}</span>
                                    @endif
                                @endforeach                                
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Hotel Menginap</span>
                                @foreach($package->package_details as $package_detail)
                                    @if($package_detail->hotel_id)
                                    <span>{{$package_detail->hotel->name}}</span>
                                    <h4 class="d-flex justify-content-between">
                                        <div class="star">
                                            @for ($i = 1; $i <= $package_detail->hotel->star; $i++)
                                            <span class="fa fa-star checked"></span>    
                                            @endfor                                
                                        </div>                                
                                    </h4>
                                    @endif
                                @endforeach                                
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Harga Paket</span>
                                <a href="#" class="price-btn">Rp.{{ number_format($package->rate,2,',','.') }}</a>
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <a href="/packages/booking/{{$package->id}}" class="btn btn-primary">Pesan Paket Sekarang</a>                                    
                            </li>                            
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach		
        </div>
    </div>	
</section>
<!-- End destinations Area -->
@endsection