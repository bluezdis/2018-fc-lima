@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$hotel_detail->hotel->name}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/hotel_details">Hotel Detail</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    
    <div class="box-header with-border">
      <h3 class="box-title">Form Edit Hotel Detail</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/hotel_details/update') }}" enctype="multipart/form-data">
        @csrf
        
        <input type="hidden" name="id" value="{{ $hotel_detail->id }}">
        <div class="form-group">
          <label class="col-sm-2 control-label">Room Type</label>
          <div class="col-sm-9">
            <select class="form-control select2" id="room_type_id" name="room_type_id" style="width: 100%;">
              <option selected="selected" >Pilih Jenis Kamar</option>
                @foreach($room_types as $room_type)
                    @if($room_type->id == $hotel_detail->room_type_id)
                        <option value="{{$room_type->id}}" selected="">{{$room_type->name}}</option>
                    @else
                        <option value="{{$room_type->id}}" >{{$room_type->name}}</option>
                    @endif                    
                @endforeach              
            </select>
          </div>
        </div>                                   
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Total Kamar</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="capacity" name="capacity" value="{{$hotel_detail->capacity}}" placeholder="Enter Kapasitas Kamar">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Kamar Terisi</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="on_booking" name="on_booking" value="{{$hotel_detail->on_booking}}" placeholder="Enter Kamar yang Sedang di Gunakan">
          </div>
        </div>
        
         <div class="form-group">
          <label class="col-sm-2 control-label">Kapasitas Orang per Kamar</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="room_capacity" name="room_capacity" value="{{$hotel_detail->room_capacity}}" placeholder="Enter Kapasitas Kamar">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Harga Kamar</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="rate" name="rate" value="{{$hotel_detail->rate}}" placeholder="Enter Harga Kamar">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Foto</label>
          <div class="col-sm-9">
            <input type="file" class="form-control" id="photo" name="photo" placeholder="Foto Kamar">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Swimming pool</label>
          <div class="col-sm-2">
                <label>
                    <input type="hidden" class="flat-red" name="swimming_pool" value="0">
                    @if($hotel_detail->swimming_pool == 1)
                        <input type="checkbox" class="flat-red" name="swimming_pool" value="1" checked>
                    @else
                        <input type="checkbox" class="flat-red" name="swimming_pool" value="1">
                    @endif                  
                </label>
          </div>
            <label class="col-sm-2 control-label">Gymnesium</label>
          <div class="col-sm-2">
                <label>
                    <input type="hidden" class="flat-red" name="gymnesium" value="0">
                    @if($hotel_detail->gymnesium == 1)
                        <input type="checkbox" class="flat-red" name="gymnesium" value="1" checked>
                    @else
                        <input type="checkbox" class="flat-red" name="gymnesium" value="1">
                    @endif
                  
                </label>
          </div>
            <label class="col-sm-2 control-label">Wi-fi</label>
          <div class="col-sm-2">
                <label>
                  <input type="hidden" class="flat-red" name="wifi" value="0">
                    @if($hotel_detail->wifi == 1)
                        <input type="checkbox" class="flat-red" name="wifi" value="1" checked>
                    @else
                        <input type="checkbox" class="flat-red" name="wifi" value="1">
                    @endif
                </label>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Room Service</label>
          <div class="col-sm-2">
                <label>
                    <input type="hidden" class="flat-red" name="room_service" value="0">
                    @if($hotel_detail->room_service == 1)
                        <input type="checkbox" class="flat-red" name="room_service" value="1" checked>
                    @else
                        <input type="checkbox" class="flat-red" name="room_service" value="1">
                    @endif                  
                </label>
          </div>
            <label class="col-sm-2 control-label">Air Condition</label>
          <div class="col-sm-2">
                <label>
                  <input type="hidden" class="flat-red" name="air_condition" value="0">
                    @if($hotel_detail->air_condition == 1)
                        <input type="checkbox" class="flat-red" name="air_condition" value="1" checked>
                    @else
                        <input type="checkbox" class="flat-red" name="air_condition" value="1">
                    @endif
                </label>
          </div>
            <label class="col-sm-2 control-label">Restaurant</label>
          <div class="col-sm-2">
                <label>
                    <input type="hidden" class="flat-red" name="restaurant" value="0">
                    @if($hotel_detail->restaurant == 1)
                        <input type="checkbox" class="flat-red" name="restaurant" value="1" checked>
                    @else
                        <input type="checkbox" class="flat-red" name="restaurant" value="1">
                    @endif                  
                </label>
          </div>
        </div>
                    

        
        <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
            <a href="/admin/hotels/show/{{$hotel_detail->hotel_id}}" class="btn btn-info"> Back</a>
        </div>        
    </form>
</div>

@endsection