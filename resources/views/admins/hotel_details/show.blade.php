@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<section class="content-header">
  <h1>
    Wisata Kota
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/hotels">Hotel</a></li>
    <li class="active">Show</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">      
              <h3 class="box-title">Hotel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <dl class="dl-horizontal">
                  @php
                    $room_available = $hotel_detail->capacity - $hotel_detail->on_booking;
                  @endphp
                <dt>Tipe Kamar</dt>
                  <dd>{{ $hotel_detail->room_type->name }}</dd>
                <dt>Kapasitas</dt>              
                  <dd>{{ $hotel_detail->capacity }}</dd>
                <dt>On Booking</dt>
                  <dd>{{ $hotel_detail->on_booking }}</dd>
                <dt>Kamar Tersedia</dt>
                  <dd>{{ $room_available }}</dd>
                <dt>Kapasitas Kamar</dt>
                  <dd>{{ $hotel_detail->room_capacity }}</dd>
                <dt>Harga Kamar</dt>
                  <dd>Rp.{{ number_format($hotel_detail->rate,2,',','.') }}</dd>                
                <dt>Swimming pool</dt>
                  <dd>
                      @if($hotel_detail->swimming_pool == 1)
                            <span class="glyphicon glyphicon-ok"></span>
                        @else
                            <span class="glyphicon glyphicon-remove"></span>
                        @endif
                  </dd>
                <dt>Gymnesium</dt>
                  <dd>
                      @if($hotel_detail->gymnesium == 1)
                            <span class="glyphicon glyphicon-ok"></span>
                        @else
                            <span class="glyphicon glyphicon-remove"></span>
                        @endif
                  </dd>
                <dt>Wi-fi</dt>
                  <dd>
                      @if($hotel_detail->wifi == 1)
                            <span class="glyphicon glyphicon-ok"></span>
                        @else
                            <span class="glyphicon glyphicon-remove"></span>
                        @endif
                  </dd>
                <dt>Room Service</dt>
                  <dd>
                      @if($hotel_detail->room_service == 1)
                            <span class="glyphicon glyphicon-ok"></span>
                        @else
                            <span class="glyphicon glyphicon-remove"></span>
                        @endif
                  </dd>
                <dt>Air Condition</dt>
                  <dd>
                      @if($hotel_detail->air_condition == 1)
                            <span class="glyphicon glyphicon-ok"></span>
                        @else
                            <span class="glyphicon glyphicon-remove"></span>
                        @endif
                  </dd>
                <dt>Restaurant</dt>
                  <dd>
                      @if($hotel_detail->restaurant == 1)
                            <span class="glyphicon glyphicon-ok"></span>
                        @else
                            <span class="glyphicon glyphicon-remove"></span>
                        @endif
                  </dd>
                                    
                  <dt>Foto Kamar</dt>
                  <dd><img class="img-responsive pad" src="{{ url($hotel_detail->photo) }}" alt="Photo" height="108" width="130"></dd>
              </dl>        
                <a href="/admin/hotels" class="btn btn-info btn-xs"> Back</a>
                <a href="/admin/hotel_details/edit/{{$hotel_detail->id}}" class="btn btn-primary btn-xs"> Edit</a>
                <a href="/admin/hotel_details/delete/{{$hotel_detail->id}}" class="btn btn-warning btn-xs"> Delete</a>
                <br>                                                         
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->     
        </div>
    </div>
    <!-- /.row -->
</section>

@endsection