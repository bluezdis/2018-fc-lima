@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<section class="content-header">
  <h1>
    Wisata Kota
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/flights">Tiket</a></li>
    <li class="active">Show</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">      
              <h3 class="box-title">Flight</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <dl class="dl-horizontal">                
                <dt>Keberangkata</dt>
                <dd>{{$flight->departure->name}} - {{$flight->departure->code_airport}} - {{$flight->departure->state->name}} - {{$flight->departure->district->name}}</dd>

                <dt>Kedatangan</dt>
                <dd>{{$flight->arrival->name}} - {{$flight->arrival->code_airport}} - {{$flight->arrival->state->name}} - {{$flight->arrival->district->name}}</dd>

                <dt>Code Penerbangan</dt>
                <dd>{{$flight->code}}</dd>

                <dt>Code Pesawat</dt>
                <dd>{{$flight->plane->plane_code}} - {{$flight->plane->model}}</dd>

                <dt>Tanggal Keberangkatan</dt>
                <dd>{{ date("d-M-Y", strtotime($flight->flight_date_departure)) }} - {{ $flight->flight_time_departure }}</dd>

                <dt>Tanggal Kedatangan</dt>
                <dd>{{ date("d-M-Y", strtotime($flight->flight_date_arrival)) }} - {{ $flight->flight_time_arrival }}</dd>

                <dt>Tanggal Kedatangan</dt>
                <dd>{{ $flight->duration }}</dd>

                <dt>Harga Tiket</dt>
                <dd>Rp.{{ number_format($flight->rate,2,',','.') }}</dd>

                <dt>Fasilitas Bagasi</dt>
                <dd>{{ $flight->facilities }}</dd>

                <dt>Refund Tiket</dt>
                <dd>
                	{{ $flight->refund }}
                	@if($flight->refund == 1)
                	YES
                	@else
                	NO
                	@endif
                </dd>
                
              </dl>        
                <a href="/admin/flights" class="btn btn-info btn-xs"> Back</a>
                <a href="/admin/flights/edit/{{$flight->id}}" class="btn btn-primary btn-xs"> Edit</a>
                <a href="/admin/flights/delete/{{$flight->id}}" class="btn btn-warning btn-xs"> Delete</a>
                <br>                                                         
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->     
        </div>
    </div>
    <!-- /.row -->
</section>

@endsection