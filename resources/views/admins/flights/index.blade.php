@extends('layouts.admin')

@section('js')
    <!-- data-tables -->
    $(function () {
        $('#example1').DataTable()          
    })
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    List flights
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/flights">$flight</a></li>
    <li class="active">Index</li>
  </ol>
</section>
<!-- /.box-header -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
          <a href="/admin/flights/create" class="btn btn-success btn-sm"> Tambah Penerbangan</a>
          <br>    
          <br>            
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Keberangkatan</th>
              <th>Kedatangan</th>
              <th>Code</th>
              <th>Plane</th>
              <th>Waktu Keberangkatan</th>
              <th>Waktu Kedatangan</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($flights as $flight)
            <tr>
              <td>{{ $flight->departure->code_airport }}</td>
              <td>{{ $flight->arrival->code_airport }}</td>
              <td>{{ $flight->code }}</td>
              <td>{{ $flight->plane->plane_code }}</td>              
              <td>{{ date("d-M-Y", strtotime($flight->flight_date_departure)) }} - {{ $flight->flight_time_departure }}</td>
              <td>{{ date("d-M-Y", strtotime($flight->flight_date_arrival)) }} - {{ $flight->flight_time_arrival }}</td>            
              <td>
                  <a href="/admin/flights/show/{{$flight->id}}" class="btn btn-info btn-xs"> Show</a>
                  <a href="/admin/flights/edit/{{$flight->id}}" class="btn btn-primary btn-xs"> Edit</a>
                  <a href="/admin/flights/delete/{{$flight->id}}" class="btn btn-warning btn-xs"> Delete</a>
              </td>          
            </tr>
            @endforeach        
            </tbody>
            <tfoot>
            <tr>
              <th>Keberangkatan</th>
              <th>Kedatangan</th>
              <th>Code</th>
              <th>Plane</th>
              <th>Waktu Keberangkatan</th>
              <th>Waktu Kedatangan</th>
              <th>Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
</div>
</section>
@endsection