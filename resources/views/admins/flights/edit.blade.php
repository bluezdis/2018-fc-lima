@extends('layouts.admin')

@section('js')
<script type="text/javascript">
    
</script>
@endsection

@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Penerbangan
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/flights">Penerbangan</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
    
    <section class="content">
      <div class="row">
            <div class="col-md-12">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/flights/update') }}" enctype="multipart/form-data">
                    @csrf                    
                    <input type="hidden" name="id" value="{{ $flight->id }}">
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Keberangkatan</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="departure_id" name="departure_id" style="width: 100%;">
                          <option selected="selected" >Pilih Bandara</option>
                            @foreach($departures as $departure)
                            	@if($departure->id == $flight->departure_id)
                            	<option value="{{$departure->id}}" selected="">{{$departure->name}} - {{$departure->code_airport}}</option>
                            	@else
                            	<option value="{{$departure->id}}">{{$departure->name}} - {{$departure->code_airport}}</option>
                            	@endif
                            @endforeach              
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Kedatangan</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="arrival_id" name="arrival_id" style="width: 100%;">
                          <option selected="selected" >Pilih Bandara</option>
                            @foreach($arrivals as $arrival)
	                            @if($arrival->id == $flight->arrival_id)
	                            	<option value="{{$arrival->id}}" selected="">{{$arrival->name}} - {{$arrival->code_airport}}</option>
	                            @else
	                            	<option value="{{$arrival->id}}">{{$arrival->name}} - {{$arrival->code_airport}}</option>
	                            @endif                                
                            @endforeach              
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Kode Penerbangan</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="code" name="code" placeholder="Enter Kode Penerbangan" value="{{$flight->code}}">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Penerbangan Untuk Pesawat</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="plane_id" name="plane_id" style="width: 100%;">
                          <option selected="selected" >Pilih Pesawat</option>
                            @foreach($planes as $plane)
                            	@if($plane->id == $flight->plane_id)
	                            	<option value="{{$plane->id}}" selected="">{{$plane->name}} - {{$plane->plane_code}}</option>
	                            @else
	                            	<option value="{{$plane->id}}">{{$plane->name}} - {{$plane->plane_code}}</option>
	                            @endif                                 
                            @endforeach              
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Tanggal Keberangkatan</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control pull-right datepicker" name="flight_date_departure" value="{{ date("d-M-Y", strtotime($flight->flight_date_departure)) }}">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Tanggal Kedatagan</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control pull-right datepicker" name="flight_date_arrival" value="{{ date("d-M-Y", strtotime($flight->flight_date_arrival)) }}">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Pukul Keberangkatan</label>
                      <div class="col-sm-9">
                        <input type="time" class="form-control pull-right" id="datepicker" name="flight_time_departure" value="{{$flight->flight_time_departure}}">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Pukul Kedatagan</label>
                      <div class="col-sm-9">
                        <input type="time" class="form-control pull-right" id="datepicker" name="flight_time_arrival" value="{{$flight->flight_time_arrival}}">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Durasi Perjalanan (Satuan Jam)</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control pull-right" name="duration" value="{{$flight->duration}}">
                      </div>
                    </div>

                     <div class="form-group">
                      <label class="col-sm-2 control-label">Harga</label>
                      <div class="col-sm-9">
                        <input type="number" class="form-control pull-right" name="rate" value="{{ $flight->rate}}">
                      </div>
                    </div> 

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Fasilitas</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" rows="3" id="facilities" name="facilities" placeholder="Masukan Penerbangan">{{$flight->facilities}}</textarea>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Bisa Refund</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="plane_id" name="plane_id" style="width: 100%;">
                          <option selected="selected" >Pilih</option>
                          @if( $flight->refund == 1 )
                          <option value="1" selected="">Ya</option>
                          <option value="2">Tidak</option>
                          @else
                          <option value="1" >Ya</option>
                          <option value="2" selected="">Tidak</option>
                          @endif                                                    
                        </select>
                      </div>
                    </div>

                    <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="/admin/flights" class="btn btn-info"> Back</a>
                    </div>
                </form>
                </div>
          </div>
        </div>
    </section>                  
</div>

@endsection