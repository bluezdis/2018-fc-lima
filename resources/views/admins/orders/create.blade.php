@extends('layouts.admin')

@section('js')
<script type="text/javascript">
   $(function(){
    var i = 0;
    var l = 1;
       
    $("#add").click(function(){
   
        ++i;
        ++l;        
   
        $("#dinamic_form").append('<div class="box-body"><div class="row"><div class="col-xs-2"><h5 class="box-title">Order Hotel '+l+'</h5> </div><div class="col-xs-3"><select class="form-control select2" id="hotel_id" name="hotel_id['+i+']" style="width: 100%;"><option selected="selected" >Pilih Hotel</option>@foreach($hotels as $hotel)<option value="{{$hotel->id}}">{{$hotel->name}}</option>@endforeach</select></div><div class="col-xs-3"><select class="form-control select2" id="room_type_id" name="room_type_id['+i+']" style="width: 100%;"><option selected="selected" >Pilih Jenis Kamar</option>@foreach($room_types as $room_type)<option value="{{$room_type->id}}">{{$room_type->name}}</option>@endforeach</select></div><div class="col-xs-3"><input type="text" class="form-control" id="quntity_order" name="quntity_order['+i+']" placeholder="Jumlah Kamar"></div></div></div><div class="box-body"><div class="col-xs-5"><div class="input-group date"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control pull-right datepicker" name="hotel_check_in['+i+']" placeholder="Tanggal Check In"></div>Tanggal Check In</div><div class="col-xs-5"><div class="input-group date"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control pull-right datepicker" name="hotel_check_out['+i+']"placeholder="Tanggal Check Out"></div>Tanggal Check Out</div><div class="col-xs-2"></div></div>');
    });

    var k = 0;
       
    $("#add_merchant_wisata").click(function(){
   
        ++k;    
   
        $("#dinamic_form_merchant_wisata").append('<div class="box-body"><div class="row"><div class="col-xs-2"><select class="form-control select2" id="merchant_id" name="merchant_id['+k+']" style="width: 100%;"><option selected="selected" >Pilih Merchant</option>@foreach($merchant_wisata as $merchant)<option value="{{$merchant->id}}">{{$merchant->name}}</option>@endforeach</select></div><div class="col-xs-5"><div class="input-group date"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control pull-right datepicker" name="merchant_order_wisata['+k+']" placeholder="Tanggal Order"></div>Tanggal Order</div><div class="col-xs-3"><input type="text" class="form-control" id="quntity_order" name="quntity_order_wisata['+k+']" placeholder="Jumlah Tiket"></div><div class="col-xs-1"></div></div></div>');
    });

    var x = 0;
       
    $("#add_merchant_culinary").click(function(){
   
        ++x;      
   
        $("#dinamic_form_merchant_culinary").append('<div class="box-body"><div class="row"><div class="col-xs-2"><select class="form-control select2" id="merchant_culinary_id['+x+']" name="merchant_id['+x+']" style="width: 100%;"><option selected="selected" >Pilih Merchant</option>@foreach($merchant_kuliner as $merchant)<option value="{{$merchant->id}}">{{$merchant->name}}</option>@endforeach</select></div><div id="merchant_detail['+x+']" class="col-xs-3"><select id="merchant_detail_id" name="merchant_detail_id['+x+']" class="form-control select2"><option value="">--- Pilih Menu Merchant ---</option></select></div><div class="col-xs-3"><div class="input-group date"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control pull-right datepicker" name="merchant_order_wisata['+x+']" placeholder="Tanggal Order"></div>Tanggal Order</div><div class="col-xs-2"><input type="text" class="form-control" id="quntity_order" name="quntity_order_kuliner['+x+']" placeholder="Jumlah Pesanan"></div><div class="col-xs-1"></div></div></div>');
    });

     $("#merchant_culinary_id[1]").change(function(){
        var merchantId = parseInt($("#merchant_culinary_id[1]").val());        
        $.ajax({
              type: "get",
              url: "/admin/orders/merchant_details/"+merchantId,
                success: function(response){
                  var merchant_details = response;

                  var newData = "<select id='merchant_id[1]' class='form-control select2'>";
                  newData += "<option value='0'>--- Pilih Menu Merchant ---</option>";

                  for(var n=0;n<merchant_details.length;n++) {
                      newData += "<option value='"+ merchant_details[n].id +"'>"+ merchant_details[n].name +"</option>";
                  }
                  newData += "</select>";

                  $("#merchant_detail[1]").empty().append(newData);
                }
        });
    });

      $("#merchant_culinary_id").change(function(){
            var merchantId = parseInt($("#merchant_culinary_id").val());
            $.ajax({
                  type: "get",
                  url: "/admin/orders/merchant_details/"+merchantId,
                    success: function(response){
                      var merchant_details = response;

                      var newData = "<select id='merchant_id' class='form-control select2'>";
                      newData += "<option value='0'>--- Pilih Menu Merchant ---</option>";

                      for(var n=0;n<merchant_details.length;n++) {
                          newData += "<option value='"+ merchant_details[n].id +"'>"+ merchant_details[n].name +"</option>";
                      }
                      newData += "</select>";

                      $("#merchant_detail").empty().append(newData);
                    }
            });
        });
      });
</script>
@endsection

@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambahkan Order
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/orders">Order</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
    
    <section class="content">
      <div class="row">
            <div class="col-md-12">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/orders/insert') }}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Invoice</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="invoice" name="invoice" value="{{$invoice}}" readonly="" placeholder="Enter Nama Order">
                      </div>
                    </div>                    

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Dipesan Oleh</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="user_id" name="user_id" style="width: 100%;">
                          <option selected="selected">Pilih Member</option>
                            @foreach($order_by as $order_by)
                                <option value="{{$order_by->id}}">{{$order_by->name}}</option>
                            @endforeach              
                        </select>
                      </div>
                    </div>
                  

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Tanggal Order</label>
                      <div class="col-sm-9">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right datepicker" name="required_date">
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Status Order</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="status_order" name="status_order" style="width: 100%;">
                          <option selected="selected">Pilih Status Order</option>
                              @php
                              $i = 1;
                              @endphp
                              @foreach($status_orders as $status_order)
                                <option value="{{$i}}">{{$status_order}}</option>
                                @php
                                $i = $i + 1;
                                @endphp
                              @endforeach                                                                
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Tipe Pembayaran</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="payment_method" name="payment_method" style="width: 100%;">
                          <option selected="selected">Pilih Tipe Pembayaran</option>
                              @php
                              $i = 1;
                              @endphp
                              @foreach($payment_methods as $payment_method)
                                <option value="{{$i}}">{{$payment_method}}</option>
                                @php
                                $i = $i + 1;
                                @endphp
                              @endforeach                                                                
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Discount</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="discount" name="discount" placeholder="Enter Discount">
                      </div>
                    </div>

                    <!-- <div class="form-group">
                      <label class="col-sm-2 control-label">Total</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="subtotal" name="subtotal" placeholder="Enter Discount">
                      </div>
                    </div> -->

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Keterangan Order</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" rows="3" id="additional_note" name="additional_note" placeholder="Masukan Keterangan Order"></textarea>
                      </div>
                    </div>

                     <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Order Hotel Detail</h3>
                        </div>                        
                        <div id="dinamic_form">            
                          <div class="box-body">                            
                            <div class="row">
                              <div class="col-xs-2">
                                <h5 class="box-title">Order Hotel 1</h5> 
                              </div>
                              <div class="col-xs-3">
                                <select class="form-control select2" id="hotel_id" name="hotel_id[0]" style="width: 100%;">
                                    <option selected="selected" >Pilih Hotel</option>
                                      @foreach($hotels as $hotel)
                                          <option value="{{$hotel->id}}">{{$hotel->name}}</option>
                                      @endforeach              
                                  </select>
                              </div>

                              <div class="col-xs-3">
                                <select class="form-control select2" id="room_type_id" name="room_type_id[0]" style="width: 100%;">
                                    <option selected="selected" >Pilih Jenis Kamar</option>
                                      @foreach($room_types as $room_type)
                                          <option value="{{$room_type->id}}">{{$room_type->name}}</option>
                                      @endforeach              
                                  </select>
                              </div>
                              

                              <div class="col-xs-3">
                                <input type="text" class="form-control" id="quntity_order" name="quntity_order[0]" placeholder="Jumlah Kamar">
                              </div>                            
                                               
                            </div>
                          </div>
                          <div class="box-body">
                            <div class="col-xs-5">                                                  
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right datepicker" name="hotel_check_in[0]" placeholder="Tanggal Check In">
                            </div>
                            Tanggal Check In
                          </div>

                          <div class="col-xs-5">                            
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right datepicker" name="hotel_check_out[0]" placeholder="Tanggal Check Out">
                            </div>
                            Tanggal Check Out
                          </div>

                          <div class="col-xs-2">                   
                              <a id="add" class="btn btn-success btn-sm"> tambah<i class="fa fa-plus"></i></a>
                          </div>
                        </div>

                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->

                      <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Order Merchant Detail Wisata</h3>
                        </div>
                        <div id="dinamic_form_merchant_wisata">            
                        <div class="box-body">
                          <div class="row">
                            <div class="col-xs-2">
                              <select class="form-control select2" id="merchant_id" name="merchant_id[0]" style="width: 100%;">
                                  <option selected="selected" >Pilih Merchant</option>
                                    @foreach($merchant_wisata as $merchant)
                                        <option value="{{$merchant->id}}">{{$merchant->name}}</option>
                                    @endforeach              
                                </select>
                            </div>

                            <div class="col-xs-5">                            
                              <div class="input-group date">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right datepicker" name="merchant_order_wisata[0]" placeholder="Tanggal Order">
                              </div>
                              Tanggal Order
                            </div>

                            <div class="col-xs-3">
                              <input type="text" class="form-control" id="quntity_order" name="quntity_order_wisata[0]" placeholder="Jumlah Tiket">
                            </div> 

                            <div class="col-xs-1">                   
                                <a id="add_merchant_wisata" class="btn btn-success btn-sm"> tambah<i class="fa fa-plus"></i></a>
                            </div>                 
                          </div>
                        </div>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->

                      <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Order Merchant Detail Kuliner</h3>
                        </div>
                        <div id="dinamic_form_merchant_culinary">            
                        <div class="box-body">
                          <div class="row">
                            <div class="col-xs-2">
                              <select class="form-control select2" id="merchant_culinary_id" name="merchant_id[0]" style="width: 100%;">
                                  <option selected="selected" >Pilih Merchant</option>
                                    @foreach($merchant_kuliner as $merchant)
                                      <option value="{{$merchant->id}}">{{$merchant->name}}</option>
                                    @endforeach              
                                </select>
                            </div>

                            <div id="merchant_detail" class="col-xs-3">                                    
                              <select id="merchant_detail_id" name="merchant_detail_id[0]" class="form-control select2">
                                  <option value="">--- Pilih Menu Merchant ---</option>                                  
                              </select>
                            </div>

                            <div class="col-xs-3">                            
                              <div class="input-group date">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right datepicker" name="merchant_order_wisata[0]" placeholder="Tanggal Order">
                              </div>
                              Tanggal Order
                            </div>

                            <div class="col-xs-2">
                              <input type="text" class="form-control" id="quntity_order" name="quntity_order_kuliner[0]" placeholder="Jumlah Pesanan">
                            </div> 

                            <div class="col-xs-1">                   
                                <a id="add_merchant_culinary" class="btn btn-success btn-sm"> tambah<i class="fa fa-plus"></i></a>
                            </div>                 
                          </div>
                        </div>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->                      

                    <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="/admin/orders" class="btn btn-info"> Back</a>
                    </div>
                </form>
                </div>
          </div>
        </div>
    </section>                  
</div>

@endsection