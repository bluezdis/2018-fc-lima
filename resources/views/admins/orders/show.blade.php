@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<section class="content-header">
  <h1>
    Order
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Order</a></li>
    <li class="active">Show</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">      
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt>Invoice</dt>
            <dd>{{$order->invoice}}</dd>        
            <dt>Dirrder Oleh</dt>
            <dd>{{$order->user->name}}</dd>
              <dt>Order Date</dt>
            <dd>{{ date("d-M-Y", strtotime($order->order_date)) }}</dd>
            @if($order->order_details->first()->hotel_id)
            <dt>Tanggal Check In</dt>
            <dd>{{ date("d-M-Y", strtotime($order->order_details->first()->hotel_check_in)) }}</dd>                      
            <dt>Tanggal Check Out</dt>
            <dd>{{ date("d-M-Y", strtotime($order->order_details->first()->hotel_check_out)) }}</dd>                      
            <dt>Jumlah Hari Menginap</dt>
            <dd>{{ $order->order_details->first()->quantity_order }}</dd>
            <dt>Harga Kamar</dt>
            <dd>Rp.{{ number_format($order->order_details->first()->unit_price,2,',','.') }}</dd>
            <dt>Tipe Kamar</dt>
            <dd>{{ $order->order_details->first()->room_type->name}}</dd>
            @endif
            @if($order->payment_method == 2 &&  $order->status_order == 3 && $order->proof_of_payment)
              <dt>Tanggal Pembayaran</dt>
            <dd>{{ date("d-M-Y", strtotime($order->purchase_date)) }}</dd>
            @endif
              <dt>Status Order</dt>
            <dd>{{$status_order}}</dd>
            <dt>Metoda Pembayaran</dt>
            <dd>{{$payment_method}}</dd>            
            <dt>Diskon</dt>
            <dd>Rp.{{ number_format($order->discount,2,',','.') }}</dd>
            <dt>Total Pembayaran</dt>
            <dd>Rp.{{ number_format($order->subtotal,2,',','.') }}</dd>
            <dt>Note Order</dt>
            <dd>{{$order->additional_note}}</dd>

          </dl>
         <div class="col s12">&nbsp;</div>
            <a href="/admin/orders" class="btn btn-info btn-xs"> Back</a>
            <a href="/admin/orders/edit/{{$order->id}}" class="btn btn-primary btn-xs"> Edit</a>
            <a href="/admin/orders/delete/{{$order->id}}" class="btn btn-warning btn-xs"> Delete</a>
        </div>         
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </div>
</section>

@endsection