@extends('layouts.admin')

@section('js')
<script type="text/javascript">
   
   
</script>
@endsection

@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambahkan Order
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/orders">Order</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    
    <section class="content">
      <div class="row">
            <div class="col-md-12">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/orders/update') }}" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="id" value="{{ $order->id }}">
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Invoice</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="invoice" name="invoice" value="{{$order->invoice}}" readonly="" placeholder="Enter Nama Order">
                      </div>
                    </div>                    

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Dipesan Oleh</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="user_id" name="user_id" style="width: 100%;">
                          <option selected="selected">Pilih Member</option>
                            @foreach($order_by as $order_by)
                                @if($order_by->id == $order->order_by)
                                  <option value="{{$order_by->id}}" selected="">{{$order_by->name}}</option>
                                @else
                                  <option value="{{$order_by->id}}">{{$order_by->name}}</option>
                                @endif                                
                            @endforeach              
                        </select>
                      </div>
                    </div>
                  

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Tanggal Order</label>
                      <div class="col-sm-9">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" id="datepicker" value="{{$order->order_date}}" name="required_date">
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Status Order</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="status_order" name="status_order" style="width: 100%;">
                          <option selected="selected">Pilih Status Order</option>
                              @php
                              $i = 1;
                              @endphp
                              @foreach($status_orders as $status_order)
                                @if($i == $order->status_order)
                                  <option value="{{$i}}" selected="">{{$status_order}}</option>
                                @else
                                  <option value="{{$i}}">{{$status_order}}</option>
                                @endif   
                                @php
                                $i = $i + 1;
                                @endphp
                              @endforeach                                                                
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Tipe Pembayaran</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="payment_method" name="payment_method" style="width: 100%;">
                          <option selected="selected">Pilih Tipe Pembayaran</option>
                              @php
                              $i = 1;
                              @endphp
                              @foreach($payment_methods as $payment_method)
                                @if($i == $order->payment_method)
                                  <option value="{{$i}}" selected="">{{$payment_method}}</option>
                                @else
                                  <option value="{{$i}}">{{$payment_method}}</option>
                                @endif
                                @php
                                $i = $i + 1;
                                @endphp
                              @endforeach                                                                
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Discount</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="discount" name="discount" value="{{$order->discount}}" placeholder="Enter Discount">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Total</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="subtotal" name="subtotal" value="{{$order->subtotal}}" placeholder="Enter Discount">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Keterangan Order</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" rows="3" id="additional_note" name="additional_note" placeholder="Masukan Keterangan Order">{{$order->additional_note}}</textarea>
                      </div>
                    </div>   

                    <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="/admin/orders" class="btn btn-info"> Back</a>
                    </div>
                </form>
                </div>
          </div>
        </div>
    </section>                  
</div>

@endsection