@extends('layouts.admin')

@section('js')
    <!-- data-tables -->
    $(function () {
        $('#example1').DataTable()          
    })
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    List Orders
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/orders">Orders</a></li>
    <li class="active">Index</li>
  </ol>
</section>
<!-- /.box-header -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
          <a href="/admin/orders/create" class="btn btn-success btn-sm"> Tambah Order</a>
          <br>    
          <br>            
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Invoice</th>
              <th>Order Merchant / Hotel / Package</th>
              <th>Order Oleh</th>
              <th>Tanggal Order</th>
              <th>Status Order</th>
              <th>Total Pembayaran</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @php
              $order_data = new App\Model\Order;;
              $status_orders = $order_data->status_order();
            @endphp
            @foreach($orders as $order)
            <tr>
              <td>{{ $order->invoice }}</td>
              <td>
                  @if($order->order_details->first()['hotel_id'])
                    {{$order->order_details->first()->hotel['name']}}
                  @elseif($order->order_details->first()['merchant_id'])
                    {{$order->order_details->first()->merchant['name']}}
                  @elseif($order->order_details->first()['flight_id'])
                    {{$order->order_details->first()->flight['code']}} - {{$order->order_details->first()->flight->plane['name']}} - {{$order->order_details->first()->flight->plane['plane_code']}}
                  @elseif($order->package_id)
                    {{$order->package->name}}
                  @endif
              </td>
              <td>{{ $order->order_by ? $order->user->name : '-' }}</td>
              <td>{{ date("d-M-Y", strtotime($order->order_date)) }}</td>              
              <td>
                {{ $status_orders[$order->status_order] }}
                @if($order->payment_method == 2 &&  $order->status_order == 3 && $order->proof_of_payment)
                  <br>
                  <a href="/{{$order->proof_of_payment}}" class="img-gal" target="_blank">Lihat bukti tranfer</a>
                @endif
              </td>              
              <td>Rp. {{ number_format($order->subtotal,2,',','.') }}</td>
              <td>
                  @if($order->status_order != 5 && $order->status_order != 6)
                  <a href="/admin/orders/reject/{{$order->id}}" class="btn btn-danger btn-xs">Tolak Order</a>
                  @endif
                  @if($order->status_order == 1 || $order->status_order ==  2 )
                  <a href="/admin/orders/show/{{$order->id}}" class="btn btn-info btn-xs"> Show</a>
                  <a href="/admin/orders/edit/{{$order->id}}" class="btn btn-primary btn-xs"> Edit</a>
                  <a href="/admin/orders/delete/{{$order->id}}" class="btn btn-warning btn-xs"> Delete</a>
                  @endif
                  @if($order->status_order == 3)
                  <a href="/admin/orders/process/{{$order->id}}" class="btn btn-success btn-xs">Proses Pesanan</a>
                  @elseif($order->status_order == 4)
                  <a href="/admin/orders/completed/{{$order->id}}" class="btn btn-success btn-xs">Pesanan Selesai</a>
                  @endif
              </td>          
            </tr>
            @endforeach        
            </tbody>
            <tfoot>
            <tr>
              <th>Invoice</th>
              <th>Order Merchant / Hotel / Package</th>
              <th>Order Oleh</th>
              <th>Tanggal Order</th>
              <th>Status Order</th>
              <th>Total Pembayaran</th>
              <th>Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
</div>
</section>
@endsection