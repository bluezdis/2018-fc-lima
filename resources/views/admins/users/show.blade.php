@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<section class="content-header">
  <h1>
    Users Detail
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/users">Users</a></li>
    <li class="active">Show</li>
  </ol>
</section>

<div class="col-md-12">
  <div class="box box-solid">
    <div class="box-header with-border">      
      <h3 class="box-title"></h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <dl class="dl-horizontal">
        <dt>Name User</dt>
        <dd>{{$user->name}}</dd>        
        <dt>Name Type</dt>
        <dd>{{$user->user_group->name}}</dd>
          <dt>NIK</dt>
        <dd>{{$user->nik}}</dd>        
          <dt>Tanggal Lahir</dt>
        <dd>{{date('d-M-Y', strtotime($user->birthdate))}}</dd>                            
          
      </dl>
     <div class="col s12">&nbsp;</div>
        <a href="/admin/users" class="btn btn-info btn-xs"> Back</a>
        <a href="/admin/users/edit/{{$user->id}}" class="btn btn-primary btn-xs"> Edit</a>
        <a href="/admin/users/delete/{{$user->id}}" class="btn btn-warning btn-xs"> Delete</a>
    </div>         
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>

@endsection