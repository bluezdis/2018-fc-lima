@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambahkan User
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/users">user</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
    
    <div class="box-header with-border">
      <h3 class="box-title"></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/users/insert') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label class="col-sm-2 control-label">Nama User</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Nama User">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">User Group</label>
          <div class="col-sm-9">              
            <select class="form-control select2" id="user_group_id" name="user_group_id" style="width: 100%;">
              <option selected="selected">Pilih User Group</option>
                @foreach($user_groups as $user_group)
                    <option value="{{$user_group->id}}">{{$user_group->name}}</option>
                @endforeach              
            </select>
          </div>
        </div>          
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Tanggal Lahir</label>
          <div class="col-sm-9">
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" class="form-control pull-right" id="datepicker" name="birthdate">
            </div>
          </div>
        </div>        
        
        <div class="form-group">
          <label class="col-sm-2 control-label">NIK</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="nik" name="nik" placeholder="Enter NIK">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Email</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="email" name="email"  placeholder="Enter email">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Password</label>
          <div class="col-sm-9">
              <input type="password" class="form-control" placeholder="Enter Password" name="password">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Password Confirmation</label>
          <div class="col-sm-9">
              <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation">
          </div>
        </div>
        
        <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
            <a href="/admin/users" class="btn btn-info"> Back</a>
        </div>
    </form>
</div>

@endsection