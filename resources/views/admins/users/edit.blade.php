@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambahkan User
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="admin/users">User</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    
    <div class="box-header with-border">
      <h3 class="box-title"></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/users/update') }}" enctype="multipart/form-data">
        @csrf
        
        <input type="hidden" name="id" value="{{ $user->id }}">
        <div class="form-group">
          <label class="col-sm-2 control-label">Nama User</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}" placeholder="Enter Nama User">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">User Group</label>
          <div class="col-sm-9">              
            <select class="form-control select2" id="user_group_id" name="user_group_id" style="width: 100%;">
              <option selected="selected">Pilih User Group</option>
                @foreach($user_groups as $user_group)
                    @if($user_group->id == $user->user_group_id)
                    <option value="{{$user_group->id}}" selected="" >{{$user_group->name}}</option>
                    @else
                    <option value="{{$user_group->id}}">{{$user_group->name}}</option>
                    @endif
                @endforeach              
            </select>
          </div>
        </div>          
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Tanggal Lahir</label>
          <div class="col-sm-9">
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" class="form-control pull-right" id="datepicker" value='{{ $user->birthdate}}' name="birthdate">
            </div>
          </div>
        </div>        
        
        <div class="form-group">
          <label class="col-sm-2 control-label">NIK</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="nik" name="nik" value="{{$user->nik}}" placeholder="Enter NIK">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Email</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="email" name="email" value="{{$user->email}}" placeholder="Enter email">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Password</label>
          <div class="col-sm-9">
              <input type="password" class="form-control" placeholder="Enter Password" name="password">
          </div>
        </div>
        
        <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
            <a href="/admin/merchants" class="btn btn-info"> Back</a>
        </div>        
    </form>
</div>

@endsection