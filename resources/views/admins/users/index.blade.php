@extends('layouts.admin')

@section('js')
    <!-- data-tables -->
    $(function () {
        $('#example1').DataTable()          
    })
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    List Users
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/users">User</a></li>
    <li class="active">Index</li>
  </ol>
</section>
<!-- /.box-header -->
<div class="row">
    <div class="col-xs-12">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
          <a href="/admin/users/create" class="btn btn-success btn-sm"> Tambah User</a>
          <br>    
          <br>            
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Tipe User</th>
              <th>Nama</th>
              <th>NIK</th>
              <th>Email</th>              
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr>
              <td>{{ $user->user_group->name }}</td>
              <td>{{ $user->name }}</td>
              <td>{{ $user->nik }}</td>
              <td>{{ $user->email }}</td>
              <td>
                  <a href="/admin/users/show/{{$user->id}}" class="btn btn-block btn-info btn-xs"> Show</a>
                  <a href="/admin/users/edit/{{$user->id}}" class="btn btn-block btn-primary btn-xs"> Edit</a>
                  <a href="/admin/users/delete/{{$user->id}}" class="btn btn-block btn-warning btn-xs"> Delete</a>
              </td>          
            </tr>
            @endforeach        
            </tbody>
            <tfoot>
            <tr>
              <th>Tipe User</th>
              <th>Nama</th>
              <th>NIK</th>
              <th>Email</th>              
              <th>Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
</div>
@endsection