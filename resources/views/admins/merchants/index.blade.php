@extends('layouts.admin')

@section('js')
    <!-- data-tables -->
    $(function () {
        $('#example1').DataTable()          
    })
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    List Merchants
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Merchant</a></li>
    <li class="active">Index</li>
  </ol>
</section>
<!-- /.box-header -->
<div class="row">
    <div class="col-xs-12">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
          <a href="/admin/merchants/create" class="btn btn-success btn-sm"> Tambah Merchane</a>
          <br>    
          <br>            
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Tipe Merchane</th>
              <th>Nama Merchane</th>
              <th>Provinsi</th>
              <th>Kota</th>
              <th>Fasilitas</th>
              <th>Alamat</th>
              <th>Rate</th>
              <th>Merchane Member</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($merchants as $merchant)
            <tr>
              <td>{{ $merchant->merchant_type->name }}</td>
              <td>{{ $merchant->name }}</td>
              <td>{{ $merchant->state->name }}</td>
              <td>{{ $merchant->district->name }}</td>
              <td>{{ $merchant->facilities }}</td>
              <td>{{ $merchant->address }}</td>
              <td>Rp.{{ number_format($merchant->rate,2,',','.') }}</td>
              <td>{{ $merchant->user_id ? $merchant->user->name : '-' }}</td>            
              <td>
                  <a href="/admin/merchants/show/{{$merchant->id}}" class="btn btn-block btn-info btn-xs"> Show</a>
                  <a href="/admin/merchants/edit/{{$merchant->id}}" class="btn btn-block btn-primary btn-xs"> Edit</a>
                  <a href="/admin/merchants/delete/{{$merchant->id}}" class="btn btn-block btn-warning btn-xs"> Delete</a>
              </td>          
            </tr>
            @endforeach        
            </tbody>
            <tfoot>
            <tr>
              <th>Tipe Merchane</th>
              <th>Nama Merchane</th>
              <th>Provinsi</th>
              <th>Kota</th>
              <th>Fasilitas</th>
              <th>Alamat</th>
              <th>Rate</th>
              <th>Merchane Member</th>
                <th>Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
</div>
@endsection