@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<section class="content-header">
  <h1>
    Merchants Detail
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Merchant</a></li>
    <li class="active">Show</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">      
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt>Name Merchane</dt>
            <dd>{{$merchant->name}}</dd>        
            <dt>Name Type</dt>
            <dd>{{$merchant->merchant_type->name}}</dd>
              <dt>Provinsi</dt>
            <dd>{{$merchant->state->name}}</dd>        
              <dt>Kota</dt>
            <dd>{{$merchant->district->name}}</dd>                  
              <dt>Fasilitas</dt>
            <dd>{{$merchant->facilities}}</dd>        
              <dt>Alamat</dt>
            <dd>{{$merchant->address}}</dd>        
              <dt>Rate</dt>
            <dd>{{$merchant->rate}}</dd>        
              <dt>User Pengelola</dt>
            <dd>{{$merchant->user->name}}</dd>

          </dl>
         <div class="col s12">&nbsp;</div>
            <a href="/admin/merchants" class="btn btn-info btn-xs"> Back</a>
            <a href="/admin/merchants/edit/{{$merchant->id}}" class="btn btn-primary btn-xs"> Edit</a>
            <a href="/admin/merchants/delete/{{$merchant->id}}" class="btn btn-warning btn-xs"> Delete</a>
        </div>         
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">      
              <h3 class="box-title">Merchant Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <a href="/admin/merchant_details/create?merchant_id={{$merchant->id}}" class="btn btn-success btn-sm"> Tambah </a>
                <br>    
                <br>            
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama Menu / Nama Tempat</th>
                  <!-- <th>Stok</th>
                  <th>On Booking</th>
                  <th>Ketersediaan</th> -->
                  <th>Harga</th>
                  <th>Foto</th>
                  <th>Keterangan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($merchant->merchant_details as $merchant_detail)
                <tr>
                    @php
                    $available = $merchant_detail->stock - $merchant_detail->on_booking;
                    @endphp
                  <td>{{ $merchant_detail->name }}</td>                  
                  <td>Rp.{{ number_format($merchant_detail->rate,2,',','.') }}</td>
                  <td>
                      @if($merchant_detail->photo)
                          <img class="img-responsive pad" src="{{ url($merchant_detail->photo) }}" alt="Photo" height="108" width="130">
                      @else
                          <img class="img-fluid" src="frontend/img/no_image.png" alt="">
                      @endif                      
                    </td>
                    <td>{{ $merchant_detail->description}}</td>
                  <td>
                      <a href="/admin/merchant_details/show/{{$merchant_detail->id}}" class="btn btn-info btn-xs"> Show</a>
                      <a href="/admin/merchant_details/edit/{{$merchant_detail->id}}" class="btn btn-primary btn-xs"> Edit</a>
                      <a href="/admin/merchant_details/delete/{{$merchant_detail->id}}" class="btn btn-warning btn-xs"> Delete</a>
                  </td>
                </tr>
                @endforeach        
                </tbody>
                <tfoot>
                <tr>
                  <th>Nama Menu / Nama Tempat</th>
                  <!-- <th>Stok</th>
                  <th>On Booking</th>
                  <th>Ketersediaan</th> -->
                  <th>Harga</th>
                  <th>Foto</th>
                  <th>Keterangan</th>
                  <th>Action</th>
                </tr>
                </tfoot>
                </table>                                                  
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->     
        </div>
    </div>
    <!-- /.row -->
</section>

@endsection