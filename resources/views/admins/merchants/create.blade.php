@extends('layouts.admin')

@section('js')
<script type="text/javascript">
   
    var i = 0;
       
    $("#add").click(function(){
   
        ++i;
   
        $("#dinamic_form").append('<div class="box-body"><div class="row"><div class="col-xs-2"><input type="text" class="form-control" id="name" name="name_detail['+i+']" placeholder="Nama Detail"></div><div class="col-xs-2"><input type="text" class="form-control" id="rate_detail" name="rate_detail['+i+']" placeholder="Harga"></div><div class="col-xs-2"><input type="file" class="form-control" id="photo_detail" name="photo_detail['+i+']" placeholder="Foto Kamar"></div><div class="col-xs-4"><textarea class="form-control" rows="3" id="description" name="description['+i+']" placeholder="Masukan Keterangan Merchane"></textarea></div><div class="col-xs-1"></div></div></div>');
    });

    $("#merchant_type_id").change(function(){
      var val = $(this).val();
      if(val == "2"){
        $("#merchant_rate").show();
        $("#dinamic_form").hide();        
      }else{
        $("#merchant_rate").hide();
        $("#dinamic_form").show();
      }      
    });
</script>
@endsection


@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambahkan Merchane
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Merchant</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
    
    <div class="box-header with-border">
      <h3 class="box-title"></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/merchants/insert') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label class="col-sm-2 control-label">Nama Merchane</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" placeholder="Masukan Nama Merchane">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Merchane Tipe</label>
          <div class="col-sm-9">              
            <select class="form-control select2" id="merchant_type_id" name="merchant_type_id" style="width: 100%;">
              <option selected="selected">Pilih Merchane</option>
                @foreach($merchant_types as $merchant_type)
                    <option value="{{$merchant_type->id}}">{{$merchant_type->name}}</option>
                @endforeach              
            </select>
          </div>
        </div>          
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Provinsi</label>
          <div class="col-sm-9">
            <select class="form-control select2" id="state_id" name="state_id" style="width: 100%;">
              <option selected="selected" >Pilih Provinsi</option>
                @foreach($states as $state)
                    <option value="{{$state->id}}">{{$state->name}}</option>
                @endforeach              
            </select>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Kota</label>
          <div class="col-sm-9">
            <select class="form-control select2" id="district_id" name="district_id" style="width: 100%;">
              <option selected="selected">Pilih Kota</option>
                @foreach($districts as $district)
                    <option value="{{$district->id}}">{{$district->name}}</option>
                @endforeach              
            </select>
          </div>
        </div>
        
        <div class="form-group" id="merchant_rate">
          <label class="col-sm-2 control-label">Rate</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="rate" name="rate" placeholder="Masukan Nama Rate Merchane">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Address</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="address" name="address" placeholder="Masukan Alamat Merchane">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Fasilitas</label>
          <div class="col-sm-9">
            <textarea class="form-control" rows="3" id="facilities" name="facilities" placeholder="Masukan Fasilitas Merchane"></textarea>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Foto Logo Merchant</label>
          <div class="col-sm-9">
            <input type="file" class="form-control" id="photo" name="photo" placeholder="Foto Kamar">
          </div>
        </div>        
                
        <div class="form-group">
          <label class="col-sm-2 control-label">Pengelola Merchane</label>
          <div class="col-sm-9">
            <select class="form-control select2" id="user_id" name="user_id" style="width: 100%;">
              <option selected="selected">Pilih User Pengelola Merchane</option>
                @foreach($merchant_member as $merchant_member)
                    <option value="{{$merchant_member->id}}">{{$merchant_member->name}}</option>
                @endforeach              
            </select>
          </div>
        </div>
        
        <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Merchant Detail Kuliner / Wisata</h3>
            </div>
            <div id="dinamic_form">            
            <div class="box-body">
              <div class="row">
                <div class="col-xs-2">
                  <input type="text" class="form-control" id="name_detail" name="name_detail[0]" placeholder="Nama Detail">
                </div>
                <!-- <div class="col-xs-1">
                  <input type="text" class="form-control" id="stock" name="stock[0]" placeholder="Ketersediaan">
                </div>
                  <input type="hidden" name="on_booking" value="0">
                <div class="col-xs-2">
                  <input type="text" class="form-control" id="room_capacity" name="on_booking[0]" placeholder="Sudah Dibooking">
                </div> -->

                <div class="col-xs-2">
                  <input type="text" class="form-control" id="rate_detail" name="rate_detail[0]" placeholder="Harga">
                </div>

                <div class="col-xs-2">
                  <input type="file" class="form-control" id="photo_detail" name="photo_detail[0]" placeholder="Foto Kamar">
                </div>
                  
                <div class="col-xs-4">
                    <textarea class="form-control" rows="3" id="description" name="description[0]" placeholder="Masukan Keterangan Merchane"></textarea>
                </div>

                <div class="col-xs-2">                   
                    <a id="add" class="btn btn-success btn-sm"> tambah<i class="fa fa-plus"></i></a>
                </div>                 
              </div>
            </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        
        <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
            <a href="/admin/merchants" class="btn btn-info"> Back</a>
        </div>
    </form>
</div>

@endsection