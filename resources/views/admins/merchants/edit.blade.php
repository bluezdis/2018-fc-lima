@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambahkan Merchane
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Merchant</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    
    <div class="box-header with-border">
      <h3 class="box-title"></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/merchants/update') }}" enctype="multipart/form-data">
        @csrf
        
        <input type="hidden" name="id" value="{{ $merchant->id }}">
        <div class="form-group">
          <label class="col-sm-2 control-label">Nama Merchane</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" value="{{$merchant->name}}" placeholder="Enter Nama Merchane">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Merchane Tipe</label>
          <div class="col-sm-9">              
            <select class="form-control select2" id="merchant_type_id" name="merchant_type_id" style="width: 100%;">
              <option selected="selected">Pilih Merchane</option>
                @foreach($merchant_types as $merchant_type)
                    @if($merchant_type->id == $merchant->merchant_type_id)
                    <option value="{{$merchant_type->id}}" selected="" >{{$merchant_type->name}}</option>
                    @else
                    <option value="{{$merchant_type->id}}">{{$merchant_type->name}}</option>
                    @endif
                @endforeach              
            </select>
          </div>
        </div>          
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Provinsi</label>
          <div class="col-sm-9">
            <select class="form-control select2" id="state_id" name="state_id" style="width: 100%;">
              <option selected="selected" >Pilih Provinsi</option>
                @foreach($states as $state)                    
                    @if($state->id == $merchant->state_id)
                    <option value="{{$state->id}}" selected="">{{$state->name}}</option>
                    @else
                    <option value="{{$state->id}}">{{$state->name}}</option>
                    @endif
                @endforeach              
            </select>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Kota</label>
          <div class="col-sm-9">
            <select class="form-control select2" id="district_id" name="district_id" style="width: 100%;">
              <option selected="selected">Pilih Kota</option>
                @foreach($districts as $district)
                    @if($district->id == $merchant->district_id)
                    <option value="{{$district->id}}" selected="">{{$district->name}}</option>
                    @else
                    <option value="{{$district->id}}">{{$district->name}}</option>
                    @endif                    
                @endforeach              
            </select>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Rate</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="rate" name="rate" value="{{$merchant->rate}}" placeholder="Enter Nama Rate Merchane">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Address</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="address" name="address" value="{{$merchant->address}}"  placeholder="Enter Alamat Merchane">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Fasilitas</label>
          <div class="col-sm-9">
            <textarea class="form-control" rows="3" id="facilities" name="facilities" placeholder="Enter Fasilitas Merchane">
            {{ $merchant->facilities }}
            </textarea>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Foto Logo Merchant</label>
          <div class="col-sm-9">
            <input type="file" class="form-control" id="photo" name="photo" placeholder="Logo Merchane" value="{{$merchant->address}}">
          </div>
        </div>
                
        <div class="form-group">
          <label class="col-sm-2 control-label">Pengelola Merchane</label>
          <div class="col-sm-9">
            <select class="form-control select2" id="user_id" name="user_id" style="width: 100%;">
              <option selected="selected">Pilih User Pengelola Merchane</option>
                @foreach($merchant_member as $merchant_member)
                    @if($merchant_member->id == $merchant->user_id)
                    <option value="{{$merchant_member->id}}" selected="">{{$merchant_member->name}}</option>
                    @else
                    <option value="{{$merchant_member->id}}">{{$merchant_member->name}}</option>
                    @endif                    
                @endforeach              
            </select>
          </div>
        </div>
        
        <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
            <a href="/admin/merchants" class="btn btn-info"> Back</a>
        </div>        
    </form>
</div>

@endsection