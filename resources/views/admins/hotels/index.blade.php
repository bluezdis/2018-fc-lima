@extends('layouts.admin')

@section('js')
    <!-- data-tables -->
    $(function () {
        $('#example1').DataTable()          
    })
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    List hotels
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/hotels">$hotel</a></li>
    <li class="active">Index</li>
  </ol>
</section>
<!-- /.box-header -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
          <a href="/admin/hotels/create" class="btn btn-success btn-sm"> Tambah Hotel</a>
          <br>    
          <br>            
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Star</th>
              <th>Nama Hotel</th>
              <th>Provinsi</th>
              <th>Kota</th>
              <th>Member</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($hotels as $hotel)
            <tr>
              <td>
                  @for ($i = 0; $i <= $hotel->star; $i++)
                    <i class="fa fa-star"></i>
                  @endfor                    
              </td>
              <td>{{ $hotel->name }}</td>
              <td>{{ $hotel->state->name }}</td>
              <td>{{ $hotel->district->name }}</td>              
              <td>{{ $hotel->user_id ? $hotel->user->name : '-' }}</td>            
              <td>
                  <a href="/admin/hotels/show/{{$hotel->id}}" class="btn btn-info btn-xs"> Show</a>
                  <a href="/admin/hotels/edit/{{$hotel->id}}" class="btn btn-primary btn-xs"> Edit</a>
                  <a href="/admin/hotels/delete/{{$hotel->id}}" class="btn btn-warning btn-xs"> Delete</a>
              </td>          
            </tr>
            @endforeach        
            </tbody>
            <tfoot>
            <tr>
              <th>Star</th>
              <th>Nama Hotel</th>
              <th>Provinsi</th>
              <th>Kota</th>              
              <th>Member</th>
              <th>Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
</div>
</section>
@endsection