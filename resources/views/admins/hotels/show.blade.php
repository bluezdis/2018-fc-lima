@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<section class="content-header">
  <h1>
    Wisata Kota
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/hotels">Hotel</a></li>
    <li class="active">Show</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">      
              <h3 class="box-title">Hotel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <dl class="dl-horizontal">
                <dt>Name Hotel</dt>
                <dd>{{$hotel->name}}</dd>        
                <dt>Star</dt>
                <dd>
                  @for ($i = 0; $i <= $hotel->star; $i++)
                    <i class="fa fa-star"></i>
                  @endfor                    
                </dd>
                  <dt>Provinsi</dt>
                <dd>{{$hotel->state->name}}</dd>        
                  <dt>Kota</dt>
                <dd>{{$hotel->district->name}}</dd>
                  <dt>Kontak</dt>
                <dd>{{$hotel->contact}}</dd>
                  <dt>Email</dt>
                <dd>{{$hotel->email}}</dd>
                  <dt>Fasilitas Hotel</dt>
                <dd>{{$hotel->facility_hotel}}</dd>        
                  <dt>Fasilitas Room</dt>
                <dd>{{$hotel->facility_room}}</dd>        
                  <dt>Alamat</dt>
                <dd>{{$hotel->address}}</dd>        
                  <dt>User Pengelola</dt>
                <dd>{{$hotel->user->name}}</dd>                  
              </dl>        
                <a href="/admin/hotels" class="btn btn-info btn-xs"> Back</a>
                <a href="/admin/hotels/edit/{{$hotel->id}}" class="btn btn-primary btn-xs"> Edit</a>
                <a href="/admin/hotels/delete/{{$hotel->id}}" class="btn btn-warning btn-xs"> Delete</a>
                <br>                                                         
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->     
        </div>
    </div>
    <!-- /.row -->
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">      
              <h3 class="box-title">Hotel Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <a href="/admin/hotel_details/create?hotel_id={{$hotel->id}}" class="btn btn-success btn-sm"> Tambah </a>
                <br>    
                <br>
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Tipe Kamar</th>
                  <th>Kapasitas</th>                                
                  <th>Kamar Tersedia</th>
                  <th>Kapasitas Kamar</th>
                  <th>Harga Kamar</th>
                  <th>Foto Kamar</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($hotel->hotel_details as $hotel_detail)
                <tr>
                    @php
                    $room_available = $hotel_detail->capacity - $hotel_detail->on_booking;
                    @endphp
                  <td>{{ $hotel_detail->room_type->name }}</td>
                  <td>{{ $hotel_detail->capacity }}</td>                
                  <td>{{ $room_available }}</td>
                  <td>{{ $hotel_detail->room_capacity }}</td>
                  <td>Rp.{{ number_format($hotel_detail->rate,2,',','.') }}</td>
                  <td>
                      <img class="img-responsive pad" src="{{ url($hotel_detail->photo) }}" alt="Photo" height="108" width="130">
                    </td>                    
                  <td>
                      <a href="/admin/hotel_details/show/{{$hotel_detail->id}}" class="btn btn-info btn-xs"> Show</a>
                      <a href="/admin/hotel_details/edit/{{$hotel_detail->id}}" class="btn btn-primary btn-xs"> Edit</a>
                      <a href="/admin/hotel_details/delete/{{$hotel_detail->id}}" class="btn btn-warning btn-xs"> Delete</a>
                  </td>
                </tr>
                @endforeach        
                </tbody>
                <tfoot>
                <tr>
                  <th>Tipe Kamar</th>
                  <th>Kapasitas</th>                                
                  <th>Kamar Tersedia</th>
                  <th>Kapasitas Kamar</th>
                  <th>Harga Kamar</th> 
                  <th>Foto Kamar</th>
                  <th>Action</th>
                </tr>
                </tfoot>
                </table>                                                  
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->     
        </div>
    </div>
    <!-- /.row -->
</section>

@endsection