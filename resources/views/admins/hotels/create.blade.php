@extends('layouts.admin')

@section('js')
<script type="text/javascript">
   
    var i = 0;
       
    $("#add").click(function(){
   
        ++i;
   
        $("#dinamic_form").append('<div class="box-body"><div class="row"><div class="row"><div class="col-xs-3"><select class="form-control select2" id="room_type_id" name="room_type_id['+i+']" style="width: 100%;"><option selected="selected" >Pilih Jenis Kamar</option>@foreach($room_types as $room_type)<option value="{{$room_type->id}}">{{$room_type->name}}</option>@endforeach</select></div><div class="col-xs-1"><input type="text" class="form-control" id="capacity" name="capacity['+i+']" placeholder="Jumlah Kamar"></div><input type="hidden" name="on_booking" value="0"><div class="col-xs-1"><input type="text" class="form-control" id="room_capacity" name="room_capacity['+i+']" placeholder="kapasitas kamar"></div><div class="col-xs-3"><input type="text" class="form-control" id="rate_room" name="rate_room['+i+']" placeholder="Harga Kamar"></div><div class="col-xs-3"><input type="file" class="form-control" id="photo" name="photo['+i+']" placeholder="Foto Kamar"></div></div></div></div>');
    });
</script>
@endsection

@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambahkan Order
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/hotels">Order</a></li>
        <li class="active">Create</li>
      </ol>
    </section>
    
    <section class="content">
      <div class="row">
            <div class="col-md-12">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/hotels/insert') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Nama Order</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Nama Order">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Order Bintang</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="star" name="star" style="width: 100%;">
                          <option selected="selected" >Pilih Bintang Order</option>                
                            <option value="1">*</option>
                            <option value="2">**</option>
                            <option value="3">***</option>
                            <option value="4">****</option>
                            <option value="5">*****</option>                
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Provinsi</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="state_id" name="state_id" style="width: 100%;">
                          <option selected="selected" >Pilih Provinsi</option>
                            @foreach($states as $state)
                                <option value="{{$state->id}}">{{$state->name}}</option>
                            @endforeach              
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Kota</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="district_id" name="district_id" style="width: 100%;">
                          <option selected="selected">Pilih Kota</option>
                            @foreach($districts as $district)
                                <option value="{{$district->id}}">{{$district->name}}</option>
                            @endforeach              
                        </select>
                      </div>
                    </div>                    

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Phone</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter Nama Phone">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Enter Nama Email">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Address</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="address" name="address" placeholder="Enter Alamat Merchane">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Fasilitas Hotel</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" rows="3" id="facility_hotel" name="facility_hotel" placeholder="Enter Fasilitas Hotel"></textarea>
                      </div>
                    </div>                  
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Photo Hotel</label>
                      <div class="col-sm-9">
                        <input type="file" class="form-control" id="photo_hotel" name="photo_hotel" placeholder="Foto Hotel">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Pengelola Hotel</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="user_id" name="user_id" style="width: 100%;">
                          <option selected="selected">Pilih User Pengelola Hotel</option>
                            @foreach($members as $member)
                                <option value="{{$member->id}}">{{$member->name}}</option>
                            @endforeach              
                        </select>
                      </div>
                    </div>

                    <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Hotel Detail</h3>
                        </div>
                        <div id="dinamic_form">            
                        <div class="box-body">
                          <div class="row">
                            <div class="col-xs-3">
                              <select class="form-control select2" id="room_type_id" name="room_type_id[0]" style="width: 100%;">
                                  <option selected="selected" >Pilih Jenis Kamar</option>
                                    @foreach($room_types as $room_type)
                                        <option value="{{$room_type->id}}">{{$room_type->name}}</option>
                                    @endforeach              
                                </select>
                            </div>
                            <div class="col-xs-1">
                              <input type="text" class="form-control" id="capacity" name="capacity[0]" placeholder="Jumlah Kamar">
                            </div>
                              <input type="hidden" name="on_booking" value="0">
                            <div class="col-xs-1">
                              <input type="text" class="form-control" id="room_capacity" name="room_capacity[0]" placeholder="kapasitas kamar">
                            </div>

                            <div class="col-xs-3">
                              <input type="text" class="form-control" id="rate_room" name="rate_room[0]" placeholder="Harga Kamar">
                            </div>

                            <div class="col-xs-3">
                              <input type="file" class="form-control" id="photo" name="photo[0]" placeholder="Foto Kamar">
                            </div>

                            <div class="col-xs-1">                   
                                <a id="add" class="btn btn-success btn-sm"> tambah<i class="fa fa-plus"></i></a>
                            </div>                 
                          </div>
                        </div>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->

                    <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="/admin/hotels" class="btn btn-info"> Back</a>
                    </div>
                </form>
                </div>
          </div>
        </div>
    </section>                  
</div>

@endsection