@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{$hotel->name}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/hotels">Hotel</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    
    <div class="box-header with-border">
      <h3 class="box-title">Edit Hotel</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/hotels/update') }}" enctype="multipart/form-data">
        @csrf
        
        <input type="hidden" name="id" value="{{ $hotel->id }}">
        <div class="form-group">
          <label class="col-sm-2 control-label">Nama Hotel</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" value="{{$hotel->name}}" placeholder="Enter Nama Hotel">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Hotel Bintang</label>
          <div class="col-sm-9">
            <select class="form-control select2" id="star" name="star" style="width: 100%;">
              <option selected="selected" value="0">Pilih Bintang Hotel</option>                                
                <option value="1">*</option>
                <option value="2">**</option>
                <option value="3">***</option>
                <option value="4">****</option>
                <option value="5">*****</option>                
            </select>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Provinsi</label>
          <div class="col-sm-9">
            <select class="form-control select2" id="state_id" name="state_id" style="width: 100%;">
              <option selected="selected" >Pilih Provinsi</option>
                @foreach($states as $state)
                    @if($state->id == $hotel->state_id)
                    <option value="{{$state->id}}" selected="">{{$state->name}}</option>
                    @else
                    <option value="{{$state->id}}">{{$state->name}}</option>
                    @endif
                @endforeach              
            </select>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Kota</label>
          <div class="col-sm-9">
            <select class="form-control select2" id="district_id" name="district_id" style="width: 100%;">
              <option selected="selected">Pilih Kota</option>
                @foreach($districts as $district)
                    @if($district->id == $hotel->district_id)
                        <option value="{{$district->id}}" selected="">{{$district->name}}</option>
                    @else
                        <option value="{{$district->id}}">{{$district->name}}</option>
                    @endif
                @endforeach              
            </select>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Rate</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="rate" name="rate" value="{{$hotel->rate}}" placeholder="Enter Nama Rate Merchane">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Phone</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="contact" name="contact" value="{{$hotel->contact}}" placeholder="Enter Nama Phone">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Email</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="email" name="email" value="{{$hotel->email}}" placeholder="Enter Nama Email">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Address</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="address" name="address" value="{{$hotel->address}}" placeholder="Enter Alamat Merchane">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Fasilitas Hotel</label>
          <div class="col-sm-9">
            <textarea class="form-control" rows="3" id="facility_hotel" name="facility_hotel" placeholder="Enter Fasilitas Hotel">{{$hotel->facility_hotel}}</textarea>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Fasilitas Kamar</label>
          <div class="col-sm-9">
            <textarea class="form-control" rows="3" id="facility_room" name="facility_room" placeholder="Enter Fasilitas Kamar">{{$hotel->facility_room}}</textarea>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Photo Hotel</label>
          <div class="col-sm-9">
            <input type="file" class="form-control" id="photo_hotel" name="photo_hotel" placeholder="Foto Hotel">
          </div>
        </div>
                
        <div class="form-group">
          <label class="col-sm-2 control-label">Pengelola Hotel</label>
          <div class="col-sm-9">
            <select class="form-control select2" id="user_id" name="user_id" style="width: 100%;">
              <option selected="selected" value="0">Pilih User Pengelola Hotel</option>
                @foreach($hotel_member as $member)
                    @if($member->id == $hotel->user_id)
                    <option value="{{$member->id}}" selected="">{{$member->name}}</option>
                    @else
                    <option value="{{$member->id}}">{{$member->name}}</option>
                    @endif                    
                @endforeach              
            </select>
          </div>
        </div>        
        
        <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
            <a href="/admin/merchants" class="btn btn-info"> Back</a>
        </div>        
    </form>
</div>

@endsection