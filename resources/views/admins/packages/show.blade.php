@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<section class="content-header">
  <h1>
    Package
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Package</a></li>
    <li class="active">Show</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">      
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt>Nama Paket Libura</dt>
            <dd>{{$package->name}}</dd>        
            <dt>Lokasi Paket</dt>
            <dd>{{$package->state->name}} - {{$package->district->name}}</dd>              
            <dt>Durasi Paket</dt>
            <dd>{{$package->duration}}</dd>
            <dt>Banner Paket Liburat</dt>
            <dd>              
              <img class="img-responsive pad" src="{{ url($package->photo) }}" alt="Photo" height="108" width="130">
            </dd>
            <dt>Durasi Paket</dt>
            <dd>{{$package->duration}}</dd>
            <dt>Harga Paket</dt>
            <dd>Rp.{{ number_format($package->rate,2,',','.') }}</dd>
            <dt>Tanggal Mulai</dt>
            <dd>{{date("d-M-Y", strtotime($package->start_date))}}</dd>
            <dt>Tanggal Berakhir</dt>
            <dd>{{date("d-M-Y", strtotime($package->end_date))}}</dd>
            <dt>Penerbangan Keberangkatan</dt>
            <dd>
              @foreach($flights as $flight)
                @foreach($package->package_details as $package_detail)
                  @if($package_detail->departure_flight_id && $flight->id == $package_detail->departure_flight_id)
                    {{$flight->code}} - {{$flight->departure->name}} to {{$flight->arrival->name}}
                  @endif
                @endforeach
              @endforeach
            </dd>
            <dt>Penerbangan Kepulangan</dt>
            <dd>
              @foreach($flights as $flight)
                @foreach($package->package_details as $package_detail)
                  @if($package_detail->arrival_flight_id && $flight->id == $package_detail->arrival_flight_id)
                    {{$flight->code}} - {{$flight->departure->name}} to {{$flight->arrival->name}}
                  @endif
                @endforeach
              @endforeach
            </dd>
            <dt>Penerbangan Hotel</dt>
            <dd>
              @foreach($hotels as $hotel)
                @foreach($package->package_details as $package_detail)
                  @if($package_detail->hotel_id && $hotel->id == $package_detail->hotel_id)
                    {{$hotel->name}}
                  @endif
                @endforeach
              @endforeach
            </dd>
          </dl>
         <div class="col s12">&nbsp;</div>
            <a href="/admin/packages" class="btn btn-info btn-xs"> Back</a>
            <a href="/admin/packages/edit/{{$package->id}}" class="btn btn-primary btn-xs"> Edit</a>
            <a href="/admin/packages/delete/{{$package->id}}" class="btn btn-warning btn-xs"> Delete</a>
        </div>         
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">      
              <h3 class="box-title">Merchant Details</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <a href="/admin/merchant_details/create?merchant_id={{$package->id}}" class="btn btn-success btn-sm"> Tambah </a>
                <br>    
                <br>            
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nama Merchant</th>
                  <th>Tipe Merchant</th>
                  <th>ALamat</th>
                  <th>Fasilitas</th>
                  <th>Harga</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($package->package_details as $package_detail)  
                @if($package_detail->merchant_id)                
                  <tr>
                    <td>{{$package_detail->merchant['name']}}</td>
                    <td>{{$package_detail->merchant['merchant_type_id']}}</td>
                    <td>{{$package_detail->merchant['address']}}</td>
                    <td>{{$package_detail->merchant['facilities']}}</td>
                    <td>{{$package_detail->merchant['rate']}}</td>
                    <td>                      
                      <a href="/admin/merchant_details/delete/{{$package_detail->merchant['id']}}" class="btn btn-warning btn-xs"> Delete</a>
                  </td>
                  </tr>
                @endif
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Nama Merchant</th>
                  <th>Tipe Merchant</th>
                  <th>ALamat</th>
                  <th>Fasilitas</th>
                  <th>Harga</th>
                  <th>Action</th>
                </tr>
                </tfoot>
                </table>                                                  
            </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->     
        </div>
    </div>
    <!-- /.row -->
</section>

@endsection