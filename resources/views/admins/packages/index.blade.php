@extends('layouts.admin')

@section('js')
    <!-- data-tables -->
    $(function () {
        $('#example1').DataTable()          
    })
@endsection

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    List Packages
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="/admin/packages">Packages</a></li>
    <li class="active">Index</li>
  </ol>
</section>
<!-- /.box-header -->
<section class="content">
<div class="row">
    <div class="col-xs-12">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
          <a href="/admin/packages/create" class="btn btn-success btn-sm"> Tambah Paket Liburan</a>
          <br>    
          <br>            
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Nama Paket Liburan</th>
              <th>Lokasi</th>
              <th>Tanggal Mulai</th>
              <th>Tanggal Berakhir</th>
              <th>rate</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($packages as $package)
            <tr>
              <td>{{ $package->name }}</td>
              <td>{{ $package->state->name }} - {{ $package->district->name }}</td>
              <td>{{ date("d-M-Y", strtotime($package->start_date)) }}</td>
              <td>{{ date("d-M-Y", strtotime($package->end_date)) }}</td>
              <td>Rp.{{ number_format($package->rate,2,',','.') }}</td>
              <td>
                  <a href="/admin/packages/show/{{$package->id}}" class="btn btn-info btn-xs"> Show</a>
                  <a href="/admin/packages/edit/{{$package->id}}" class="btn btn-primary btn-xs"> Edit</a>
                  <a href="/admin/packages/delete/{{$package->id}}" class="btn btn-warning btn-xs"> Delete</a>
              </td>          
            </tr>
            @endforeach        
            </tbody>
            <tfoot>
            <tr>
              <th>Nama Paket Liburan</th>
              <th>Lokasi</th>
              <th>Tanggal Mulai</th>
              <th>Tanggal Berakhir</th>
              <th>rate</th>
              <th>Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
</div>
</section>
@endsection