@extends('layouts.admin')

@section('js')
<script type="text/javascript">
      
</script>
@endsection

@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambahkan Paket Liburan
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/packages">Paket Liburan</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    
    <section class="content">
      <div class="row">
            <div class="col-md-12">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/packages/update') }}" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="id" value="{{ $package->id }}">
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Nama Paket Liburan</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" value="{{$package->name}}" placeholder="Enter Nama Paket Liburan">
                      </div>
                    </div>                    

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Provinsi</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="state_id" name="state_id" style="width: 100%;">
                          <option selected="selected" >Pilih Provinsi</option>
                            @foreach($states as $state)
                              @if($state->id == $package->state_id)
                              <option value="{{$state->id}}" selected="">{{$state->name}}</option>
                              @else
                              <option value="{{$state->id}}">{{$state->name}}</option>
                              @endif
                            @endforeach              
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Kota</label>
                      <div class="col-sm-9">
                        <select class="form-control select2" id="district_id" name="district_id" style="width: 100%;">
                          <option selected="selected">Pilih Kota</option>
                            @foreach($districts as $district)
                              @if($district->id == $package->district_id)
                              <option value="{{$district->id}}" selected="">{{$district->name}}</option>
                              @else
                              <option value="{{$district->id}}">{{$district->name}}</option>
                              @endif
                            @endforeach              
                        </select>
                      </div>
                    </div>                    

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Durasi Liburan</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" id="duration" name="duration" value="{{$package->duration}}" placeholder="Durasi Liburan">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Banner Paket Liburan</label>
                      <div class="col-sm-9">
                        <input type="file" class="form-control" id="photo" name="photo" value="kosong" placeholder="Foto Kamar">
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Tanggal Mulai Liburan</label>
                      <div class="col-sm-9">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right datepicker" value="{{$package->start_date}}" name="start_date">
                        </div>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Tanggal Berakhir Liburan</label>
                      <div class="col-sm-9">
                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right datepicker" value="{{$package->end_date}}" name="ent_date">
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="col-sm-2 control-label">Keterangan Order</label>
                      <div class="col-sm-9">
                        <textarea class="form-control" rows="3" id="additional_note" name="additional_note" placeholder="Masukan Keterangan Order">{{$package->description}}</textarea>
                      </div>
                    </div>                   

                    <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Pilih Penerbangan </h3>
                        </div>
                        <div id="dinamic_form">            
                        <div class="box-body">
                          <div class="row">
                            <div class="col-xs-6">
                              Penerbangan Keberangkatan :
                            </div>
                            <div class="col-xs-6">
                              <select class="form-control select2" id="departure_flight_id" name="departure_flight_id" style="width: 100%;">
                                  <option selected="selected" >Pilih Penerbangan Keberangkatan</option>
                                    @foreach($flights as $flight)
                                      @foreach($package->package_details as $package_detail)
                                        @if($package_detail->departure_flight_id && $flight->id == $package_detail->departure_flight_id)
                                          <option value="{{$flight->id}}" selected="">{{$flight->code}}</option>
                                        @endif
                                      @endforeach
                                        <option value="{{$flight->id}}">{{$flight->code}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <br>
                            <div class="col-xs-6">
                              Penerbangan Kepulangan :
                            </div>
                            <div class="col-xs-6">
                              <select class="form-control select2" id="arrival_flight_id" name="arrival_flight_id" style="width: 100%;">
                                  <option selected="selected" >Pilih Penerbangan Kepulangan</option>
                                    @foreach($flights as $flight)
                                      @foreach($package->package_details as $package_detail)
                                        @if($package_detail->arrival_flight_id && $flight->id == $package_detail->arrival_flight_id)
                                          <option value="{{$flight->id}}" selected="">{{$flight->code}}</option>
                                        @endif
                                      @endforeach
                                        <option value="{{$flight->id}}">{{$flight->code}}</option>
                                    @endforeach              
                                </select>
                            </div>
                                                        
                          </div>
                        </div>
                        </div>
                        <!-- /.box-body -->
                      </div>
                      <!-- /.box -->

                      <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Pilih Hotel </h3>
                        </div>
                        <div id="dinamic_form">            
                        <div class="box-body">
                          <div class="row">
                            <div class="col-xs-6">
                              Hotel :
                            </div>
                            <div class="col-xs-6">
                              <select class="form-control select2" id="hotel_id" name="hotel_id" style="width: 100%;">
                                  <option selected="selected" >Pilih Hotel Paket Liburan</option>
                                    @foreach($hotels as $hotel)
                                      @foreach($package->package_details as $package_detail)
                                        @if($package_detail->hotel_id && $hotel->id == $package_detail->hotel_id)
                                          <option value="{{$hotel->id}}" selected="">{{$hotel->name}}</option>
                                        @endif
                                      @endforeach
                                        <option value="{{$hotel->id}}">{{$hotel->name}}</option>
                                    @endforeach              
                                </select>
                            </div>                            
                                                        
                          </div>
                        </div>
                        </div>
                        <!-- /.box-body -->
                      </div>

                      <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Pilih Mercant Wisata</h3>
                        </div>
                        <div id="dinamic_form_merchant_wisata">            
                        @php
                        $i = 1;
                        @endphp
                        @foreach($package->package_details as $package_detail)
                        @if($package_detail->merchant_id &&  $package_detail->merchant->merchant_type_id == 2 )
                        <div class="box-body">
                          <div class="row">
                            <div class="col-xs-6">
                              Merchant Wisata ke-1:
                            </div>
                            <div class="col-xs-5">
                              <select class="form-control select2" id="merchant_wisata_id[{{$i}}]" name="merchant_wisata_id[{{$i}}]" style="width: 100%;">
                                  <option selected="selected" >Pilih Merchant Wisata</option>
                                    @foreach($merchants_wisata as $merchant)
                                      @if($package_detail->merchant_id == $merchant->id)
                                        <option value="{{$merchant->id}}" selected="">{{$merchant->name}}</option>
                                      @else
                                        <option value="{{$merchant->id}}">{{$merchant->name}}</option>
                                      @endif                                        
                                    @endforeach              
                                </select>
                            </div> 
                            <div class="col-xs-1">                   
                                <!-- <a id="add_merchant_wisata" class="btn btn-success btn-sm"> tambah<i class="fa fa-plus"></i></a> -->
                            </div>
                          </div>
                        </div>
                        @php
                        $i = $i + 1;
                        @endphp
                        @endif                        
                        @endforeach

                        </div>
                        <!-- /.box-body -->
                      </div>

                      <div class="box box-info">
                        <div class="box-header with-border">
                          <h3 class="box-title">Pilih Mercant Kuliner</h3>
                        </div>
                        <div id="dinamic_form_merchant_culinary">
                        @php
                        $j = 1;
                        @endphp
                        @foreach($package->package_details as $package_detail)
                        @if($package_detail->merchant_id &&  $package_detail->merchant->merchant_type_id == 1 )
                        <div class="box-body">
                          <div class="row">
                            <div class="col-xs-6">
                              Merchant Kuliner ke-1:
                            </div>
                            <div class="col-xs-5">
                              <select class="form-control select2" id="merchant_culinary_id[{{$j}}]" name="merchant_culinary_id[{{$j}}]" style="width: 100%;">
                                  <option selected="selected" >Pilih Merchant Kuliner</option>
                                    @foreach($merchants_culinary as $merchant)
                                    @if($package_detail->merchant_id == $merchant->id)
                                        <option value="{{$merchant->id}}" selected="">{{$merchant->name}}</option>
                                      @else
                                        <option value="{{$merchant->id}}">{{$merchant->name}}</option>
                                      @endif
                                    @endforeach              
                                </select>
                            </div>                    
                          </div>
                        </div>
                        @php
                        $j = $j + 1;
                        @endphp
                        @endif                        
                        @endforeach
                        </div>
                        <!-- /.box-body -->
                      </div>

                    <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="/admin/packages" class="btn btn-info"> Back</a>
                    </div>
                </form>
                </div>
          </div>
        </div>
    </section>                  
</div>

@endsection