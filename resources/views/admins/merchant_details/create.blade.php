@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<!-- /.box-header -->
<div class="box box-info">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambahkan Merchant Detail
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/admin/merchants">Merchant</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>
    
    <div class="box-header with-border">
      <h3 class="box-title"></h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/merchant_details/insert') }}" enctype="multipart/form-data">
        @csrf           
        
        <input type="hidden" name="merchant_id" value="{{$merchant->id}}">
        <div class="form-group">
          <label class="col-sm-2 control-label">Nama Menu / Nama Arena</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="name" name="name" placeholder="Enter ...">
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-2 control-label">Stok / Kapasitas</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="stock" name="stock" placeholder="Enter ....">
          </div>
        </div>
        
         <div class="form-group">
          <label class="col-sm-2 control-label">Booking</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="on_booking" name="on_booking" placeholder="Enter ....">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Harga</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="rate" name="rate" placeholder="Enter ....">
          </div>
        </div>   

        <div class="form-group">
          <label class="col-sm-2 control-label">Photo</label>
          <div class="col-sm-9">
            <input type="file" class="form-control" id="photo" name="photo"  placeholder="Enter ....">
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-2 control-label">Keterangan</label>
          <div class="col-sm-9">
            <textarea class="form-control" rows="3" id="description" name="description" placeholder="Masukan Keterangan Merchane"></textarea>
          </div>
        </div>
              
                    

        
        <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
            <a href="/admin/merchants/show/{{$merchant->id}}" class="btn btn-info"> Back</a>
        </div>        
    </form>
</div>

@endsection