@extends('layouts.admin')

@section('js')

@endsection

@section('content')
<section class="content-header">
  <h1>
    Merchants Detail
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="">Merchant Detail</a></li>
    <li class="active">Show</li>
  </ol>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">      
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            @php
            $available = $merchant_detail->stock - $merchant_detail->on_booking;
            @endphp
            <dt>Nama Menu / Nama Tempat</dt>
            <dd>{{ $merchant_detail->name }}</dd>      
            <dt>Stok</dt>
            <dd>{{$merchant_detail->stock}}</dd>
              <dt>On Booking</dt>
            <dd>{{$merchant_detail->on_booking}}</dd>        
              <dt>Ketersediaan</dt>
            <dd>{{$available}}</dd>                  
              <dt>Harga</dt>
            <dd>Rp.{{ number_format($merchant_detail->rate,2,',','.') }}</dd>        
              <dt>Foto</dt>
            <dd>
              <img class="img-responsive pad" src="{{ url($merchant_detail->photo) }}" alt="Photo" height="308" width="230">
          </dl>
         <div class="col s12">&nbsp;</div>
            <a href="/admin/merchants/show/{{$merchant_detail->merchant_id}}" class="btn btn-info btn-xs"> Back</a>
            <a href="/admin/merchant_detail/edit/{{$merchant_detail->id}}" class="btn btn-primary btn-xs"> Edit</a>
            <a href="/admin/merchant_detail/delete/{{$merchant_detail->id}}" class="btn btn-warning btn-xs"> Delete</a>
        </div>         
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    </div>
</section>

@endsection