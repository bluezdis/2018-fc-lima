@extends('layouts.app')

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $("#quantity_order").change(function(){            
            var quantity_order = parseInt($('#quantity_order').val());
            var rate = parseInt($('#rate_data').val());
            var total = quantity_order * rate;
            $("#total").val("Rp."+numberWithCommas(parseInt(total))+",00");
        });

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }    
    });
</script>
@endsection

@section('content')
<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">				
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    Flight
                </h1>	
                <p class="text-white link-nav"><a href="#">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="#"> Flight</a></p>
            </div>	
        </div>
    </div>
</section>
<!-- End banner Area -->	

<!-- Start destinations Area -->
<section class="destinations-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-40 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Boking Wisata Kota</h1>
                </div>
            </div>            
        </div>       
        <div class="row">                 
            <div class="col-lg-6">
            	<div class="single-destinations">
                    <div class="thumb">                        
                        <img class="img-fluid" src="{{ url($flight->plane->logo) }}" alt="">
                    </div>
                    <div class="details">
                        <h4 class="d-flex justify-content-between">
                            <span>{{$flight->plane->name}}</span>
                        </h4>
                        <ul class="package-list">
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Kode Penerbangan</span>
                                {{ $flight->code }}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Maskapai</span>
                                {{ $flight->plane->name }} - {{ $flight->plane->plane_code }}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Tanggal & Jam Keberangkatan</span>
                                {{ date("d-M-Y", strtotime($flight->flight_date_departure)) }} - {{$flight->flight_time_departure}}
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Tanggal & Jam Keberangkatan</span>
                                {{$flight->duration}}
                            </li>
                        	<li class="d-flex justify-content-between align-items-center">
                                <span>Bandara Keberangkatan</span>
                                {{ $flight->departure->name }} - {{ $flight->departure->code_airport }}
                            </li>                            
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Bandara Kedatangan</span>
                                {{ $flight->arrival->name }} - {{ $flight->arrival->code_airport }} 
                            </li>           
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Harga Tiket </span>
                                <input type="text" class="price-btn" id="rate" name="rate" value="Rp.{{ number_format($flight->rate,2,',','.') }}" readonly="" >
                                <input type="hidden" name="rate_data" id="rate_data" value="{{$flight->rate}}">
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Total Biaya</span>               

                                <input type="text" class="price-btn" id="total" name="total" value="Rp.{{ number_format($flight->rate,2,',','.') }}" readonly="" >
                            </li>                            
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
	            <div class="section-top-border">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<h3 class="mb-30">Isi Form Berikut</h3>
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('flight/booking/insert') }}">
                                @csrf
                                <input type="hidden" name="flight_id" id="flight_id" value="{{$flight->id}}">
                                <div class="mt-10">
                                    <input type="text" class="form-control" id="invoice" name="invoice" value="{{$invoice}}" readonly="" placeholder="Enter Nama Order">
                                    Nomor Invoice
                                </div>                                
                                <div class="mt-10">
                                    <input type="text" class="form-control" id="datepicker" name="flight_date_departure" placeholder="Tanggal Pemesanan" value="{{$flight->flight_date_departure}}" readonly="">
                                    Tanggal Keberangkatan 
                                </div>
                                <div class="mt-10">
                                    <input type="text" class="form-control" id="datepicker" name="flight_time_departure" placeholder="Tanggal Pemesanan" value="{{$flight->flight_time_departure}}" readonly="">
                                    Jam Keberangkatan 
                                </div>
								<div class="mt-10">
									<input type="text" class="form-control" id="quantity_order" name="total" placeholder="Jumlah Tiket yang di pesan" value="" >
                                    Jumlah Tiket
								</div>
								<div class="input-group-icon mt-10">
									<div class="icon"><i class="fa fa-money" aria-hidden="true"></i></div>
									<div class="form-select" id="default-select">
										<select id="payment_method" name="payment_method">
											<option value="1" selected="">Piih Tipe Pembayaran</option>
											@php
				                              $i = 1;
				                             @endphp
				                              @foreach($payment_methods as $payment_method)
				                                <option value="{{$i}}">{{$payment_method}}</option>
				                                @php
				                                $i = $i + 1;
				                                @endphp
				                              @endforeach											
										</select>
									</div>
								</div>
								<div class="mt-10">
								<textarea class="form-control" rows="3" id="additional_note" name="additional_note" placeholder="Masukan Keterangan Order"></textarea>								
								</div>
																                                
                                <div class="mt-10">
                                    <button type="submit" class="genric-btn success radius">Submit</button>
                                </div>

							</form>
						</div>					
					</div>
				</div>
			</div>            

        </div>
    </div>	
</section>
<!-- End destinations Area -->
@endsection