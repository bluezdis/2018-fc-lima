@extends('layouts.app')

@section('content')
<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">				
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    Flight				
                </h1>	
                <p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="about.html"> Flight</a></p>
            </div>	
        </div>
    </div>
</section>
<!-- End banner Area -->

<!-- Start about-info Area -->
<section class="about-info-area section-gap">
    <div class="container">
        <h1 class="text-black">
            Jadwal Penerbangan
        </h1>
        <br>
        <div class="row align-items-center">
            <div class="col-lg-12">
                <form class="form-area contact-form text-right" method="GET">
                    <div class="row">   
                        <div class="col-lg-3 form-group">
                            <p>Keberangkatan</p>
                            <div class="input-group-icon mt-10">                            
                                <select class="form-control select2" id="departure_id" name="departure_id" style="width: 100%;">
                                    <option selected="selected" value="">Pilih Bandara <i class="fa fa-globe" aria-hidden="true"></i></option>
                                    @foreach($departures as $departure)
                                        <option value="{{$departure->id}}">{{$departure->name}} - {{$departure->code_airport}}</option>
                                    @endforeach                                
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 form-group">
                            <p>Kedatangan</p>
                            <div class="input-group-icon mt-10">                            
                                <select class="form-control select2" id="arrival_id" name="arrival_id" style="width: 100%;">
                                    <option selected="selected" value="">Pilih Bandara <i class="fa fa-globe" aria-hidden="true"></i></option>
                                    @foreach($arrivals as $arrival)
                                        <option value="{{$arrival->id}}">{{$arrival->name}} - {{$arrival->code_airport}}</option>
                                    @endforeach                                
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 form-group">
                            <p>Tanggal Keberangkatan</p>
                            <div class="input-group-icon mt-10">                            
                                <input type="date" class="form-control" id="datepicker" name="flight_date_departure" placeholder="Tanggal Keberangkatan">
                            </div>
                        </div>
                        <!-- <div class="col-lg-3 form-group">
                            <p>Tanggal Kedatangan </p>
                            <div class="input-group-icon mt-10">
                                <input type="date" class="form-control" id="datepicker" name="flight_date_arrival" placeholder="Tanggal Kedatangan">
                            </div>
                        </div> -->                                                            
                        <div class="col-lg-12">
                            <div class="alert-msg" style="text-align: left;"></div>
                            <button class="genric-btn primary" style="float: left;">Cari Sekarang</button>
                        </div>
                    </div>                
                </form> 
            </div>                                     
        </div>

        <div class="section-top-border">                        
            <div class="col-md-12 mt-sm-30 typo-sec">
                <div class="">
                    <ul class="unordered-list">
                        <li>{{$_GET['departure_id'] ? $departures->find($_GET['departure_id'])->name : ' '}} / {{$_GET['arrival_id'] ? $arrivals->find($_GET['arrival_id'])->name : ' '}}</li>
                        <li>Keberangkatan : {{$_GET['flight_date_departure'] ? date("d-M-Y", strtotime($_GET['flight_date_departure'])) : ' '}}</li>                        
                    </ul>
                </div>
            </div>

            <br>
            <br>
            <table id="example1" class="table table-bordered table-striped" width="1150";>
                <thead>
                <tr>
                  <th>Maskapai</th>
                  <th>Departure</th>
                  <th>Arrival</th>
                  <th>Waktu</th>
                  <th>Harga</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($flights as $flight)
                    <tr>
                      <td>
                          <img src="{{$flight->plane->logo}}" alt="flag" height="88" width="178">
                      </td>
                      <td>{{ date("d-M-Y", strtotime($flight->flight_date_departure)) }} - {{ $flight->flight_time_departure }} - {{$flight->departure->district->name}} ({{$flight->code}})</td>
                      <td>{{ date("d-M-Y", strtotime($flight->flight_date_arrival)) }} - {{ $flight->flight_time_arrival }} - {{$flight->arrival->district->name}} ({{$flight->code}})</td>
                      <td>{{$flight->duration}}</td>          
                      <td>Rp.{{ number_format($flight->rate,2,',','.') }}/Org</td>
                      <td>
                          <a href="/flight/booking/{{$flight->id}}" class="btn btn-info btn-xs">Pilih</a>                          
                      </td>          
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Maskapai</th>
                  <th>Departure</th>
                  <th>Arrival</th>
                  <th>Waktu</th>
                  <th>Harga</th>              
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>               
        </div>
    </div>	    
</section>
<!-- End about-info Area -->



@endsection